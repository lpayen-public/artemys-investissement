### Pré-requis
- symfony-cli
- composer
- npm

---

### Installation
- git clone git@gitlab.com:eosia/artemys.git `[folder]`
- cd `[folder]`
- docker-compose up -d --build
- docker exec -it `[WWW container_name]` bash (voir docker-compose.yml)
- symfony composer install
- npm install
- npm run build

### Base de données
- .env : DATABASE_URL="mysql://root:`[DB container_name]`:3306/artemys?..." (voir docker-compose.yml)
- symfony console `doctrine:database:create`
- symfony console `doctrine:migrations:migrate`
- importer en DB le fichier `.docker/ville.sql.zip` (contient les 36000 villes françaises notamment)
- tests unitaires : refaire les manipulations en DB pour .env.test


### Création de l'utilisateur SUPER ADMIN
- symfony console `ct:user:create` `email` (email obligatoire en paramètre)

### Insertion des données fictives
- symfony console `ct:db:init` (pas de fixtures, sinon suppression des villes)
- symfony console `ct:customer:create`

### Droits / permissions
- adduser `username`
- chown -R `username`:`username` .
- chown -R www-data:www-data public/.
- chown -R www-data:www-data var/.

### Il n'y a plus qu'à :)
- npm run watch
- http://localhost:8080/ (site)
- http://localhost:8080/admin/ (admin)
- http://localhost:8082/ (phpmyadmin)
- http://localhost:8083/ (maildev)