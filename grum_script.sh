#!/bin/bash
echo "--------------------------------------------------------------------------------------"
echo ""
echo "php bin/console lint:twig"
php bin/console lint:twig
echo "--------------------------------------------------------------------------------------"
echo ""
echo "./vendor/bin/twig-linter lint ./templates"
./vendor/bin/twig-linter lint ./templates
echo "--------------------------------------------------------------------------------------"
echo ""
echo "php -d memory_limit=-1 vendor/bin/php-cs-fixer fix -vvv --show-progress=dots"
php -d memory_limit=-1 vendor/bin/php-cs-fixer fix -vvv --show-progress=dots
echo "--------------------------------------------------------------------------------------"
echo ""
echo "php -d memory_limit=-1 vendor/bin/phpstan analyse"
php -d memory_limit=-1 vendor/bin/phpstan analyse
echo "--------------------------------------------------------------------------------------"
echo ""
echo "./vendor/bin/phpcbf"
./vendor/bin/phpcbf
echo "--------------------------------------------------------------------------------------"
echo ""
echo "./vendor/bin/phpcs -v --standard=PSR12 --ignore=./src/Kernel.php ./src"
./vendor/bin/phpcs -v --standard=PSR12 --ignore=./src/Kernel.php ./src
echo "--------------------------------------------------------------------------------------"
echo ""
echo "php bin/phpunit"
php bin/phpunit
echo "--------------------------------------------------------------------------------------"
