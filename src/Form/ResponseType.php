<?php

namespace App\Form;

use App\Entity\ProductQuestion;
use App\Entity\ProductResponse;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ResponseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('value', TextType::class, [
                'label' => 'Valeur',
            ])
            ->add('question', EntityType::class, [
                'class' => ProductQuestion::class,
                'label' => 'name',
                'choice_label' => 'name',
                'query_builder' => function (EntityRepository $er): QueryBuilder {
                    return $er->createQueryBuilder('question')
                        ->orderBy('question.name', 'ASC')
                    ;
                },
                'attr' => ['class' => 'd-none'],
                'label_attr' => ['class' => 'bold'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ProductResponse::class,
        ]);
    }
}
