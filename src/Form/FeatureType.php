<?php

namespace App\Form;

use App\Entity\Feature;
use App\Entity\Product;
use App\Entity\User;
use App\Service\Admin\HtmlService;
use App\Utils\Text;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FeatureType extends AbstractType
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Feature $feature */
        $feature = $builder->getData();
        $products = clone $feature->getProducts();

        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom',
                'label_attr' => ['class' => 'bold'],
                'row_attr' => ['class' => HtmlService::MARGIN_FORM],
            ])
//            ->add('icon', TextType::class, [
//                'label'    => 'Icone',
//                'required' => false,
//                'label_attr' => ['class' => 'bold'],
//                'row_attr'   => ['class' => HtmlService::MARGIN_FORM],
//            ])
            ->add('token', HiddenType::class, [
                'data' => $feature->getToken() ?: Text::token(),
            ])
            ->add('products', EntityType::class, [
                'label' => 'Terrains',
                'label_html' => true,
                'class' => Product::class,
                'choice_label' => function (Product $product) {
                    return '<div class=""><span class="text-black">' . $product->getName() . '</span>' .
                           ' <span class="color-6">(' . $product->getType()?->getName() . ')</span>' .
                           '<div class="color-b">' . $product->getCityZipcode() . '</div></div>';
                },
                'query_builder' => function (EntityRepository $er): QueryBuilder {
                    return $er->createQueryBuilder('product')
                        ->orderBy('product.name', 'ASC')
                    ;
                },
                'expanded' => true,
                'multiple' => true,
                'label_attr' => ['class' => 'mb10'],
                'row_attr' => ['class' => '1'],
            ])
            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) use ($products) {
                /** @var Feature $feature */
                $feature = $event->getData();

                $productsForm = $feature->getProducts();

                foreach ($products as $product) {
                    if (!$productsForm->contains($product)) {
                        $product->removeFeature($feature);
                    }
                }

                foreach ($productsForm as $product) {
                    $product->addFeature($feature);
                }

                /** @var User $user */
                $user = $this->em->getRepository(User::class)->find(1);

                if (!$feature->getCreatedAt()) {
                    $feature
                        ->setCreatedAt(new \DateTime())
                        ->setCreatedBy($user)
                    ;
                }
                $feature
                    ->setUpdatedAt(new \DateTime())
                    ->setUpdatedBy($user)
                ;
            })
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Feature::class,
        ]);
    }
}
