<?php

namespace App\Form;

use App\Entity\ProductQuestion;
use App\Entity\User;
use App\Utils\Text;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductQuestion1Type extends AbstractType
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom',
                'label_attr' => ['class' => 'bold'],
                'row_attr' => ['class' => ' '],
            ])
            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
                /** @var ProductQuestion $question */
                $question = $event->getData();

                /** @var User $user */
                $user = $this->em->getRepository(User::class)->find(1);

                if (!$question->getCreatedAt()) {
                    $question
                        ->setCreatedAt(new \DateTime())
                        ->setCreatedBy($user)
                    ;
                }
                $question
                    ->setUpdatedAt(new \DateTime())
                    ->setUpdatedBy($user)
                    ->setToken(Text::token())
                ;
            })
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ProductQuestion::class,
        ]);
    }
}
