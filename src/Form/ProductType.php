<?php

namespace App\Form;

use App\Entity\Feature;
use App\Entity\Product;
use App\Entity\User;
use App\Service\Admin\HtmlService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Product $product */
        $product = $builder->getData();

        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom',
                'label_attr' => ['class' => 'bold'],
                'row_attr' => ['class' => HtmlService::MARGIN_FORM],
            ])
            ->add('text', TextareaType::class, [
                'label' => 'Description',
                'required' => false,
                'label_attr' => ['class' => 'bold'],
                'row_attr' => ['class' => HtmlService::MARGIN_FORM],
                'attr' => ['class' => 'tinymce'],
                'help' => 'Si aucune description, le bien est automatiquement désactivé',
                'help_attr' => ['class' => 'text-end'],
            ])
            ->add('type', EntityType::class, [
                'label' => 'Type',
                'class' => \App\Entity\ProductType::class,
                'choice_label' => 'name',
                'query_builder' => function (EntityRepository $er): QueryBuilder {
                    return $er->createQueryBuilder('type')
                        ->orderBy('type.name', 'ASC')
                    ;
                },
                'attr' => ['class' => 'no-search'],
                'label_attr' => ['class' => 'bold'],
                'row_attr' => ['class' => HtmlService::MARGIN_FORM],
            ])
            ->add('features', EntityType::class, [
                'label' => 'Caractéristiques',
                'class' => Feature::class,
                'choice_label' => 'name',
                'query_builder' => function (EntityRepository $er): QueryBuilder {
                    return $er->createQueryBuilder('feature')
                        ->orderBy('feature.name', 'ASC')
                    ;
                },
                'expanded' => true,
                'multiple' => true,
                'row_attr' => ['class' => HtmlService::MARGIN_FORM],
            ])
            ->add('token', HiddenType::class, [
                'data' => $product->getToken(),
            ])
            ->add('pictures', FileType::class, [
                'label' => false,
                'required' => !\count($product->getPictures()),
                'multiple' => true,
                'mapped' => false,
                'label_attr' => ['class' => 'bold'],
            ])
            ->add('brochures', FileType::class, [
                'label' => false,
                'required' => false,
                'multiple' => true,
                'mapped' => false,
                'label_attr' => ['class' => 'bold'],
            ])
            ->add('plans', FileType::class, [
                'label' => false,
                'required' => false,
                'multiple' => true,
                'mapped' => false,
                'label_attr' => ['class' => 'bold'],
            ])
            ->add('responses', CollectionType::class, [
                'entry_type' => ResponseType::class,
                'label' => 'Réponses',
                'by_reference' => false,
                'help' => 'Au moins 1 caractère par réponse',
                'label_attr' => ['class' => 'bold'],
                'attr' => ['class' => ' '],
            ])
            ->add('available', CheckboxType::class, [
                'label' => false,
                'required' => false,
                'label_attr' => ['class' => ''],
            ])
            ->add('active', CheckboxType::class, [
                'label' => false,
                'required' => false,
                'label_attr' => ['class' => ''],
                'block_prefix' => 'active',
            ])
            ->add('city', TextType::class, [
                'label' => 'Ville',
                'mapped' => false,
                'data' => $product->getCityZipcode(),
                'row_attr' => ['class' => 'm0'],
                'label_attr' => ['class' => 'bold'],
            ])
            ->add('cityToken', HiddenType::class, [
                'mapped' => false,
                'data' => $product->getCity()?->getToken(),
            ])
            ->add('metaTitle', TextType::class, [
                'label' => 'Titre de le page',
                'required' => false,
                'label_attr' => ['class' => 'bold'],
                'row_attr' => ['class' => HtmlService::MARGIN_FORM],
                'help' => 'Apparait dans l\'onglet du navigateur (si vide, le nom du bien est utilisé)',
            ])
            ->add('metaDescription', TextareaType::class, [
                'label' => 'Description de la page',
                'required' => false,
                'attr' => ['rows' => 3],
                'label_attr' => ['class' => 'bold'],
                'row_attr' => ['class' => HtmlService::MARGIN_FORM],
                'help' => 'Description lue par les robots pour indexer le site (si vide, aucune description)',
            ])
            ->add('metaKeywords', TextType::class, [
                'label' => 'Mots clés de la page',
                'required' => false,
                'label_attr' => ['class' => 'bold'],
                'row_attr' => ['class' => ' '],
                'help' => 'Mots clés lus par les robots pour indexer le site 
                (séparer par des virgules / si vide, aucun mot clé)',
            ])
            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
                /** @var Product $product */
                $product = $event->getData();

                $form = $event->getForm();
                $picturesUploaded = $form->all()['pictures'] ?? [];

                if (!\count($picturesUploaded->getData()) && !\count($product->getPictures())) {
                    $event->getForm()->get('pictures')->addError(
                        new FormError('Vous devez choisir au moins une image')
                    );
                }

                if (!$product->getText()) {
                    $product->setActive(false);
                }

                /** @var User $user */
                $user = $this->em->getRepository(User::class)->find(1);

                if (!$product->getCreatedAt()) {
                    $product
                        ->setCreatedAt(new \DateTime())
                        ->setCreatedBy($user)
                    ;
                }
                $product
                    ->setUpdatedAt(new \DateTime())
                    ->setUpdatedBy($user)
                ;
            })
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
            'enctype' => 'multipart/form-data',
        ]);
    }
}
