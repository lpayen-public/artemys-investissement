<?php

namespace App\Form;

use App\Entity\Partner;
use App\Entity\User;
use App\Service\Admin\HtmlService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class PartnerType extends AbstractType
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Partner $partner */
        $partner = $builder->getData();

        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom',
                'label_attr' => ['class' => 'bold'],
                'row_attr' => ['class' => HtmlService::MARGIN_FORM],
            ])
            ->add('url', TextType::class, [
                'label' => 'Lien Internet',
                'label_attr' => ['class' => 'bold'],
                'row_attr' => ['class' => HtmlService::MARGIN_FORM],
                'required' => false,
                'constraints' => [
                    new Assert\Regex([
                        'pattern' => '/(https?|ftp|ssh|mailto):\/\/[a-z0-9\/:%_+.,#?!@&=-]+/',
                        'message' => 'Erreur de format',
                    ]),
                ],
                'help' => 'Si rien n\'est saisi, le partenaire ne sera pas cliquable sur le site internet',
            ])
            ->add('active', CheckboxType::class, [
                'label' => false,
                'required' => false,
                'label_attr' => ['class' => ''],
                'attr' => ['class' => ''],
                'row_attr' => ['class' => ' '],
                'block_prefix' => 'active',
            ])
            ->add('background', FileType::class, [
                'label' => false,
                'required' => !$partner->getMedia(),
                'multiple' => false,
                'mapped' => false,
                'label_attr' => ['class' => 'bold'],
            ])
            ->add('token', HiddenType::class, [
                'data' => $partner->getToken(),
            ])
            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
                /** @var Partner $partner */
                $partner = $event->getData();

                /** @var User $user */
                $user = $this->em->getRepository(User::class)->find(1);

                if (!$partner->getCreatedAt()) {
                    $partner
                        ->setCreatedAt(new \DateTime())
                        ->setCreatedBy($user)
                    ;
                }
                $partner
                    ->setUpdatedAt(new \DateTime())
                    ->setUpdatedBy($user)
                ;
            })
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Partner::class,
        ]);
    }
}
