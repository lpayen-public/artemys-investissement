<?php

namespace App\Form;

use App\Entity\User;
use App\Service\Admin\HtmlService;
use App\Service\EnvironmentService;
use App\Service\User\UserPasswordInterface;
use App\Twig\AppExtension;
use App\Utils\Phone;
use App\Utils\Text;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class UserType extends AbstractType
{
    private Generator $faker;

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly AppExtension $twig,
        private readonly UserPasswordInterface $password,
        private readonly EnvironmentService $environment
    ) {
        $this->faker = Factory::create('fr_FR');
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var User $user */
        $user = $builder->getData();
        $isNew = !$user->getId();
        $color = $options['color'];
        $isForProfil = $options['isForProfil'] ?? false;

        if (!$isForProfil) {
            $builder
                ->add('lastname', TextType::class, [
                    'label' => 'Nom',
                    'label_attr' => ['class' => 'bold'],
                    'row_attr' => ['class' => HtmlService::MARGIN_FORM],
                    'data' => $isNew && $this->environment->isDev()
                        ? \mb_strtoupper($this->faker->lastName)
                        : $user->getLastname(),
                ])
                ->add('firstname', TextType::class, [
                    'label' => 'Prénom',
                    'label_attr' => ['class' => 'bold'],
                    'row_attr' => ['class' => HtmlService::MARGIN_FORM],
                    'data' => $isNew && $this->environment->isDev()
                        ? $this->faker->firstName
                        : $user->getFirstname(),
                ])
                ->add('jobName', TextType::class, [
                    'label' => 'Fonction',
                    'label_attr' => ['class' => 'bold'],
                    'row_attr' => ['class' => HtmlService::MARGIN_FORM],
                    'data' => $isNew && $this->environment->isDev() ? $this->faker->jobTitle : $user->getJobName(
                    ),
                ])
                ->add('active', CheckboxType::class, [
                    'label' => false,
                    'required' => false,
                    'label_attr' => ['class' => ''],
                    'attr' => ['class' => ' '],
                    'block_prefix' => 'active',
                ])
            ;
        }

        $builder
            ->add('color', TextType::class, [
                'label' => 'Couleur',
                'label_attr' => ['class' => 'bold'],
                'attr' => ['class' => 'js-color h31 w100p'],
                'row_attr' => ['class' => HtmlService::MARGIN_FORM],
                'required' => false,
                'mapped' => false,
                'data' => $color,
            ])
            ->add('email', EmailType::class, [
                'label' => 'Email',
                'label_attr' => ['class' => 'bold'],
                'row_attr' => ['class' => HtmlService::MARGIN_FORM],
                'data' => $isNew ? $this->faker->email : $user->getEmail(),
            ])
            ->add('phone', TelType::class, [
                'label' => 'Téléphone',
                'label_attr' => ['class' => 'bold'],
                'row_attr' => ['class' => HtmlService::MARGIN_FORM],
                'data' => $isNew && $this->environment->isDev()
                    ? $this->faker->phoneNumber
                    : $this->twig->getPhone($user->getPhone()),
                'constraints' => [
                    new Assert\Regex([
                        'pattern' => '/^[0-9 ()\+]{8,15}$/',
                        'message' => 'Erreur de format',
                    ]),
                ],
            ])
            ->add('token', HiddenType::class, [
                'data' => $user->getToken() ?: Text::token(),
            ])
        ;

        $builder
            ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($user, $isForProfil) {
                $this->setPassword($event, $user, $isForProfil);
            })
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $datas = $event->getData();
                $datas['phone'] = $this->twig->getPhone(Phone::clear($datas['phone']));
                $event->setData($datas);
            })
            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) use ($isForProfil) {
                /** @var User $user */
                $user = $event->getData();

                $this->updatePassword($event, $user, $isForProfil);

                if (!$user->getId()) {
                    $user->setRoles(['ROLE_ADMIN']);
                }

                $user
                    ->setLastname(\mb_strtoupper($user->getLastname()))
                    ->setPhone(Phone::clear($user->getPhone()))
                ;

                /** @var User $userModifier */
                $userModifier = $this->em->getRepository(User::class)->find(1);

                if (!$user->getCreatedAt()) {
                    $user
                        ->setCreatedAt(new \DateTime())
                        ->setCreatedBy($userModifier)
                    ;
                }
                $user
                    ->setUpdatedAt(new \DateTime())
                    ->setUpdatedBy($userModifier)
                ;
            })
        ;
    }

    private function getPasswordPattern(): string
    {
        $sc = $this->getSpecialCharacter();

        return '/^(?=(.*[a-z]){1,})(?=(.*[A-Z]){1,})(?=(.*[0-9]){1,})(?=(.*[' . $sc . ']){1,}).{8,}$/';
    }

    private function getSpecialCharacter(): string
    {
        return '!@#$%&*';
    }

    private function setPassword(FormEvent $event, User $user, bool $isForProfil): void
    {
        if (!$isForProfil) {
            $event->getForm()->add('plainPassword', TextType::class, [
                'label' => 'Mot de passe',
                'label_attr' => ['class' => 'bold'],
                'row_attr' => ['class' => HtmlService::MARGIN_FORM],
                'mapped' => false,
                'required' => !$user->getId(),
                'data' => !$user->getId() ? Text::token(5, true, true) . Text::token(5) : '',
                'constraints' => [
                    new Assert\Regex([
                        'pattern' => $this->getPasswordPattern(),
                        'message' => 'Mot de passe invalide !  8 caractères min avec au moins 1 lettre MAJ, 
                                1 lettre MIN, 1 chiffre et 1 caractère spécial (' . $this->getSpecialCharacter() . ')',
                    ]),
                ],
            ]);

            return;
        }

        $event->getForm()
            ->add('plainPassword', PasswordType::class, [
                'label' => 'Mot de passe',
                'help' => 'Laissez vide si vous ne souhaitez pas le modifier',
                'label_attr' => ['class' => 'bold'],
                'row_attr' => ['class' => HtmlService::MARGIN_FORM],
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new Assert\Regex([
                        'pattern' => $this->getPasswordPattern(),
                        'message' => 'Mot de passe invalide !  8 caractères min avec au moins 1 lettre MAJ, 
                                1 lettre MIN, 1 chiffre et 1 caractère spécial (' . $this->getSpecialCharacter() . ')',
                    ]),
                ],
            ])
            ->add('repeatPlainPassword', PasswordType::class, [
                'label' => 'Confirmation du mot de passe',
                'label_attr' => ['class' => 'bold'],
                'row_attr' => ['class' => HtmlService::MARGIN_FORM],
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new Assert\Regex([
                        'pattern' => $this->getPasswordPattern(),
                        'message' => 'Mot de passe invalide !  8 caractères min avec au moins 1 lettre MAJ, 
                                1 lettre MIN, 1 chiffre et 1 caractère spécial (' . $this->getSpecialCharacter() . ')',
                    ]),
                ],
            ])
        ;
    }

    private function updatePassword(FormEvent $event, User $user, bool $isForProfil): void
    {
        $submittedPwd = $event->getForm()->all()['plainPassword']->getData();
        $submittedConfirmPwd = isset($event->getForm()->all()['repeatPlainPassword']) ?
            $event->getForm()->all()['repeatPlainPassword']->getData()
            : null;

        if ($isForProfil && $submittedPwd !== $submittedConfirmPwd) {
            $message = 'Le mot de passe et sa confirmation ne correspondent pas';
            $event->getForm()->get('plainPassword')->addError(new FormError($message));
            $event->getForm()->get('repeatPlainPassword')->addError(new FormError($message));

            return;
        }

        if ($submittedPwd) {
            $user->setPassword($this->password->hash($user, $submittedPwd));
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'isForProfil' => false,
            'color' => '#2980B9',
        ]);
    }
}
