<?php

namespace App\Form;

use App\Entity\Plot;
use App\Entity\PlotStatus;
use App\Service\Admin\HtmlService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PlotType extends AbstractType
{
    public function __construct(private readonly HtmlService $html)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom',
                'label_attr' => ['class' => 'bold'],
                'row_attr' => ['class' => 'col-12 col-md-12 col-lg-2 col-xl-2 mb10'],
            ])
            ->add('surface', TextType::class, [
                'label' => 'Surface',
                'label_attr' => ['class' => 'bold'],
                'row_attr' => ['class' => 'col-12 col-md-12 col-lg-2 col-xl-2 mb10'],
            ])
            ->add('price', IntegerType::class, [
                'label' => 'Prix',
                'grouping' => true,
                'rounding_mode' => \NumberFormatter::ROUND_HALFEVEN,
                'label_attr' => ['class' => 'bold'],
                'row_attr' => ['class' => 'col-12 col-md-12 col-lg-3 col-xl-2 mb10'],
            ])
            ->add('status', EntityType::class, [
                'label' => 'Statut',
                'class' => PlotStatus::class,
                'choice_label' => 'name',
                'expanded' => false,
                'multiple' => false,
                'label_attr' => ['class' => 'bold'],
                'attr' => ['class' => 'no-search'],
                'row_attr' => ['class' => 'col-12 col-md-12 col-lg-3 col-xl-2 mb10'],
            ])
            ->add('delete', ButtonType::class, [
                'label' => '<i class="ri-close-line"></i>',
                'label_html' => true,
                'attr' => ['class' => $this->html->getBtnDelete('sm') . ' js-product-plot-delete'],
                'row_attr' => ['class' => 'col-12 col-md-12 col-lg-2 col-xl-1 mb10 d-flex align-items-end'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Plot::class,
        ]);
    }
}
