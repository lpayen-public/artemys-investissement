<?php

namespace App\Form;

use App\Entity\ProductQuestion;
use App\Entity\ProductResponse;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductQuestionType extends AbstractType
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $product = $options['product'];

        $questions = $this->em->getRepository(ProductQuestion::class)->findAll();

        /** @var ProductQuestion $question */
        foreach ($questions as $question) {
            $response = $this->em->getRepository(ProductResponse::class)->findOneBy([
                'product' => $product,
                'question' => $question,
            ]);

            $builder->add(
                'questions_' . $question->getId(),
                TextType::class,
                [
                    'label' => $question->getName(),
                    'required' => false,
                    'mapped' => false,
                    'row_attr' => ['class' => 'col-4 mb-3'],
                    'data' => $response ? $response->getValue() : '',
                    'attr' => ['salut'],
                    'block_name' => 'qr',
                    'block_prefix' => 'coucou',
                ]
            );
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'product' => null,
        ]);
    }
}
