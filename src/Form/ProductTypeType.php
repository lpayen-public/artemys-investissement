<?php

namespace App\Form;

use App\Entity\ProductType;
use App\Entity\User;
use App\Service\Admin\HtmlService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductTypeType extends AbstractType
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var ProductType $productType */
        $productType = $builder->getData();

        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom',
                'label_attr' => ['class' => 'bold'],
                'row_attr' => ['class' => HtmlService::MARGIN_FORM],
            ])
            ->add('baseline', TextType::class, [
                'label' => 'Accroche',
                'label_attr' => ['class' => 'bold'],
                'row_attr' => ['class' => HtmlService::MARGIN_FORM],
                'data' => $productType->getId() ? $productType->getBaseline() : '',
                'required' => false,
            ])
            ->add('text', TextareaType::class, [
                'label' => 'Description',
                'label_attr' => ['class' => 'bold'],
                'row_attr' => ['class' => HtmlService::MARGIN_FORM],
                'attr' => ['class' => 'tinymce'],
                'required' => false,
                'help' => 'Si aucune description, le bien est automatiquement désactivé',
                'help_attr' => ['class' => 'text-end'],
            ])
            ->add('active', CheckboxType::class, [
                'label' => false,
                'required' => false,
                'label_attr' => ['class' => ''],
                'row_attr' => ['class' => ' '],
                'block_prefix' => 'active',
            ])
            ->add('background', FileType::class, [
                'label' => false,
                'required' => false,
                'multiple' => false,
                'mapped' => false,
                'label_attr' => ['class' => 'bold'],
            ])
            ->add('token', HiddenType::class, [
                'data' => $productType->getToken(),
            ])
            ->add('sliderTitle', TextType::class, [
                'label' => 'Titre du slider',
                'required' => false,
                'label_attr' => ['class' => 'bold'],
                'row_attr' => ['class' => HtmlService::MARGIN_FORM],
                'help' => 'Apparait dans le slider en dessous de la marque (si vide, 
                "Nos offres [nom du type de biens]")',
            ])
            ->add('metaTitle', TextType::class, [
                'label' => 'Titre de le page',
                'required' => false,
                'label_attr' => ['class' => 'bold'],
                'row_attr' => ['class' => HtmlService::MARGIN_FORM],
                'help' => 'Apparait dans l\'onglet du navigateur (si vide, le nom du type de bien est utilisé)',
            ])
            ->add('metaDescription', TextareaType::class, [
                'label' => 'Description de la page',
                'required' => false,
                'attr' => ['rows' => 3],
                'label_attr' => ['class' => 'bold'],
                'row_attr' => ['class' => HtmlService::MARGIN_FORM],
                'help' => 'Description lue par les robots pour indexer le site (si vide, aucune description)',
            ])
            ->add('metaKeywords', TextType::class, [
                'label' => 'Mots clés de la page',
                'required' => false,
                'label_attr' => ['class' => 'bold'],
                'row_attr' => ['class' => ' '],
                'help' => 'Mots clés lus par les robots pour indexer le site 
                (séparer par des virgules / si vide, aucun mot clé)',
            ])
            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
                /** @var ProductType $productType */
                $productType = $event->getData();

                /** @var User $user */
                $user = $this->em->getRepository(User::class)->find(1);

                if (!$productType->getText()) {
                    $productType->setActive(false);
                }

                if (!$productType->getCreatedAt()) {
                    $productType
                        ->setCreatedAt(new \DateTime())
                        ->setCreatedBy($user)
                    ;
                }
                $productType
                    ->setUpdatedAt(new \DateTime())
                    ->setUpdatedBy($user)
                ;
            })
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ProductType::class,
        ]);
    }
}
