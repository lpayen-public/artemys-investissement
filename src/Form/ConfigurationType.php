<?php

namespace App\Form;

use App\Entity\Configuration;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConfigurationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => false,
                'label_attr' => ['class' => 'bold'],
                'row_attr' => ['class' => 'col-12 col-lg-6 col-xl-4 mb5'],
            ])
            ->add('value', TextType::class, [
                'label' => false,
                'label_attr' => ['class' => 'bold'],
                'row_attr' => ['class' => 'col-12 col-lg-6 col-xl-4 mb5'],
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Configuration::class,
        ]);
    }
}
