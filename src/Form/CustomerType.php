<?php

namespace App\Form;

use App\Entity\Customer;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Karser\Recaptcha3Bundle\Form\Recaptcha3Type;
use Karser\Recaptcha3Bundle\Validator\Constraints\Recaptcha3;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class CustomerType extends AbstractType
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly KernelInterface $kernel
    ) {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Customer $customer */
        $customer = $builder->getData();

        $builder
            ->add('lastname', TextType::class, [
                'label' => 'Nom',
                'label_attr' => ['class' => 'fw600'],
            ])
            ->add('firstname', TextType::class, [
                'label' => 'Prénom',
                'label_attr' => ['class' => 'fw600'],
            ])
            ->add('email', EmailType::class, [
                'label' => 'Email',
                'label_attr' => ['class' => 'fw600'],
            ])
            ->add('phone', TelType::class, [
                'label' => 'Téléphone',
                'label_attr' => ['class' => 'fw600'],
                'required' => false,
                'constraints' => [
                    new Assert\Regex([
                        'pattern' => '/^[0-9\+]{8,15}$/',
                        'message' => 'Erreur de format',
                    ]),
                ],
            ])
            ->add('text', TextareaType::class, [
                'label' => 'Message',
                'label_attr' => ['class' => 'fw600'],
                'required' => true,
                'attr' => ['rows' => 8],
            ])
            ->add('token', HiddenType::class, [
                'data' => $customer->getToken(),
            ])
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $datas = $event->getData();
                $datas['phone'] = \str_replace(' ', '', $datas['phone']);
                $event->setData($datas);
            })
            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
                /** @var Customer $customer */
                $customer = $event->getData();

                /** @var User $user */
                $user = $this->em->getRepository(User::class)->find(1);

                if (!$customer->getCreatedAt()) {
                    $customer
                        ->setCreatedAt(new \DateTime())
                        ->setCreatedBy($user)
                    ;
                }
                $customer
                    ->setUpdatedAt(new \DateTime())
                    ->setUpdatedBy($user)
                ;
            })
        ;

        if ('prod' === $this->kernel->getEnvironment()) {
            $builder
                ->add('captcha', Recaptcha3Type::class, [
                    'constraints' => new Recaptcha3(),
                    'action_name' => 'contact',
                    'locale' => 'fr',
                ])
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Customer::class,
        ]);
    }
}
