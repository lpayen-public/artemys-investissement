<?php

namespace App\Form;

use App\Entity\Feature;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $page = $options['data']['page'] ?? 1;

        $builder
            ->add('features', EntityType::class, [
                'label' => 'Caractéristiques',
                'required' => false,
                'placeholder' => 'Aucune donnée',
                'class' => Feature::class,
                'choice_label' => 'name',
                'query_builder' => function (EntityRepository $er): QueryBuilder {
                    return $er->createQueryBuilder('feature')
                        ->orderBy('feature.name', 'ASC')
                    ;
                },
                'expanded' => false,
                'multiple' => true,
                'row_attr' => ['class' => 'col-12 col-lg-6 col-xl-4 col-xxl-3 mb20'],
                'attr' => [
                    'data-choices' => 'data-choices',
                    'class' => 'form-control choices__input',
                    'name' => 'choices-single-default',
                ],
                'label_attr' => ['class' => 'bold'],
            ])
            ->add('type', EntityType::class, [
                'label' => 'Type',
                'class' => \App\Entity\ProductType::class,
                'choice_label' => 'name',
                'placeholder' => '-',
                'query_builder' => function (EntityRepository $er): QueryBuilder {
                    return $er->createQueryBuilder('type')
                        ->orderBy('type.name', 'ASC')
                    ;
                },
                'attr' => ['class' => 'no-search', 'data-choices' => 'data-choices'],
                'required' => false,
                'row_attr' => ['class' => 'col-12 col-lg-6 col-xl-4 col-xxl-3 mb20'],
                'label_attr' => ['class' => 'bold'],
            ])
            ->add('page', HiddenType::class, [
                'data' => $page,
            ])
//            ->add('name', TextType::class, [
//                'label' => 'Nom',
//                'row_attr' => ['class' => 'col-3'],
//                'required'      => false,
//            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => null,
            'csrf_protection' => false,
            'method' => 'GET',
        ]);
    }

    public function getBlockPrefix(): string
    {
        return '';
    }
}
