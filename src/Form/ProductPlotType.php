<?php

namespace App\Form;

use App\Entity\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductPlotType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('plots', CollectionType::class, [
                'label' => false,
                'entry_type' => PlotType::class,
                'entry_options' => [
                    'label' => false,
                    'attr' => ['class' => 'row js-product-plot-block mb20'],
                    'row_attr' => ['class' => 'js-product-plot-block'],
                ],
                'allow_add' => true,
                'allow_delete' => true,
                'attr' => ['class' => 'row'],
                'label_attr' => ['class' => 'row'],
                'row_attr' => ['class' => 'row'],
                'by_reference' => false,
                'prototype_name' => '__productPlot__',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
