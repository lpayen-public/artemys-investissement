<?php

namespace App;

use App\Service\Interface\PaymentInterface;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;

class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    public function __construct(string $environment, bool $debug)
    {
        \date_default_timezone_set('Europe/Paris');

        parent::__construct($environment, $debug);
    }

    protected function build(ContainerBuilder $container): void
    {
        parent::build($container);

        $container
            ->registerForAutoconfiguration(PaymentInterface::class)
            ->addTag('payment.provider');
    }
}
