<?php

namespace App\Command;

use App\Entity\City;
use App\Entity\Configuration;
use App\Entity\Customer;
use App\Entity\Department;
use App\Entity\Feature;
use App\Entity\Media;
use App\Entity\MediaType;
use App\Entity\Partner;
use App\Entity\Product;
use App\Entity\ProductQuestion;
use App\Entity\ProductType;
use App\Entity\User;
use App\Service\Slugger\SluggerUrlInterface;
use App\Utils\Text;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'ct:check:datas',
    description: 'Add a short description for your command',
)]
class CheckDatasCommand extends Command
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly SluggerUrlInterface $url,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('email', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->checkTokens();
        $this->checkSlugs();
        $this->checkConfs();

        $this->em->flush();

        $io->success('Datas checked');

        return Command::SUCCESS;
    }

    private function checkTokens(): void
    {
        $tab = [
            City::class,
            Customer::class,
            Department::class,
            Feature::class,
            Media::class,
            MediaType::class,
            Partner::class,
            Product::class,
            ProductType::class,
            ProductQuestion::class,
            User::class,
        ];

        foreach ($tab as $e) {
            $entites = $this->em->getRepository($e)->findBy(['token' => '']);
            foreach ($entites as $entity) {
                $entity->setToken(Text::token());
            }
        }
    }

    private function checkSlugs(): void
    {
        $products = $this->em->getRepository(Product::class)->findBy(['url' => '']);
        /** @var Product $product */
        foreach ($products as $product) {
            $this->url->slugProduct($product);
        }

        $productTypes = $this->em->getRepository(ProductType::class)->findBy(['url' => '']);
        /** @var ProductType $productType */
        foreach ($productTypes as $productType) {
            $this->url->slugProductType($productType);
        }
    }

    private function checkConfs(): void
    {
        foreach (Configuration::getWindowConfs() as $k => $v) {
            $conf = $this->em->getRepository(Configuration::class)->findOneBy(['name' => $k]);
            if (null === $conf) {
                $conf = (new Configuration())
                    ->setName($k)
                    ->setValue($v)
                ;
                $this->em->persist($conf);
            }
        }
    }
}
