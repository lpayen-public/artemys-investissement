<?php

namespace App\Command;

use App\Entity\Customer;
use App\Entity\User;
use App\Utils\Phone;
use App\Utils\Text;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'ct:customer:create',
    description: 'Création de clients',
)]
class CustomerCreateCommand extends Command
{
    private Generator $faker;
    private int $nbCustomers = 50;

    public function __construct(
        private readonly EntityManagerInterface $em,
        string $name = null
    ) {
        $this->faker = Factory::create('fr_FR');
        parent::__construct($name);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $user = $this->em->find(User::class, 1);
        $this->deleteCustomers();

        for ($i = 0; $i < $this->nbCustomers; ++$i) {
            $date = $this->faker->dateTimeBetween('-6 months');
            $customer = (new Customer())
                ->setLastname(\mb_strtoupper($this->faker->lastName()))
                ->setFirstname($this->faker->firstName())
                ->setEmail($this->faker->email)
                ->setPhone(Phone::clear($this->faker->phoneNumber))
                ->setText($this->faker->paragraphs(20, true))
                ->setToken(Text::token())
                ->setCreatedBy($user)
                ->setUpdatedBy($user)
                ->setCreatedAt($date)
                ->setUpdatedAt($date)
            ;

            $this->em->persist($customer);
        }

        $this->em->flush();

        return Command::SUCCESS;
    }

    private function deleteCustomers(): void
    {
        $customers = $this->em->getRepository(Customer::class)->findAll();
        foreach ($customers as $customer) {
            $this->em->remove($customer);
        }
    }

    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }
}
