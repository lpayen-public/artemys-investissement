<?php

namespace App\Command;

use App\Entity\City;
use App\Entity\Configuration;
use App\Entity\Feature;
use App\Entity\Media;
use App\Entity\MediaType;
use App\Entity\PlotStatus;
use App\Entity\Product;
use App\Entity\ProductQuestion;
use App\Entity\ProductResponse;
use App\Entity\ProductType;
use App\Entity\User;
use App\Service\Product\ProductMediaInterface;
use App\Service\Slugger\SluggerUrlInterface;
use App\Utils\File;
use App\Utils\Text;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\KernelInterface;

#[AsCommand(
    name: 'ct:db:init',
    description: 'Création des produits',
)]
class DatabaseInitCommand extends Command
{
    private Generator $faker;
    private SymfonyStyle $io;
    private string $root;
    private string $rootRessource;
    private int $nbProducts = 15;

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly SluggerUrlInterface $sluggerUrl,
        private readonly ProductMediaInterface $media,
        private readonly KernelInterface $kernel,
        string $name = null
    ) {
        $this->faker = Factory::create('fr_FR');
        $this->root = $this->kernel->getProjectDir() . '/public/';
        $this->rootRessource = $this->root . 'ressource/';
        parent::__construct($name);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->io = new SymfonyStyle($input, $output);

        $user = $this->em->find(User::class, 1);

        $this->delete();
        //        $this->em->flush();
        //        return Command::SUCCESS;

        $this->addConfs();
        $this->addMediaTypes($user);
        $this->addPlotStatus();
        $this->em->flush();

        $types = $this->addProductTypes($user);
        $questions = $this->addQuestions($user);
        $features = $this->addFeatures($user);
        $docs = $this->getFiles();

        for ($i = 0; $i < $this->nbProducts; ++$i) {
            $product = $this->setProduct($user, $types);
            $this->setFeatures($product, $features);
            $this->setQuestions($product, $questions);
            $this->setDocuments($product, $docs);
            $this->em->persist($product);
        }

        $this->em->flush();

        return Command::SUCCESS;
    }

    private function addConfs(): void
    {
        foreach (Configuration::getWindowConfs() as $k => $v) {
            $conf = $this->em->getRepository(Configuration::class)->findOneBy(['name' => $k]);
            if (null === $conf) {
                $conf = (new Configuration())
                    ->setName($k)
                    ->setValue($v);
                $this->em->persist($conf);
            }
        }
    }

    private function addProductTypes(User $user): array
    {
        $date = $this->faker->dateTimeBetween('-6 months');
        $tab = [];

        for ($i = 0; $i < 5; ++$i) {
            $type = (new ProductType())
                ->setToken(Text::token())
                ->setBaseline($this->faker->sentence())
                ->setSliderTitle($this->faker->sentence())
                ->setText($this->faker->paragraph(20))
                ->setName('Type ' . ($i + 1))
                ->setCreatedBy($user)
                ->setUpdatedBy($user)
                ->setCreatedAt($date)
                ->setUpdatedAt($date);

            $this->em->persist($type);
            $this->sluggerUrl->slugProductType($type);
            $tab[] = $type;
        }

        return $tab;
    }

    private function addQuestions(User $user): array
    {
        $date = $this->faker->dateTimeBetween('-6 months');
        $tab = [];

        for ($i = 0; $i < 5; ++$i) {
            $question = (new ProductQuestion())
                ->setToken(Text::token())
                ->setName('Question ' . ($i + 1))
                ->setCreatedBy($user)
                ->setUpdatedBy($user)
                ->setCreatedAt($date)
                ->setUpdatedAt($date);

            $this->em->persist($question);
            $tab[] = $question;
        }

        return $tab;
    }

    private function addFeatures(User $user): array
    {
        $date = $this->faker->dateTimeBetween('-6 months');
        $tab = [];

        for ($i = 0; $i < 5; ++$i) {
            $feature = (new Feature())
                ->setToken(Text::token())
                ->setName('Caractéristique ' . ($i + 1))
                ->setCreatedBy($user)
                ->setUpdatedBy($user)
                ->setCreatedAt($date)
                ->setUpdatedAt($date);

            $this->em->persist($feature);
            $tab[] = $feature;
        }

        return $tab;
    }

    private function addPlotStatus(): void
    {
        $status = $this->em->getRepository(PlotStatus::class)->findAll();

        if (count($status)) {
            return;
        }

        $tab = [
            'Disponible',
            'Réservé',
            'Vendu',
            'A venir',
        ];

        foreach ($tab as $name) {
            $status = (new PlotStatus())
                ->setName($name);

            $this->em->persist($status);
        }
    }

    private function addMediaTypes(User $user): void
    {
        $types = $this->em->getRepository(MediaType::class)->findAll();

        if (count($types)) {
            return;
        }

        $date = $this->faker->dateTimeBetween('-6 months');

        $tab = [
            'Image',
            'Plan',
            'Plaquette commerciale',
        ];

        foreach ($tab as $k => $name) {
            $mediaType = (new MediaType())
                ->setName($name)
                ->setToken(Text::token())
                ->setOrder($k)
                ->setCreatedBy($user)
                ->setUpdatedBy($user)
                ->setCreatedAt($date)
                ->setUpdatedAt($date);

            $this->em->persist($mediaType);
        }
    }

    private function delete(): void
    {
        $products = $this->em->getRepository(Product::class)->findAll();

        /** @var Product $product */
        foreach ($products as $product) {
            /** @var Media $media */
            foreach ($product->getMedias() as $media) {
                $url = $this->root . $media->getUrl();
                if (File::exists($url)) {
                    File::delete($url, true);
                }
            }
            $this->em->remove($product);
        }

        $types = $this->em->getRepository(ProductType::class)->findAll();
        foreach ($types as $type) {
            $this->em->remove($type);
        }

        $features = $this->em->getRepository(Feature::class)->findAll();
        foreach ($features as $feature) {
            $this->em->remove($feature);
        }

        $questions = $this->em->getRepository(ProductQuestion::class)->findAll();
        foreach ($questions as $question) {
            $this->em->remove($question);
        }

        $this->em->flush();
    }

    /**
     * @return array|array[]
     */
    private function getFiles(): array
    {
        $docs = [
            'pictures' => [],
            'pdfs' => [],
        ];

        foreach (\glob($this->rootRessource . '*') as $file) {
            if (!\is_file($file)) {
                continue;
            }
            if (str_contains($file, 'jpg')) {
                $docs['pictures'][] = $file;
                continue;
            }
            if (str_contains($file, 'pdf')) {
                $docs['pdfs'][] = $file;
            }
        }

        return $docs;
    }

    private function setDocuments(Product $product, array $docs): void
    {
        $nbPictures = rand(1, 5);
        $nbBrochurePdfs = rand(1, 2);
        $nbPlanPdfs = rand(1, 3);

        $pictures = $this->getDocuments($docs['pictures'], $nbPictures);
        $brochures = $this->getDocuments($docs['pdfs'], $nbBrochurePdfs);
        $plans = $this->getDocuments($docs['pdfs'], $nbPlanPdfs);

        $this->media->upload($product, false, $pictures, $brochures, $plans);
    }

    private function setFeatures(Product $product, array $features): void
    {
        $nbFeatures = \rand(1, count($features) - 1);

        for ($j = 0; $j < $nbFeatures; ++$j) {
            /** @var Feature $feature */
            $feature = $features[\rand(0, count($features) - 1)];
            $product->addFeature($feature);
        }
    }

    private function setQuestions(Product $product, array $questions): void
    {
        $yesNo = ['Oui', 'Non'];

        /** @var ProductQuestion $question */
        foreach ($questions as $k => $question) {
            if (!$k) {
                $value = \rand(500, 5000);
            } elseif (1 === $k) {
                $value = \rand(2, 8);
            } else {
                $value = $yesNo[\rand(0, 1)];
            }

            $response = (new ProductResponse())
                ->setProduct($product)
                ->setQuestion($question)
                ->setValue($value);

            $product->addResponse($response);
        }
    }

    private function setProduct(User $user, array $types): Product
    {
        /** @var ProductType $type */
        $type = $types[\rand(0, count($types) - 1)];

        /** @var City $city */
        $city = $this->em->getRepository(City::class)->find(\rand(1000, 30000));

        $date = $this->faker->dateTimeBetween('-6 months');

        $price = rand(25000, 300000);

        $product = (new Product())
            ->setName($this->faker->sentence(4))
            ->setText($this->faker->paragraph(10))
            ->setToken(Text::token())
            ->setCity($city)
            ->setType($type)
            ->setPrice($price)
            ->setCreatedBy($user)
            ->setUpdatedBy($user)
            ->setCreatedAt($date)
            ->setUpdatedAt($date);

        $this->sluggerUrl->slugProduct($product);
        $this->io->text($product->getToken());

        return $product;
    }

    private function getDocuments(array $documents, int $nbDocuments): array
    {
        $tab = [];
        for ($i = 0; $i < $nbDocuments; ++$i) {
            $path = $documents[rand(0, count($documents) - 1)];
            $name = str_replace($this->rootRessource, '', $path);
            $tab[] = new UploadedFile($path, $name, null, null, true);
        }

        return $tab;
    }

    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description');
    }
}
