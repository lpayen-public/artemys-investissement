<?php

namespace App\Repository;

use App\Entity\ProductType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ProductType>
 *
 * @method ProductType|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductType|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductType[]    findAll()
 * @method ProductType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductType::class);
    }

    /**
     * @throws NonUniqueResultException
     */
    public function getProductTypeByOldUrl(string $url): ?ProductType
    {
        $qb = $this
            ->createQueryBuilder('productType')
            ->join('productType.urls', 'url')
            ->andWhere('url.value = :url')
            ->setParameter('url', $url)
            ->setMaxResults(1)
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function search(string $text)
    {
        return $this->createQueryBuilder('productType')
            ->andWhere('productType.name LIKE :search')
            ->orWhere('productType.baseline LIKE :search')
            ->setParameter('search', '%' . $text . '%')
            ->getQuery()
            ->getResult()
        ;
    }
}
