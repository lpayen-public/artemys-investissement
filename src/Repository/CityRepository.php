<?php

namespace App\Repository;

use App\Entity\City;
use App\Utils\Text;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<City>
 *
 * @method City|null find($id, $lockMode = null, $lockVersion = null)
 * @method City|null findOneBy(array $criteria, array $orderBy = null)
 * @method City[]    findAll()
 * @method City[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, City::class);
    }

    /**
     * @return array<string, \App\Entity\City>
     */
    public function like(string $search): array
    {
        $spacedSearch = Text::trim(Text::lower(\str_replace('-', ' ', $search)));

        $qb = $this->createQueryBuilder('city')
            ->andWhere(
                '
                city.name LIKE :search
                OR city.zipcode LIKE :search
                OR city.spacedName LIKE :spacedSearch
            '
            )
            ->andWhere('city.name NOT LIKE :exclude')
            ->setParameters([
                'search' => $search . '%',
                'spacedSearch' => $spacedSearch . '%',
                'exclude' => '%CEDEX%',
            ])
            ->orderBy('city.name', 'ASC')
            ->orderBy('city.zipcode', 'ASC')
        ;

        $tab = $this->doublon($qb->getQuery()->getResult());

        $qb = $this->createQueryBuilder('city')
            ->andWhere(
                '
                city.name LIKE :search
                OR city.zipcode LIKE :search
                OR city.spacedName LIKE :spacedSearch
            '
            )
            ->andWhere('city.name NOT LIKE :exclude')
            ->setParameters([
                'search' => '%' . $search . '%',
                'spacedSearch' => '%' . $spacedSearch . '%',
                'exclude' => '%CEDEX%',
            ])
            ->orderBy('city.name', 'ASC')
            ->orderBy('city.zipcode', 'ASC')
        ;

        return $this->doublon($qb->getQuery()->getResult(), $tab);
    }

    /**
     * @param array<int, \App\Entity\City> $entities
     * @param array<string, City>          $tab
     *
     * @return array<string, City>
     */
    private function doublon(array $entities, array $tab = []): array
    {
        /** @var City $entity */
        foreach ($entities as $entity) {
            $concat = $entity->getName() . $entity->getZipcode();
            if (!isset($tab[$concat])) {
                $tab[$concat] = $entity;
            }
        }

        return $tab;
    }
}
