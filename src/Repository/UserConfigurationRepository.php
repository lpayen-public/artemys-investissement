<?php

namespace App\Repository;

use App\Entity\Configuration;
use App\Entity\User;
use App\Entity\UserConfiguration;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<UserConfiguration>
 *
 * @method UserConfiguration|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserConfiguration|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserConfiguration[]    findAll()
 * @method UserConfiguration[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserConfigurationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserConfiguration::class);
    }

    public function getDQL(?User $user): array
    {
        if (null === $user) {
            return [];
        }

        $qb = $this->createQueryBuilder('userConfiguration')
            ->join('userConfiguration.configuration', 'configuration')
            ->select('configuration.name, userConfiguration.value')
            ->andWhere('userConfiguration.user = :user')
            ->setParameter('user', $user)
        ;

        $tab = [];
        $results = $qb->getQuery()->getResult();
        foreach ($results as $r) {
            $tab[$r['name']] = $r['value'];
        }

        return $tab;
    }

    public function getUserColor(User $user): ?string
    {
        if (null === $user->getId()) {
            return null;
        }

        $qb = $this->createQueryBuilder('userConfiguration')
            ->join('userConfiguration.configuration', 'configuration')
            ->select('userConfiguration.value')
            ->andWhere('configuration.name = :configName')
            ->andWhere('userConfiguration.user = :user')
            ->setParameter('configName', Configuration::WINDOW_COLOR)
            ->setParameter('user', $user)
            ->setMaxResults(1)
        ;

        $results = $qb->getQuery()->getResult();

        return count($results) ? $results[0]['value'] : null;
    }
}
