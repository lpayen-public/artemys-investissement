<?php

namespace App\Repository;

use App\Entity\Media;
use App\Entity\MediaType;
use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Media>
 *
 * @method Media|null find($id, $lockMode = null, $lockVersion = null)
 * @method Media|null findOneBy(array $criteria, array $orderBy = null)
 * @method Media[]    findAll()
 * @method Media[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MediaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Media::class);
    }

    public function getDocumentsByProduct(Product $product): array
    {
        $qb = $this->createQueryBuilder('media')
            ->select('media.token, media.url, type.token as typeToken, type.name as typeName, type.icon as typeIcon')
            ->join('media.type', 'type')
            ->andWhere('media.product = :product')
            ->andWhere('media.type != :typeId')
            ->setParameters([
                'product' => $product,
                'typeId' => MediaType::PICTURE,
            ])
            ->addOrderBy('type.order', 'ASC')
            ->addOrderBy('media.order', 'ASC')
        ;

        return $qb->getQuery()->getResult();
    }
}
