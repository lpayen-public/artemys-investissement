<?php

namespace App\Repository;

use App\Entity\PlotStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<PlotStatus>
 *
 * @method PlotStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlotStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlotStatus[]    findAll()
 * @method PlotStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlotStatusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlotStatus::class);
    }

    //    /**
    //     * @return PlotStatus[] Returns an array of PlotStatus objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('p.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?PlotStatus
    //    {
    //        return $this->createQueryBuilder('p')
    //            ->andWhere('p.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
