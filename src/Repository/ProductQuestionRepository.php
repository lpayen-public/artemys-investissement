<?php

namespace App\Repository;

use App\Entity\ProductQuestion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ProductQuestion>
 *
 * @method ProductQuestion|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductQuestion|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductQuestion[]    findAll()
 * @method ProductQuestion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductQuestionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductQuestion::class);
    }

    //    /**
    //     * @return ProductQuestion[] Returns an array of ProductQuestion objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('q')
    //            ->andWhere('q.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('q.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?ProductQuestion
    //    {
    //        return $this->createQueryBuilder('q')
    //            ->andWhere('q.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
