<?php

namespace App\Repository;

use App\Entity\Media;
use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Product>
 *
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * @return Product[] Returns an array of Product objects
     */
    public function getByFilters(array $datas = []): array
    {
        $features = $datas['features'] ?? [];
        $type = $datas['type'] ?? null;

        $qb = $this->createQueryBuilder('product')
            ->orderBy('product.id', 'DESC')
        ;

        if (count($features)) {
            $qb->join('product.features', 'feature');
            foreach ($features as $feature) {
                $qb->andWhere('feature IN (:feature)')
                    ->setParameter('feature', $feature)
                ;
            }
        }

        if ($type) {
            $qb->andWhere('product.type = :type')
                ->setParameter('type', $type)
            ;
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @throws NonUniqueResultException
     */
    public function getProductByOldUrl(string $url): ?Product
    {
        $qb = $this
            ->createQueryBuilder('product')
            ->join('product.urls', 'url')
            ->andWhere('url.value = :url')
            ->setParameter('url', $url)
            ->setMaxResults(1)
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @throws NonUniqueResultException
     */
    public function getProductByMedia(Media $media): ?Product
    {
        $qb = $this
            ->createQueryBuilder('product')
            ->join('product.medias', 'media')
            ->andWhere('media = :media')
            ->setParameter('media', $media)
            ->setMaxResults(1)
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function search(string $text)
    {
        return $this->createQueryBuilder('product')
            ->andWhere('product.name LIKE :search')
            ->orWhere('product.text LIKE :search')
            ->setParameter('search', '%' . $text . '%')
            ->getQuery()
            ->getResult()
        ;
    }
}
