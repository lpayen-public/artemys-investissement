<?php

namespace App\EventSubscriber;

use App\Event\UserEvent;
use App\Service\Mailer;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UserSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly Mailer $mailer,
    ) {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            UserEvent::USER_ADD => [
                ['sendEmailUserAdd'],
            ],
            //            UserEvent::USER_UPDATE => [
            //                ['sendEmailUserUpdate'],
            //            ],
        ];
    }

    public function sendEmailUserAdd(UserEvent $event): void
    {
        $this->logger->info('USER ADD : ' . $event->getUser()->getName());
        $this->mailer->addUser($event->getUser());
    }

    public function sendEmailUserUpdate(UserEvent $event): void
    {
        $this->logger->info('USER UPDATE : ' . $event->getUser()->getName());
    }
}
