<?php

namespace App\EventSubscriber;

use App\Event\ProductTypeEvent;
use App\Service\Mailer;
use App\Service\Sms;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Notifier\Exception\TransportExceptionInterface;

class ProductTypeSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly Mailer $mailer,
        private readonly Sms $sms
    ) {
    }

    /**
     * @return array[]
     */
    public static function getSubscribedEvents(): array
    {
        return [
            //            ProductTypeEvent::PRODUCT_TYPE_ADD    => [
            //                ['sendEmailProductType'],
            //            ],
            //            ProductTypeEvent::PRODUCT_TYPE_UPDATE => [
            //                ['sendEmailProductType'],
            //                ['sendSmsProductType'],
            //            ],
            ProductTypeEvent::PRODUCT_TYPE_DELETE => [
                ['sendEmailProductType'],
            ],
        ];
    }

    public function sendEmailProductType(ProductTypeEvent $productTypeEvent): void
    {
        $this->logger->info('sendEmailProductType: ' . $productTypeEvent->getProductType()->getName());
        $this->mailer->addProductType($productTypeEvent->getProductType());
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function sendSmsProductType(ProductTypeEvent $productTypeEvent): void
    {
        $this->logger->info('sendSmsProductType: ' . $productTypeEvent->getProductType()->getName());
        $this->sms->newProductType($productTypeEvent->getProductType());
    }
}
