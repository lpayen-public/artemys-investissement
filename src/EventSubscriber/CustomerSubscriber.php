<?php

namespace App\EventSubscriber;

use App\Event\CustomerEvent;
use App\Service\Mailer;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CustomerSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly Mailer $mailer,
    ) {
    }

    /**
     * @return array[]
     */
    public static function getSubscribedEvents(): array
    {
        return [
            CustomerEvent::CUSTOMER_ADD => [
                ['sendEmailCustomer'],
            ],
        ];
    }

    public function sendEmailCustomer(CustomerEvent $event): void
    {
        $customer = $event->getCustomer();
        $this->logger->info('sendEmailCustomer: ' . $customer->getName());
        $this->mailer->addCustomer($event->getCustomer());
    }
}
