<?php

namespace App\EventSubscriber;

use App\Service\ConfigurationService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ControllerSubscriber implements EventSubscriberInterface
{
    public function __construct(private readonly ConfigurationService $configurationService)
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::CONTROLLER => 'onKernelController',
        ];
    }

    public function onKernelController(ControllerEvent $event): void
    {
        $route = $event->getRequest()->get('_route');

        if (null === $route || '_wdt' === $route) {
            return;
        }

        $this->configurationService->load();
    }
}
