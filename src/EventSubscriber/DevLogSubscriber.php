<?php

namespace App\EventSubscriber;

use App\Utils\File;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\KernelEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\KernelInterface;

class DevLogSubscriber implements EventSubscriberInterface
{
    public function __construct(private readonly KernelInterface $kernel)
    {
    }

    /**
     * @return array[]
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => ['onKernelRequest', 5000],
        ];
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        if ($this->isDebugBar($event)) {
            return;
        }

        if ('dev' !== $this->kernel->getEnvironment()) {
            return;
        }

        $path = $this->kernel->getLogDir() . '/dev.log';
        if ((int) File::getKiloOctet($path) > 100) {
            \file_put_contents($path, '');
        }
    }

    private function isDebugBar(KernelEvent $event): bool
    {
        return str_contains($event->getRequest()->getRequestUri(), '_wdt');
    }
}
