<?php

namespace App\EventSubscriber;

use App\Event\ProductEvent;
use App\Service\Mailer;
use App\Service\Sms;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Notifier\Exception\TransportExceptionInterface;

class ProductSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly Mailer $mailer,
        private readonly Sms $sms
    ) {
    }

    /**
     * @return array[]
     */
    public static function getSubscribedEvents(): array
    {
        return [
            //            ProductEvent::PRODUCT_ADD    => [
            //                ['sendEmailProduct'],
            //            ],
            //            ProductEvent::PRODUCT_UPDATE => [
            //                ['sendEmailProduct'],
            //                ['sendSmsProduct'],
            //            ],
            ProductEvent::PRODUCT_DELETE => [
                ['sendEmailProduct'],
            ],
        ];
    }

    public function sendEmailProduct(ProductEvent $productEvent): void
    {
        $this->logger->info('sendEmailProduct: ' . $productEvent->getProduct()->getName());
        $this->mailer->addProduct($productEvent->getProduct());
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function sendSmsProduct(ProductEvent $productEvent): void
    {
        $this->logger->info('sendSmsProduct: ' . $productEvent->getProduct()->getName());
        $this->sms->newProduct($productEvent->getProduct());
    }
}
