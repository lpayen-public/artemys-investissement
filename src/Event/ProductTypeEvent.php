<?php

namespace App\Event;

use App\Entity\ProductType;
use Symfony\Contracts\EventDispatcher\Event;

class ProductTypeEvent extends Event
{
    public const PRODUCT_TYPE_ADD = 'productType.add';
    public const PRODUCT_TYPE_UPDATE = 'productType.update';
    public const PRODUCT_TYPE_DELETE = 'productType.delete';

    private ProductType $productType;

    public function __construct(ProductType $productType)
    {
        $this->productType = $productType;
    }

    public function getProductType(): ProductType
    {
        return $this->productType;
    }
}
