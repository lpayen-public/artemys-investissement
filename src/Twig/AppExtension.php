<?php

declare(strict_types=1);

namespace App\Twig;

use App\Service\Admin\HtmlService;
use App\Utils\Number;
use App\Utils\Text;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

use function print_r;

class AppExtension extends AbstractExtension
{
    public function __construct(private readonly HtmlService $html)
    {
    }

    /**
     * @return TwigFilter[]
     */
    public function getFilters(): array
    {
        return [
            new TwigFilter('dateFr', [$this, 'getDateFr']),
            new TwigFilter('dateHeure', [$this, 'getDateHeure']),
            new TwigFilter('yesNo', [$this, 'getYesNo'], ['is_safe' => ['html']]),
            new TwigFilter('yes', [$this, 'getYes'], ['is_safe' => ['html']]),
            new TwigFilter('phone', [$this, 'getPhone']),
            new TwigFilter('firstUpper', [$this, 'getFirstUpper']),
            new TwigFilter('price', [$this, 'getPrice']),
            new TwigFilter('pr', [$this, 'getPr']),
            new TwigFilter('prArray', [$this, 'getPrArray']),
            new TwigFilter('float', [$this, 'getFloat']),
            new TwigFilter('percent', [$this, 'getPercent']),
            new TwigFilter('highLight', [$this, 'getHighLight']),
            new TwigFilter('pluriel', [$this, 'getPluriel']),
            new TwigFilter('floatNull', [$this, 'getFloatNull']),
            new TwigFilter('nbHoursWeek', [$this, 'getNbHoursWeek']),
            new TwigFilter('int', [$this, 'getInt']),
            new TwigFilter('upDown', [$this, 'getUpDown']),
            new TwigFilter('arrowUpDown', [$this, 'getArrowUpDown']),
            new TwigFilter('intro', [$this, 'getIntro']),
        ];
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('token', [$this, 'getToken']),
            new TwigFunction('random', [$this, 'getRandom']),
            new TwigFunction('show', [$this, 'show'], ['is_safe' => ['html']]),
            new TwigFunction('update', [$this, 'update'], ['is_safe' => ['html']]),
            new TwigFunction('delete', [$this, 'delete'], ['is_safe' => ['html']]),
            new TwigFunction('visit', [$this, 'visit'], ['is_safe' => ['html']]),
            new TwigFunction('return', [$this, 'return'], ['is_safe' => ['html']]),
            new TwigFunction('back', [$this, 'back'], ['is_safe' => ['html']]),
            new TwigFunction('submit', [$this, 'submit'], ['is_safe' => ['html']]),
            new TwigFunction('alert', [$this, 'alert'], ['is_safe' => ['html']]),
        ];
    }

    public function getDateFr(?\DateTime $date): string
    {
        if (null === $date) {
            return '';
        }

        return $date->format('d/m/Y');
    }

    public function getDateHeure(?\DateTime $date, bool $withSeconds = false): string
    {
        if (null === $date) {
            return '';
        }

        return !$withSeconds ? $date->format('d/m/Y H\\hi') : $date->format('d/m/Y H\\hi\'s');
    }

    /**
     * @param array<int, string> $answers
     */
    public function getYesNo(
        $isTrue,
        bool $withText = true,
        bool $justYes = false,
        array $answers = ['Oui', 'Non']
    ): string {
        if (\is_bool($isTrue) || \is_numeric($isTrue)) {
            $isTrue = (int) $isTrue;
        } else {
            $isTrue = (int) !empty($isTrue);
        }

        if ($justYes && !$isTrue) {
            return '';
        }

        $class = $isTrue ? 'check-line text-success bold' : 'close-line text-danger bold';
        $class .= $withText ? ' mr5' : '';
        $yesNo = $withText ? ($isTrue ? $answers[0] : $answers[1]) : '';

        return '<i class="ri-' . $class . ' fs16"></i>' . $yesNo;
    }

    public function getYes($isTrue, bool $withText = true): string
    {
        return $this->getYesNo($isTrue, $withText, true);
    }

    public function getPhone(?string $phone, string $sep = ' '): string
    {
        if (null === $phone) {
            return '';
        }

        $phone = \str_replace(' ', '', $phone);

        if (!str_contains($phone, '+') && 10 == \strlen($phone)) {
            return \wordwrap($phone, 2, $sep, true);
        }

        if (12 === \strlen($phone)) {
            $indicatif = \substr($phone, 0, 3);
            $code = \substr($phone, 3, 1);
            $reste = \str_replace($indicatif . $code, '', $phone);

            if ('+33' === $indicatif) {
                $indicatif = '0';
            } else {
                $indicatif .= ' ';
            }

            return $indicatif . $code . ' ' . \wordwrap($reste, 2, $sep, true);
        }

        return (string) $phone;
    }

    public function getFirstUpper(string $text = null): ?string
    {
        return Text::majAll($text);
    }

    public function getPrice(string $text, int $nbDecimal = 2, bool $withEuro = true): string
    {
        $price = $this->getFloat($text, $nbDecimal);

        return $price . ($withEuro ? ' €' : '');
    }

    public function getInt(string $text, bool $withEuro = true): string
    {
        $text = (int) $text;

        return \number_format($text, 0, ',', ' ');
    }

    public function getFloat(string $text, int $nbDecimal = 2): string
    {
        return \number_format(Number::toFloat($text), $nbDecimal, ',', ' ');
    }

    public function getFloatNull(string $text): string
    {
        if ('' === $text) {
            return '';
        }

        return $this->getFloat($text);
    }

    public function getPr($text = null, string $comment = null): void
    {
        if (null === $text) {
            $text = 'NULL';
        } elseif (!$text && !\is_numeric($text)) {
            $text = !\is_array($text) ? 'false' : '[]';
        } elseif ($text && !\is_array($text) && !\is_numeric($text)) {
            true === $text ? 'true' : $text;
        }

        echo '<div class="debug-block"><div class="debug-pr"><pre class="debug-pre">';
        if ($comment) {
            echo '<div class="debug-comment">';
            \print_r('› ' . $comment);
            echo '</div>';
        }
        echo '<div class="debug-content">';
        \print_r($text);
        echo '</div>';
        echo '</pre></div></div>';
    }

    public function getPrArray($text): void
    {
        \print_r($text);
        //        if (!isset($tab['text'])) {
        //            $tab['text'] = '???';
        //        }
        //
        //        echo '<div class="debug-pr">';
        //        echo '<pre class="debug-pre">';
        //        if (!empty($tab['title'])) {
        //            echo '<div class="debug-comment text-success">';
        //            print_r('› ' . $tab['title']);
        //            echo '</div>';
        //        }
        //        echo '<div class="debug-content">';
        //        print_r($tab['text']);
        //        echo '</div>';
        //        echo '</pre>';
        //        echo '</div>';
    }

    public function getPercent(string $text): string
    {
        $price = $this->getFloat($text);

        return $price . ' %';
    }

    public function getToken(int $length = 30): string
    {
        return Text::token($length);
    }

    public function getHighLight(
        ?string $text,
        string $search,
        string $bg = 'fafa7b',
        string $color = null,
        bool $isBold = false
    ): ?string {
        if (null === $text) {
            return null;
        }

        $search = \str_replace('+', '', $search);
        $replace = '<span class="' . ($isBold ? 'bold' : '') . '" ';
        $replace .= 'style="background:#' . $bg . ($color ? ';color:#' . $color : '') . ';">$1</span>';

        return \preg_replace('#(' . \stripslashes($search) . ')#i', $replace, \stripslashes($text));
    }

    public function getRandom(int $from = 0, int $to = 100): int
    {
        return Number::random($from, $to);
    }

    public function getPluriel(int $nb, string $text, string $letter = 's'): string
    {
        return $nb . ' ' . $text . (!\in_array($nb, [0, 1]) ? $letter : '');
    }

    public function getNbHoursWeek(float $nb): string
    {
        $nbHours = \floor($nb);
        $decimal = $nb - $nbHours;
        $minutes = $decimal * 60;
        $minutes = $minutes ?: '00';

        return $nbHours . 'h' . $minutes;
    }

    public function getUpDown(float $nb): string
    {
        $class = $nb > 0 ? 'text-success' : ($nb < 0 ? 'text-danger' : 'color-9');
        $icon = $nb > 0 ? 'up' : ($nb < 0 ? 'down' : 'neutral');

        $text = '<span class="' . $class . ' fs-13 mb-0">';
        $text .= '<i class="mdi mdi-trending-' . $icon . ' align-middle me-1"></i>';
        $text .= $this->getPercent((string) $nb);
        $text .= '</span>';

        return $text;
    }

    public function getIntro(?string $text, int $nbCharacters = 20, bool $force = false, string $end = '...'): ?string
    {
        if (null === $text || \strlen($text) <= $nbCharacters) {
            return $text;
        }

        if ($force) {
            return \substr($text, 0, $nbCharacters) . $end;
        }

        $pos = \strpos($text, ' ', $nbCharacters);

        if (!$pos) {
            return $text;
        }

        return \substr($text, 0, $pos) . $end;
    }

    public function show(string $path): string
    {
        return $this->link($path, $this->html->getBtnShow('xs'), 'Voir', 'ri-eye-line');
    }

    public function update(string $path): string
    {
        return $this->link($path, $this->html->getBtnUpdate('xs'), 'Modifier', 'ri-pencil-line');
    }

    public function visit(string $path): string
    {
        return $this->link($path, $this->html->getBtnShow('xs'), 'Voir sur le site Internet', 'ri-ie-line', true);
    }

    public function back(string $path): string
    {
        return $this->link($path, $this->html->getBtnBack('xs'), 'Retour', 'ri-arrow-left-line');
    }

    public function submit(bool $justIcon = true, bool $redirect = true): string
    {
        $class = $justIcon || $redirect
            ? $this->html->getBtnSave($justIcon ? 'xs' : 'sm')
            : $this->html->getBtnAdd('sm');

        $title = $justIcon ? 'title="Enregistrer"' : '';
        $icon = 'class="ri-save-line"';
        $text = $justIcon ? "<i {$icon}></i>" : ($redirect ? 'Enregistrer' : 'Appliquer');
        $class .= $redirect ? '' : ' js-submit-stay';
        $type = $redirect ? 'submit' : 'button';

        return "<button type='{$type}' class='{$class}' data-bs-toggle='tooltip' {$title}>{$text}</button>";
    }

    public function delete(string $path = null): string
    {
        $class = 'class="' . $this->html->getBtnDelete('xs') . ' js-btn-delete"';
        $title = 'title="Supprimer"';
        $icon = 'class="ri-delete-bin-line"';

        return "<button type='button' {$class} data-bs-toggle='tooltip' {$title}><i {$icon}></i></button>";
    }

    private function link(string $path, string $class, string $text, string $icon, bool $isBlankTarget = false): string
    {
        $href = 'href="' . $path . '"';
        $class = 'class="' . $class . '"';
        $title = 'title="' . $text . '"';
        $target = $isBlankTarget ? 'target="_blank"' : '';
        $icon = 'class="' . $icon . '"';

        return "<a {$href} {$class} data-bs-toggle='tooltip' {$title} {$target}><i {$icon}></i></a>";
    }

    public function alert(string $message, string $type = 'danger'): string
    {
        $text = '<div class="alert alert-' . $type . '">';
        $text .= '<i class="ri-alarm-warning-line fs16 mr10"></i>';
        $text .= $message;
        $text .= '</div>';

        return $text;
    }
}
