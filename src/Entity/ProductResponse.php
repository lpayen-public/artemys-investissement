<?php

namespace App\Entity;

use App\Repository\ProductResponseRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ProductResponseRepository::class)]
class ProductResponse
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'responses')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Product $product = null;

    #[ORM\ManyToOne(inversedBy: 'responses')]
    #[ORM\JoinColumn(nullable: false)]
    private ?ProductQuestion $question = null;

    #[ORM\Column(length: 255)]
    #[Assert\Length(min: 1)]
    private ?string $value = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuestion(): ?ProductQuestion
    {
        return $this->question;
    }

    public function setQuestion(?ProductQuestion $question): static
    {
        $this->question = $question;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): static
    {
        $this->value = $value;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): static
    {
        $this->product = $product;

        return $this;
    }
}
