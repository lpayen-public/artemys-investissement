<?php

namespace App\Entity;

use App\Repository\ProductTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProductTypeRepository::class)]
class ProductType
{
    use PropertyTrait;
    use MetaTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'type', targetEntity: Product::class, cascade: ['remove'])]
    private Collection $products;

    #[ORM\Column(length: 255, unique: true)]
    private ?string $url = null;

    #[ORM\ManyToMany(targetEntity: Url::class, inversedBy: 'productTypes', cascade: ['persist', 'remove'])]
    private Collection $urls;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $baseline = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $text = null;

    #[ORM\ManyToMany(targetEntity: Media::class, inversedBy: 'productTypes', cascade: ['persist', 'remove'])]
    #[ORM\OrderBy(['order' => 'ASC'])]
    private Collection $medias;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $sliderTitle = null;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->urls = new ArrayCollection();
        $this->medias = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Product>
     */
    public function getProducts(bool $onlyActive = false): Collection
    {
        if (!$onlyActive) {
            return $this->products;
        }

        return $this->products->filter(function (Product $product) {
            return $product->getActive();
        });
    }

    /**
     * @return $this
     */
    public function addProduct(Product $product): static
    {
        if (!$this->products->contains($product)) {
            $this->products->add($product);
            $product->setType($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeProduct(Product $product): static
    {
        if ($this->products->removeElement($product)) {
            // set the owning side to null (unless already changed)
            if ($product->getType() === $this) {
                $product->setType(null);
            }
        }

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @return $this
     */
    public function setUrl(string $url): static
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return Collection<int, Url>
     */
    public function getUrls(): Collection
    {
        return $this->urls;
    }

    /**
     * @return $this
     */
    public function addUrl(Url $url): static
    {
        if (!$this->urls->contains($url)) {
            $this->urls->add($url);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeUrl(Url $url): static
    {
        $this->urls->removeElement($url);

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @return $this
     */
    public function setText(?string $text): static
    {
        $this->text = $text;

        return $this;
    }

    public function getBaseline(): ?string
    {
        return $this->baseline ?: 'Découvrez nos offres "' . $this->getName() . '"';
    }

    /**
     * @return $this
     */
    public function setBaseline(?string $baseline): static
    {
        $this->baseline = $baseline;

        return $this;
    }

    /**
     * @return Collection<int, Media>
     */
    public function getMedias(): Collection
    {
        return $this->medias;
    }

    /**
     * @return $this
     */
    public function addMedia(Media $media): static
    {
        if (!$this->medias->contains($media)) {
            $this->medias->add($media);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeMedia(Media $media): static
    {
        $this->medias->removeElement($media);

        return $this;
    }

    public function getBackground(): ?string
    {
        return count($this->getMedias())
            ? $this->getMedias()->get(0)->getUrl()
            : 'img/4.jpg';
    }

    public function getSliderTitle(): ?string
    {
        return $this->sliderTitle ?: 'Nos offres ' . $this->getName();
    }

    public function setSliderTitle(?string $sliderTitle): static
    {
        $this->sliderTitle = $sliderTitle;

        return $this;
    }

    public function getPictures(): Collection
    {
        return $this->getMedias();
    }
}
