<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ProductRepository::class)]
class Product
{
    use PropertyTrait;
    use MetaTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotBlank()]
    #[Assert\Length(min: 5, max: 255, minMessage: '10 caractères minimum', maxMessage: '255 caractères maximum')]
    private ?string $name = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $text = null;

    #[ORM\Column(length: 255, unique: true)]
    private ?string $url = null;

    #[ORM\Column(options: ['default' => 1])]
    private bool $available = true;

    #[ORM\ManyToOne(inversedBy: 'products')]
    #[ORM\JoinColumn(nullable: false)]
    private ?ProductType $type = null;

    #[ORM\OneToMany(mappedBy: 'product', targetEntity: ProductResponse::class, cascade: ['persist', 'remove'])]
    private Collection $responses;

    #[ORM\ManyToOne(inversedBy: 'products')]
    private ?City $city = null;

    #[ORM\ManyToMany(targetEntity: Feature::class, inversedBy: 'products', cascade: ['persist'])]
    #[ORM\OrderBy(['name' => 'ASC'])]
    private Collection $features;

    #[ORM\ManyToMany(targetEntity: Url::class, inversedBy: 'products', cascade: ['persist', 'remove'])]
    private Collection $urls;

    #[ORM\ManyToMany(targetEntity: Media::class, inversedBy: 'products', cascade: ['persist', 'remove'])]
    #[ORM\OrderBy(['order' => 'ASC'])]
    private Collection $medias;

    #[ORM\OneToMany(
        mappedBy: 'product',
        targetEntity: Plot::class,
        cascade: ['persist', 'remove'],
        orphanRemoval: true
    )]
    private Collection $plots;

    #[ORM\Column(options: ['default' => 0])]
    private ?int $price = 0;

    public function __construct()
    {
        $this->responses = new ArrayCollection();
        $this->features = new ArrayCollection();
        $this->urls = new ArrayCollection();
        $this->medias = new ArrayCollection();
        $this->plots = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getType(): ?ProductType
    {
        return $this->type;
    }

    public function setType(?ProductType $type): static
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection<int, ProductResponse>
     */
    public function getResponses(): Collection
    {
        return $this->responses;
    }

    public function addResponse(ProductResponse $response): static
    {
        if (!$this->responses->contains($response)) {
            $this->responses->add($response);
            $response->setProduct($this);
        }

        return $this;
    }

    public function removeResponse(ProductResponse $response): static
    {
        if ($this->responses->removeElement($response)) {
            // set the owning side to null (unless already changed)
            if ($response->getProduct() === $this) {
                $response->setProduct(null);
            }
        }

        return $this;
    }

    public function isAvailable(): ?bool
    {
        return $this->available;
    }

    public function setAvailable(bool $available): static
    {
        $this->available = $available;

        return $this;
    }

    /**
     * @return ArrayCollection<int, Media>
     */
    public function getDocuments(int $typeId = MediaType::PICTURE, Media $mediaToExclude = null): ArrayCollection
    {
        return $this->getMedias()->filter(function (Media $media) use ($typeId, $mediaToExclude) {
            return $typeId === $media->getType()->getId() && (!$mediaToExclude || $mediaToExclude !== $media);
        });
    }

    /**
     * @return ArrayCollection<int, Media>
     */
    public function getPictures(Media $mediaToExclude = null): ArrayCollection
    {
        return $this->getDocuments(MediaType::PICTURE, $mediaToExclude);
    }

    public function getFirstPicture(): ?Media
    {
        foreach ($this->getMedias() as $media) {
            if (MediaType::PICTURE === $media->getType()->getId()) {
                return $media;
            }
        }

        return null;
    }

    public function getBrochures(): ArrayCollection
    {
        return $this->getDocuments(MediaType::BROCHURE);
    }

    public function getPlans(): ArrayCollection
    {
        return $this->getDocuments(MediaType::PLAN);
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): static
    {
        $this->url = $url;

        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): static
    {
        $this->city = $city;

        return $this;
    }

    public function getCityZipcode(): ?string
    {
        return $this->getCity()?->getNameZipcode();
    }

    /**
     * @return Collection<int, Feature>
     */
    public function getFeatures(): Collection
    {
        return $this->features;
    }

    public function addFeature(Feature $feature): static
    {
        if (!$this->features->contains($feature)) {
            $this->features->add($feature);
        }

        return $this;
    }

    public function removeFeature(Feature $feature): static
    {
        $this->features->removeElement($feature);

        return $this;
    }

    /**
     * @return Collection<int, Url>
     */
    public function getUrls(): Collection
    {
        return $this->urls;
    }

    public function addUrl(Url $url): static
    {
        if (!$this->urls->contains($url)) {
            $this->urls->add($url);
        }

        return $this;
    }

    public function removeUrl(Url $url): static
    {
        $this->urls->removeElement($url);

        return $this;
    }

    /**
     * @return Collection<int, Media>
     */
    public function getMedias(): Collection
    {
        return $this->medias;
    }

    public function addMedia(Media $media): static
    {
        if (!$this->medias->contains($media)) {
            $this->medias->add($media);
        }

        return $this;
    }

    public function removeMedia(Media $media): static
    {
        $this->medias->removeElement($media);

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(?string $text): static
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return Collection<int, Plot>
     */
    public function getPlots(): Collection
    {
        return $this->plots;
    }

    public function addPlot(Plot $plot): static
    {
        if (!$this->plots->contains($plot)) {
            $this->plots->add($plot);
            $plot->setProduct($this);
        }

        return $this;
    }

    public function removePlot(Plot $plot): static
    {
        if ($this->plots->removeElement($plot)) {
            // set the owning side to null (unless already changed)
            if ($plot->getProduct() === $this) {
                $plot->setProduct(null);
            }
        }

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): static
    {
        $this->price = $price;

        return $this;
    }
}
