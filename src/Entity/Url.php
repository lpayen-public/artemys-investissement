<?php

namespace App\Entity;

use App\Repository\UrlRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: UrlRepository::class)]
class Url
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, unique: true)]
    private ?string $value = null;

    #[ORM\ManyToMany(targetEntity: ProductType::class, mappedBy: 'urls')]
    private Collection $productTypes;

    #[ORM\ManyToMany(targetEntity: Product::class, mappedBy: 'urls')]
    private Collection $products;

    public function __construct()
    {
        $this->productTypes = new ArrayCollection();
        $this->products = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): static
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return Collection<int, ProductType>
     */
    public function getProductTypes(): Collection
    {
        return $this->productTypes;
    }

    public function addProductType(ProductType $productType): static
    {
        if (!$this->productTypes->contains($productType)) {
            $this->productTypes->add($productType);
            $productType->addUrl($this);
        }

        return $this;
    }

    public function removeProductType(ProductType $productType): static
    {
        if ($this->productTypes->removeElement($productType)) {
            $productType->removeUrl($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Product>
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): static
    {
        if (!$this->products->contains($product)) {
            $this->products->add($product);
            $product->addUrl($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): static
    {
        if ($this->products->removeElement($product)) {
            $product->removeUrl($this);
        }

        return $this;
    }
}
