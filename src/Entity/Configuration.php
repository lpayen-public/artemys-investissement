<?php

namespace App\Entity;

use App\Repository\ConfigurationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ConfigurationRepository::class)]
class Configuration
{
    public const WINDOW_SIZE = 'WINDOW_SIZE';
    public const WINDOW_COLOR = 'WINDOW_COLOR';
    public const WINDOW_HAMBURGER = 'WINDOW_HAMBURGER';
    public const HOME_META_TITLE = 'HOME_META_TITLE';
    public const HOME_META_DESCRIPTION = 'HOME_META_DESCRIPTION';
    public const HOME_META_KEYWORDS = 'HOME_META_KEYWORDS';
    public const HOME_SLIDER_TITLE = 'HOME_SLIDER_TITLE';
    public const CONTACT_META_TITLE = 'CONTACT_META_TITLE';
    public const CONTACT_META_DESCRIPTION = 'CONTACT_META_DESCRIPTION';
    public const CONTACT_META_KEYWORDS = 'CONTACT_META_KEYWORDS';
    public const CONTACT_SLIDER_TITLE = 'CONTACT_SLIDER_TITLE';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, unique: true)]
    private ?string $name = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $value = null;

    #[ORM\OneToMany(mappedBy: 'configuration', targetEntity: UserConfiguration::class)]
    private Collection $userConfigurations;

    public function __construct()
    {
        $this->userConfigurations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): static
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return Collection<int, UserConfiguration>
     */
    public function getUserConfigurations(): Collection
    {
        return $this->userConfigurations;
    }

    public function addUserConfiguration(UserConfiguration $userConfiguration): static
    {
        if (!$this->userConfigurations->contains($userConfiguration)) {
            $this->userConfigurations->add($userConfiguration);
            $userConfiguration->setConfiguration($this);
        }

        return $this;
    }

    public function removeUserConfiguration(UserConfiguration $userConfiguration): static
    {
        if ($this->userConfigurations->removeElement($userConfiguration)) {
            // set the owning side to null (unless already changed)
            if ($userConfiguration->getConfiguration() === $this) {
                $userConfiguration->setConfiguration(null);
            }
        }

        return $this;
    }

    public static function getWindowConfs(): array
    {
        return [
            self::WINDOW_SIZE => 100,
            self::WINDOW_COLOR => '#4c7a90',
            self::WINDOW_HAMBURGER => 'open',
            self::HOME_META_TITLE => 'Artemys Investissement',
            self::HOME_META_DESCRIPTION => '',
            self::HOME_META_KEYWORDS => '',
            self::HOME_SLIDER_TITLE => 'La qualité au service de vos envies',
            self::CONTACT_META_TITLE => 'Contact',
            self::CONTACT_META_DESCRIPTION => '',
            self::CONTACT_META_KEYWORDS => '',
            self::CONTACT_SLIDER_TITLE => 'Un lien entre vous et nous',
        ];
    }
}
