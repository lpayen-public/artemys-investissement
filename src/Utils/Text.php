<?php

declare(strict_types=1);

namespace App\Utils;

class Text
{
    public static function clearSpaces(?string $text): ?string
    {
        if (null === $text) {
            return null;
        }

        /** @var string[] $words */
        $words = \preg_split('#[ ]+#', $text);
        $text = '';

        foreach ($words as $word) {
            $text .= $word ? $word . ' ' : '';
        }

        return \trim($text);
    }

    /**
     * @return array|string[]
     */
    public static function split(?string $text, string $spliter = ';'): array
    {
        if (null === $text) {
            return [];
        }

        /** @var string[] $tab */
        $tab = \preg_split('#([' . $spliter . '])#', $text, -1, \PREG_SPLIT_NO_EMPTY);

        foreach ($tab as $k => $v) {
            $v = \trim($v);
            if (!$v) {
                unset($tab[$k]);
            }
        }

        return $tab;
    }

    public static function token(int $lenth = 30, bool $onlyUpper = false, bool $specialChars = false): string
    {
        $tab0 = [];
        for ($i = 0; $i < 10; ++$i) {
            $tab0[] = $i;
        }

        $tab1 = [];
        for ($i = 97; $i <= 122; ++$i) {
            $tab1[] = \chr($i);
        }

        $tab2 = ['!', '$', '#'];

        $tab3 = [];
        for ($i = 65; $i <= 90; ++$i) {
            $tab3[] = \chr($i);
        }

        $tabP = [];
        $nbTab = $specialChars ? 2 : 1;

        while (\count($tabP) < $lenth) {
            $rand = \rand(0, $nbTab);
            $tab = ${'tab' . $rand};
            $rand = \rand(0, \count($tab) - 1);
            $val = $tab[$rand];
            $tabP[] = $val;
        }

        $text = \implode('', $tabP);

        return $onlyUpper ? \mb_strtoupper($text) : $text;
    }

    public static function length(string $text, int $nb = 10, bool $fromRight = false): string
    {
        $length = \strlen($text);
        $diff = $length - $nb;

        // Si la longueur du texte est supérieure à 0, on retire le texte en trop
        if ($diff > 0) {
            return \substr($text, 0, $nb);
        }

        // Si on ne place pas le texte à droite
        if (!$fromRight) {
            // Répétition de l'espace par rapport à la différence entre l'espacement voulu et le texte
            $text .= \str_repeat(' ', \abs($diff));

            return $text;
        }
        // Répétition de l'espace par rapport à la différence entre l'espacement voulu et le texte
        $before = \str_repeat(' ', \abs($diff));

        // Ajout d'un espace avant le texte
        return $before . $text;
    }

    public static function slug(?string $text): string
    {
        if (null === $text) {
            return '';
        }

        $from = [
            '/',
            '"',
            ' ',
            "'",
            '_',
            ',',
            '.',
            'À',
            'Á',
            'Â',
            'Ã',
            'Ä',
            'Å',
            'Ç',
            'È',
            'É',
            'Ê',
            'Ë',
            'Ì',
            'Í',
            'Î',
            'Ï',
            'Ò',
            'Ó',
            'Ô',
            'Õ',
            'Ö',
            'Ù',
            'Ú',
            'Û',
            'Ü',
            'Ý',
            'à',
            'á',
            'â',
            'ã',
            'ä',
            'å',
            'ç',
            'è',
            'é',
            'ê',
            'ë',
            'ì',
            'í',
            'î',
            'ï',
            'ð',
            'ò',
            'ó',
            'ô',
            'õ',
            'ö',
            'ù',
            'ú',
            'û',
            'ü',
            'ý',
            'ÿ',
        ];
        $to = [
            '-',
            '-',
            '-',
            '-',
            '-',
            '',
            '.',
            'A',
            'A',
            'A',
            'A',
            'A',
            'A',
            'C',
            'E',
            'E',
            'E',
            'E',
            'I',
            'I',
            'I',
            'I',
            'O',
            'O',
            'O',
            'O',
            'O',
            'U',
            'U',
            'U',
            'U',
            'Y',
            'a',
            'a',
            'a',
            'a',
            'a',
            'a',
            'c',
            'e',
            'e',
            'e',
            'e',
            'i',
            'i',
            'i',
            'i',
            'o',
            'o',
            'o',
            'o',
            'o',
            'o',
            'u',
            'u',
            'u',
            'u',
            'y',
            'y',
        ];
        $text = \str_replace($from, $to, \trim($text));
        $text = \preg_replace('#([-]+)#', '-', $text);

        return $text ? \strtolower($text) : '';
    }

    public static function majAll(?string $text): ?string
    {
        if (null === $text) {
            return null;
        }

        $text = utf8_decode($text);

        /** @var string[] $tab */
        $tab = \preg_split('#([ -]+)#', $text, -1, \PREG_SPLIT_DELIM_CAPTURE);
        $text = '';

        if (!\count($tab)) {
            return null;
        }

        foreach ($tab as $v) {
            $v = \strtolower($v);
            $first = self::maj(\substr($v, 0, 1));
            $reste = \substr($v, 1, \strlen($v) - 1);
            $text .= $first . $reste;
        }

        return utf8_encode($text);
    }

    public static function maj(?string $text): ?string
    {
        if (null === $text) {
            return null;
        }

        $from = [
            'à',
            'á',
            'â',
            'ã',
            'ä',
            'å',
            'ç',
            'è',
            'é',
            'ê',
            'ë',
            'ì',
            'í',
            'î',
            'ï',
            'ð',
            'ò',
            'ó',
            'ô',
            'õ',
            'ö',
            'ù',
            'ú',
            'û',
            'ü',
            'ý',
            'ÿ',
        ];
        $to = [
            'À',
            'Á',
            'Â',
            'Ã',
            'Ä',
            'Å',
            'Ç',
            'È',
            'É',
            'Ê',
            'Ë',
            'Ì',
            'Í',
            'Î',
            'Ï',
            'Ò',
            'Ó',
            'Ô',
            'Õ',
            'Ö',
            'Ù',
            'Ú',
            'Û',
            'Ü',
            'Ý',
            'Ý',
            'Ý',
        ];

        return \strtoupper(\str_replace($from, $to, $text));
    }

    public static function has(string $word, string $search): bool
    {
        return !(false === \strpos($word, $search));
    }

    public static function trim(?string $text): ?string
    {
        return null !== $text ? \trim($text) : $text;
    }

    public static function string(?string $text): string
    {
        return null !== $text ? \trim($text) : '';
    }

    public static function upper(?string $text): ?string
    {
        return null !== $text ? \mb_strtoupper($text) : $text;
    }

    public static function lower(?string $text): ?string
    {
        return null !== $text ? mb_strtolower($text) : $text;
    }
}
