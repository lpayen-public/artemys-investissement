<?php

declare(strict_types=1);

namespace App\Utils;

class Date
{
    public static function toFr(?\DateTime $date): string
    {
        if (null === $date) {
            return '';
        }

        return $date->format('d/m/Y');
    }

    public static function toUs(?\DateTime $date): string
    {
        if (null === $date) {
            return '';
        }

        return $date->format('Y-m-d');
    }

    public static function isFr(?string $date): bool
    {
        if (null === $date) {
            return false;
        }

        return (bool) \preg_match('#([0-9]{2})\/([0-9]{2})\/([0-9]{4})#', $date);
    }

    public static function isUs(?string $date): bool
    {
        if (null === $date) {
            return false;
        }

        return (bool) \preg_match('#([0-9]{4})-([0-9]{2})-([0-9]{2})#', $date);
    }

    public static function frToUs(?string $date): ?string
    {
        $date = self::addSlashes($date);

        if (!self::isFr($date)) {
            return $date;
        }

        $date = Text::string($date);

        /** @var array<int, string> $dates */
        $dates = preg_split('#(/)#', $date);

        return 3 === count($dates)
            ? $dates[2] . '-' . $dates[1] . '-' . $dates[0]
            : $date;
    }

    public static function setDateTime(?string $date, \DateTime $default = null, bool $forceDate = true): ?\DateTime
    {
        $date = self::isUs($date) ? $date : null;

        if ($date) {
            // @var \DateTime $date
            return \DateTime::createFromFormat('Y-m-d', $date);
        }

        return $forceDate ? new \DateTime() : $default;
    }

    public static function addSlashes(?string $date): string
    {
        $date = Text::string($date);

        if (8 !== strlen($date)) {
            return $date;
        }

        $day = \substr($date, 0, 2);
        $month = \substr($date, 2, 2);
        $year = \substr($date, 4, 4);

        return $day . '/' . $month . '/' . $year;
    }
}
