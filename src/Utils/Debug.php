<?php

namespace App\Utils;

use Symfony\Component\HttpFoundation\RequestStack;

class Debug
{
    public function __construct(private readonly RequestStack $requestStack)
    {
    }

    public function pr($text, string $comment = null): void
    {
        $session = $this->requestStack->getSession();
        $debug = $session->get('DEBUG', []);
        $debug[] = [
            'text' => $text,
            'comment' => $comment,
        ];

        $session->set('DEBUG', $debug);
    }
}
