<?php

namespace App\Utils;

class File
{
    public static function create(string $dir): void
    {
        if (!\is_dir($dir)) {
            \mkdir($dir, 0755, true);
        }
    }

    public static function exists(string $file): bool
    {
        return \file_exists($file);
    }

    public static function delete(string $path, bool $deleteFolderIfEmtpy = false): void
    {
        if (!self::exists($path)) {
            return;
        }

        \unlink($path);

        if (!$deleteFolderIfEmtpy) {
            return;
        }

        $folder = \substr($path, 0, strrpos($path, '/'));
        if (0 === count(\glob($folder . '/*'))) {
            \rmdir($folder);
        }
    }

    public static function getSize(string $path): int
    {
        return \file_exists($path) ? (int) \filesize($path) : 0;
    }

    public static function getOctet(string $path): float
    {
        return self::getSize($path);
    }

    public static function getKiloOctet(string $path): float
    {
        $size = self::getSize($path);

        return \round($size / 1024);
    }

    public static function getMegaOctet(string $path): float
    {
        $size = self::getSize($path);

        return \round($size / 1048576, 2);
    }

    public static function getGigaOctet(string $path): float
    {
        $size = self::getSize($path);

        return \round($size / 1073741824, 2);
    }

    public static function getTeraOctet(string $path): float
    {
        $size = self::getSize($path);

        return \round($size / 1099511627776, 2);
    }

    public static function getPetaOctet(string $path): float
    {
        $size = self::getSize($path);

        return \round($size / 1125899906842620, 2);
    }
}
