<?php

declare(strict_types=1);

namespace App\Utils;

class Reorder
{
    /**
     * À utiliser pour les collections d'entités
     * Modifie la clé pour chaque entrée du tableau en prenant la $key en paramètre
     * Permet d'avoir en clé le vrai ID (ou autre) d'une entité plutôt que 0, 1, 2 qu'un findAll() ou autre aurait créé.
     *
     * @param mixed       $entities Tableau d'entités à classer
     * @param string      $key      Champ sur lequel on veut classer
     * @param string|null $m2O      Nom de l'entité parente en M2O sur laquelle on veut classer
     *
     * @return array<int|string, mixed>
     */
    public static function changeKeyForEntity($entities, string $key = 'Id', string $m2O = null): array
    {
        $tab = [];
        foreach ($entities as $e) {
            // Pas d'entité parente
            if (!$m2O) {
                $tab[$e->{'get' . $key}()] = $e;
            } elseif (null !== $e->{'get' . $m2O}()) {
                $repoM2O = $e->{'get' . $m2O}();
                $tab[$repoM2O->{'get' . $key}()] = $e;
            }
        }

        return $tab;
    }

    /**
     * À utiliser pour les tableaux
     * Modifie la clé pour chaque entrée du tableau en prenant la $key en paramètre
     * Permet d'avoir en clé le vrai ID (ou autre) d'une entrée plutôt que 0, 1, 2 qu'un tableau aurait créé.
     *
     * @param array<int|string, mixed> $datas Tableau de données à classer
     * @param string                   $key   Champ sur lequel on veut classer
     *
     * @return array<int|string, mixed>
     */
    public static function changeKeyForTable(array $datas, string $key = 'id'): array
    {
        $tab = [];
        foreach ($datas as $e) {
            $tab[$e[$key]] = $e;
        }

        return $tab;
    }

    /**
     * Classe les valeurs d'un tableau (d'entités ou pas) selon la $key en paramètre.
     *
     * @param array<int|string, mixed> $datas
     *
     * @return array<int|string, mixed>
     */
    public static function by(array $datas, string $key = 'Id', string $direction = 'ASC'): array
    {
        $ordre = [];
        foreach ($datas as $k => $data) {
            $ordre[$k] = \is_object($data)
                ? $data->{'get' . $key}()
                : $data[$key];
        }

        $sort = 'ASC' == $direction ? \SORT_ASC : \SORT_DESC;
        \array_multisort($ordre, $sort, $datas);

        return $datas;
    }
}
