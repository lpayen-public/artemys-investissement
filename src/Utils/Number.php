<?php

declare(strict_types=1);

namespace App\Utils;

class Number
{
    public static function toFloat(?string $text): float
    {
        if (null === $text) {
            return 0.0;
        }

        $text = \preg_replace('#([^0-9,\.-])#', '', $text);

        return (float) $text;
    }

    public static function toInt(?string $text): int
    {
        if (null === $text) {
            return 0;
        }

        return (int) $text;
    }

    public static function toEuro(?string $text): string
    {
        return self::toFloat($text) . ' €';
    }

    public static function random(int $from = 0, int $to = 100): int
    {
        return \rand($from, $to);
    }
}
