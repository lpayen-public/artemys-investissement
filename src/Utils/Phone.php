<?php

declare(strict_types=1);

namespace App\Utils;

class Phone
{
    public static function clear(?string $text): ?string
    {
        $text = \preg_replace('#([^0-9\+]+)#', '', Text::string($text));

        return \str_replace('+330', '0', $text);
    }
}
