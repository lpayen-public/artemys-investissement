<?php

namespace App\Controller\Admin;

use App\Controller\ArtemysController;
use App\Entity\Product;
use App\Service\Product\ProductCityInterface;
use App\Service\Product\ProductFilterInterface;
use App\Service\Product\ProductFolderInterface;
use App\Service\Product\ProductFormInterface;
use App\Service\Product\ProductMediaInterface;
use App\Service\Product\ProductPlotFormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends ArtemysController
{
    public function index(ProductFilterInterface $productFilter): Response
    {
        return $this->render('admin/product/index.html.twig', $productFilter->index());
    }

    public function show(Product $product): Response
    {
        return $this->render('admin/product/show.html.twig', [
            'product' => $product,
        ]);
    }

    public function form(ProductFormInterface $productForm, Product $product = null): Response
    {
        $route = !$product ? 'admin/product/new.html.twig' : 'admin/product/edit.html.twig';
        $product = $product ?: new Product();
        $datas = $productForm->setForm($product);

        return $datas['redirect']
            ? $this->redirect($datas['redirect'])
            : $this->render($route, $datas);
    }

    public function delete(Product $product, ProductFormInterface $productForm): Response
    {
        $productForm->delete($product);

        return $this->redirectToRoute('admin_product_index');
    }

    public function deleteMedia(ProductMediaInterface $productMedia): JsonResponse
    {
        $productMedia->delete();

        return $this->json(true);
    }

    public function orderMedia(ProductMediaInterface $productMedia): Response
    {
        $product = $productMedia->order();

        return $this->render('admin/product/picture.html.twig', [
            'product' => $product,
        ]);
    }

    public function citySearch(ProductCityInterface $productCity): Response
    {
        return $this->render('admin/product/city.html.twig', $productCity->search());
    }

    public function plot(Product $product, ProductPlotFormInterface $productPlotForm): Response
    {
        $datas = $productPlotForm->setForm($product);

        return $datas['redirect']
            ? $this->redirect($datas['redirect'])
            : $this->render('admin/product/plot.html.twig', $datas);
    }

    public function folder(Product $product, ProductFolderInterface $productFolder): JsonResponse
    {
        $productFolder
            ->set($product)
            ->render()
        ;

        return $this->json(true);
    }
}
