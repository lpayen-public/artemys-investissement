<?php

namespace App\Controller\Admin;

use App\Controller\ArtemysController;
use App\Entity\ProductType;
use App\Repository\ProductTypeRepository;
use App\Service\ProductType\ProductTypeFormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProductTypeController extends ArtemysController
{
    public function index(ProductTypeRepository $productTypeRepository): Response
    {
        return $this->render('admin/productType/index.html.twig', [
            'productTypes' => $productTypeRepository->findAll(),
        ]);
    }

    public function show(ProductType $productType): Response
    {
        return $this->render('admin/productType/show.html.twig', [
            'productType' => $productType,
        ]);
    }

    public function form(ProductTypeFormInterface $productTypeForm, ProductType $productType = null): Response
    {
        $route = !$productType ? 'admin/productType/new.html.twig' : 'admin/productType/edit.html.twig';
        $productType = $productType ?: new ProductType();
        $datas = $productTypeForm->setForm($productType);

        return $datas['redirect']
            ? $this->redirect($datas['redirect'])
            : $this->render($route, $datas);
    }

    public function delete(Request $request, ProductType $productType): Response
    {
        if ($this->isCsrfTokenValid('deleteProductType' . $productType->getToken(), $request->request->get('_token'))) {
            $this->em->remove($productType);
            $this->em->flush();
        }

        return $this->redirectToRoute('admin_product_type_index', [], Response::HTTP_SEE_OTHER);
    }
}
