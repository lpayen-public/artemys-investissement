<?php

namespace App\Controller\Admin;

use App\Controller\ArtemysController;
use App\Entity\Partner;
use App\Repository\PartnerRepository;
use App\Service\Partner\PartnerFormInterface;
use Symfony\Component\HttpFoundation\Response;

class PartnerController extends ArtemysController
{
    public function index(PartnerRepository $partnerRepository): Response
    {
        return $this->render('admin/partner/index.html.twig', [
            'partners' => $partnerRepository->findAll(),
        ]);
    }

    public function show(Partner $partner): Response
    {
        return $this->render('admin/partner/show.html.twig', [
            'partner' => $partner,
        ]);
    }

    public function form(PartnerFormInterface $partnerForm, Partner $partner = null): Response
    {
        $route = !$partner ? 'admin/partner/new.html.twig' : 'admin/partner/edit.html.twig';
        $partner = $partner ?: new Partner();
        $datas = $partnerForm->setForm($partner);

        if ($datas['isRedirect']) {
            return $this->redirectToRoute('admin_partner_edit', [
                'token' => $partner->getToken(),
            ]);
        }

        return $this->render($route, $datas);
    }

    public function delete(Partner $partner): Response
    {
        $isCsrfValid = $this->isCsrfTokenValid('deletePartner' . $partner->getToken(), $this->request->get('_token'));

        if ($this->userService->isSuperAdmin() && $isCsrfValid) {
            $this->em->remove($partner);
            $this->em->flush();
        }

        return $this->redirectToRoute('admin_partner_index');
    }
}
