<?php

namespace App\Controller\Admin;

use App\Controller\ArtemysController;
use App\Service\Interface\PaymentHandler;
use App\Service\Payment\PaymentProvider;
use App\Utils\Debug;
use Symfony\Component\HttpFoundation\Response;

use function array_map;

class InterfaceController extends ArtemysController
{
    public function index(PaymentProvider $payment): Response
    {
        //        $payment->gotoPayment(1, 100);
        //        $payment->gotoPayment(2, 100);
        //        $payment->gotoPayment(3, 100);
        $payment->gotoPayment(4, 100);
        //        $payment->gotoPayment(5, 100);

        return $this->render('admin/interface/index.html.twig', [
        ]);
    }

    public function map(): void
    {
        //        $products = array_map(function ($product) {
        //            return [
        //                ...$product,
        //                'total' => $product['quantity'] * $product['unitPrice'],
        //            ];
        //        }, $this->getProducts());
        //
        $products = \array_map(fn ($product): array => [
            ...$product,
            'total' => $product['quantity'] * $product['unitPrice'],
        ], $this->getProducts());

        dd($products);
    }

    /**
     * @return array[]
     */
    private function getProducts(): array
    {
        return [
            [
                'name' => 'Peluche',
                'unitPrice' => 10,
                'quantity' => 5,
            ],
            [
                'name' => 'Jouet',
                'unitPrice' => 25,
                'quantity' => 3,
            ],
            [
                'name' => 'Pantalon',
                'unitPrice' => 59,
                'quantity' => 1,
            ],
        ];
    }

    public function interfaceBest(PaymentHandler $paymentHandler, Debug $debug): Response
    {
        $amount = 100;
        $debug->pr($amount, '$amount');
        $debug->pr($paymentHandler->pay('sg', $amount), 'SG');
        $debug->pr($paymentHandler->pay('ca', $amount), 'CA');
        $debug->pr($paymentHandler->pay('lcl', $amount), 'LCL');

        return $this->render('admin/interface/index.html.twig', [
        ]);
    }
}
