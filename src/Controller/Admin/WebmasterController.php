<?php

namespace App\Controller\Admin;

use App\Controller\ArtemysController;
use App\Entity\Customer;
use App\Service\Mailer;
use App\Service\Webmaster\ConfigurationFormInterface;
use App\Service\Webmaster\WebmasterLogInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class WebmasterController extends ArtemysController
{
    public function index(): Response
    {
        if (!$this->userService->isSuperAdmin()) {
            return $this->redirectToRoute('admin_index');
        }

        return $this->render('admin/webmaster/index.html.twig');
    }

    public function log(WebmasterLogInterface $webmasterLog): Response
    {
        if (!$this->userService->isSuperAdmin()) {
            return $this->redirectToRoute('admin_index');
        }

        return $this->render('admin/webmaster/log.html.twig', $webmasterLog->get());
    }

    public function logRead(string $path, WebmasterLogInterface $webmasterLog): Response
    {
        if (!$this->userService->isSuperAdmin()) {
            return $this->redirectToRoute('admin_index');
        }

        return new Response($webmasterLog->read($path));
    }

    public function logDelete(string $path, WebmasterLogInterface $webmasterLog): Response
    {
        if (!$this->userService->isSuperAdmin()) {
            return $this->redirectToRoute('admin_index');
        }

        $webmasterLog->delete($path);

        return $this->redirectToRoute('admin_webmaster_log_index');
    }

    public function email(): Response
    {
        if (!$this->userService->isSuperAdmin()) {
            return $this->redirectToRoute('admin_index');
        }

        /** @var Customer $customer */
        $customer = $this->em->getRepository(Customer::class)->findOneBy([], ['id' => 'DESC']);

        return $this->render('email/customer_new.html.twig', [
            'customer' => $customer,
        ]);
    }

    public function sendEmail(Mailer $mailer)
    {
        if (!$this->userService->isSuperAdmin()) {
            return $this->redirectToRoute('admin_index');
        }

        $mailer->test();

        /** @var Customer $customer */
        $customer = $this->em->getRepository(Customer::class)->findOneBy([], ['id' => 'DESC']);
        //        $mailer->addCustomer($customer);

        return $this->render('email/customer_new.html.twig', [
            'customer' => $customer,
        ]);
    }

    public function configuration(ConfigurationFormInterface $configurationForm): RedirectResponse|Response
    {
        $datas = $configurationForm->setForm();
        if ($datas['isRedirect']) {
            return $this->redirectToRoute('admin_webmaster_conf');
        }

        return $this->render('admin/webmaster/configuration.html.twig', $datas);
    }
}
