<?php

namespace App\Controller\Admin;

use App\Controller\ArtemysController;
use App\Entity\ProductQuestion;
use App\Form\ProductQuestion1Type;
use App\Repository\ProductQuestionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class QuestionController extends ArtemysController
{
    public function index(ProductQuestionRepository $questionRepository): Response
    {
        return $this->render('admin/question/index.html.twig', [
            'questions' => $questionRepository->findAll(),
        ]);
    }

    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $question = new ProductQuestion();
        $form = $this->createForm(ProductQuestion1Type::class, $question);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($question);
            $entityManager->flush();

            return $this->redirectToRoute('admin_question_edit', [
                'token' => $question->getToken(),
            ]);
        }

        return $this->render('admin/question/new.html.twig', [
            'question' => $question,
            'form' => $form,
        ]);
    }

    public function show(ProductQuestion $question): Response
    {
        return $this->render('admin/question/show.html.twig', [
            'question' => $question,
        ]);
    }

    public function edit(Request $request, ProductQuestion $question, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(ProductQuestion1Type::class, $question);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('admin_question_edit', [
                'token' => $question->getToken(),
            ]);
        }

        return $this->render('admin/question/edit.html.twig', [
            'question' => $question,
            'form' => $form,
        ]);
    }

    public function delete(Request $request, ProductQuestion $question, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete' . $question->getId(), $request->request->get('_token'))) {
            $entityManager->remove($question);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_question_index', [], Response::HTTP_SEE_OTHER);
    }
}
