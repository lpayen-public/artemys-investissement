<?php

namespace App\Controller\Admin;

use App\Controller\ArtemysController;
use App\Entity\Feature;
use App\Form\FeatureType;
use App\Repository\FeatureRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FeatureController extends ArtemysController
{
    public function index(FeatureRepository $featureRepository): Response
    {
        return $this->render('admin/feature/index.html.twig', [
            'features' => $featureRepository->findAll(),
        ]);
    }

    public function new(Request $request): Response
    {
        $feature = new Feature();
        $form = $this->createForm(FeatureType::class, $feature);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($feature);
            $this->em->flush();

            return $this->redirectToRoute('admin_feature_edit', [
                'token' => $feature->getToken(),
            ]);
        }

        return $this->render('admin/feature/new.html.twig', [
            'feature' => $feature,
            'form' => $form,
        ]);
    }

    public function show(Feature $feature): Response
    {
        return $this->render('admin/feature/show.html.twig', [
            'feature' => $feature,
        ]);
    }

    public function edit(Request $request, Feature $feature): Response
    {
        $form = $this->createForm(FeatureType::class, $feature);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();

            return $this->redirectToRoute('admin_feature_edit', [
                'token' => $feature->getToken(),
            ]);
        }

        return $this->render('admin/feature/edit.html.twig', [
            'feature' => $feature,
            'form' => $form,
        ]);
    }

    public function delete(Request $request, Feature $feature): Response
    {
        if ($this->isCsrfTokenValid('delete' . $feature->getId(), $request->request->get('_token'))) {
            $this->em->remove($feature);
            $this->em->flush();
        }

        return $this->redirectToRoute('admin_feature_index', [], Response::HTTP_SEE_OTHER);
    }
}
