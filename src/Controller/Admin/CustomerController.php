<?php

namespace App\Controller\Admin;

use App\Controller\ArtemysController;
use App\Entity\Customer;
use App\Service\Customer\CustomerFilterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CustomerController extends ArtemysController
{
    public function index(CustomerFilterInterface $customerFilter): Response
    {
        return $this->render(
            'admin/customer/index.html.twig',
            $customerFilter->index()
        );
    }

    public function show(Customer $customer): Response
    {
        return $this->render('admin/customer/show.html.twig', [
            'customer' => $customer,
        ]);
    }

    public function delete(Request $request, Customer $customer): Response
    {
        if ($this->isCsrfTokenValid('delete' . $customer->getToken(), $request->request->get('_token'))) {
            $this->em->remove($customer);
            $this->em->flush();
        }

        $params = $request->query->get('page')
            ? ['page' => $request->query->get('page')]
            : [];

        return $this->redirectToRoute('admin_customer_index', $params);
    }
}
