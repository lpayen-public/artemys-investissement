<?php

namespace App\Controller\Admin;

use App\Controller\ArtemysController;
use App\Entity\User;
use App\Service\User\UserFilterInterface;
use App\Service\User\UserFormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class UserController extends ArtemysController
{
    public function index(UserFilterInterface $userFilter): Response
    {
        if (!$this->userService->isSuperAdmin()) {
            return $this->redirectToRoute('admin_index');
        }

        return $this->render(
            'admin/user/index.html.twig',
            $userFilter->index()
        );
    }

    public function show(User $user): Response
    {
        if (!$this->userService->isSuperAdmin()) {
            return $this->redirectToRoute('admin_index');
        }

        return $this->render('admin/user/show.html.twig', [
            'user' => $user,
        ]);
    }

    public function form(UserFormInterface $userForm, User $user = null): RedirectResponse|Response
    {
        if (!$this->userService->isSuperAdmin()) {
            return $this->redirectToRoute('admin_index');
        }

        $route = !$user ? 'admin/user/new.html.twig' : 'admin/user/edit.html.twig';
        $user = $user ?: new User();
        $datas = $userForm->setForm($user);

        if ($datas['isRedirect']) {
            return $this->redirectToRoute('admin_user_edit', [
                'token' => $user->getToken(),
            ]);
        }

        return $this->render($route, $datas);
    }

    public function delete(UserFormInterface $userForm, User $user): Response
    {
        if (!$this->userService->isSuperAdmin()) {
            return $this->redirectToRoute('admin_index');
        }

        $userForm->delete($user);

        return $this->redirectToRoute('admin_user_index');
    }
}
