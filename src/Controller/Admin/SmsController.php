<?php

namespace App\Controller\Admin;

use App\Entity\Sms;
use App\Form\SmsType;
use App\Repository\SmsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/sms')]
class SmsController extends AbstractController
{
    public function index(SmsRepository $smsRepository): Response
    {
        return $this->render('admin/sms/index.html.twig', [
            'sms' => $smsRepository->findAll(),
        ]);
    }

    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $sms = new Sms();
        $form = $this->createForm(SmsType::class, $sms);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($sms);
            $entityManager->flush();

            return $this->redirectToRoute('admin_sms_index', []);
        }

        return $this->render('admin/sms/new.html.twig', [
            'sms' => $sms,
            'form' => $form,
        ]);
    }

    public function show(Sms $sms): Response
    {
        return $this->render('admin/sms/show.html.twig', [
            'sms' => $sms,
        ]);
    }

    public function edit(Request $request, Sms $sms, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(SmsType::class, $sms);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('admin_sms_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('admin/sms/edit.html.twig', [
            'sms' => $sms,
            'form' => $form,
        ]);
    }

    public function delete(Request $request, Sms $sms, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete' . $sms->getId(), $request->request->get('_token'))) {
            $entityManager->remove($sms);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_sms_index', [], Response::HTTP_SEE_OTHER);
    }
}
