<?php

namespace App\Controller\Admin;

use App\Controller\ArtemysController;
use App\Entity\Customer;
use App\Entity\Feature;
use App\Entity\Partner;
use App\Entity\Product;
use App\Entity\ProductQuestion;
use App\Entity\ProductType;
use App\Service\Admin\SearchService;
use App\Service\ConfigurationService;
use App\Service\Mailer;
use App\Service\User\UserFormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends ArtemysController
{
    private array $types = ['btn', 'btn-outline'];
    private array $colors = ['primary', 'secondary', 'info', 'warning', 'success', 'danger'];
    private array $sizes = ['xs', 'sm', 'md', 'lg', 'xl', 'xxl'];

    public function index(Mailer $mailer): Response
    {
        $buttons = [];
        foreach ($this->sizes as $s) {
            $buttons[$s] = [];
            foreach ($this->types as $t) {
                $type = $t . '-';
                $buttons[$s][$t] = [];
                foreach ($this->colors as $color) {
                    $name = $type . $color . '-';
                    $buttons[$s][$t][$name] = [
                        'name' => \ucfirst($color),
                        'class' => 'btn btn-' . $s . ' ' . $type . $color,
                    ];
                }
            }
        }

        $nbProducts = count($this->em->getRepository(Product::class)->findAll());
        $nbProductTypes = count($this->em->getRepository(ProductType::class)->findAll());
        $nbContacts = count($this->em->getRepository(Customer::class)->findAll());
        $nbFeatures = count($this->em->getRepository(Feature::class)->findAll());
        $nbQuestions = count($this->em->getRepository(ProductQuestion::class)->findAll());
        $nbPartners = count($this->em->getRepository(Partner::class)->findAll());

        $mailer->test();

        return $this->render('admin/index.html.twig', [
            'buttons' => $buttons,
            'nbProducts' => $nbProducts,
            'nbProductTypes' => $nbProductTypes,
            'nbContacts' => $nbContacts,
            'nbFeatures' => $nbFeatures,
            'nbQuestions' => $nbQuestions,
            'nbPartners' => $nbPartners,
        ]);
    }

    public function hamburger(ConfigurationService $configurationService): JsonResponse
    {
        $configurationService->setHamburger();

        return $this->json(true);
    }

    public function windowSize(Request $request, ConfigurationService $configurationService): JsonResponse
    {
        $configurationService->setSize($request->get('windowSize'));

        return $this->json(true);
    }

    public function search(SearchService $searchService): Response
    {
        return $this->render('admin/search.html.twig', $searchService->search());
    }

    public function profil(UserFormInterface $userForm): RedirectResponse|Response
    {
        $user = $this->userService->getActiveUser();
        $datas = $userForm->setForm($user, true);

        if ($datas['isRedirect']) {
            return $this->redirectToRoute('admin_profil');
        }

        return $this->render('admin/profil/index.html.twig', $datas);
    }
}
