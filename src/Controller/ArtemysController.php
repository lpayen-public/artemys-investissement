<?php

namespace App\Controller;

use App\Service\UserService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

class ArtemysController extends AbstractController
{
    protected ?Request $request;

    public function __construct(
        protected readonly EntityManagerInterface $em,
        protected readonly RequestStack $requestStack,
        protected readonly UserService $userService
    ) {
        $this->request = $this->requestStack->getCurrentRequest();
    }

    public function goto404(bool $isFromAdmin = false): Response
    {
        $view = !$isFromAdmin ? 'front/error404.html.twig' : 'admin/error404.html.twig';

        return new Response($this->renderView($view), Response::HTTP_NOT_FOUND);
    }
}
