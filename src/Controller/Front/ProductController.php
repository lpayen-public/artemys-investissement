<?php

namespace App\Controller\Front;

use App\Controller\ArtemysController;
use App\Service\Map\MapInterface;
use App\Service\Product\ProductMediaInterface;
use App\Service\Slugger\SluggerUrlInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends ArtemysController
{
    public function index(
        string $typeSlug,
        string $productSlug,
        ProductMediaInterface $productMedia,
        SluggerUrlInterface $sluggerUrl,
        MapInterface $map
    ): Response {
        [$productType, $productTypeUrl] = $sluggerUrl->getProductType($typeSlug);
        [$product, $productUrl] = $sluggerUrl->getProduct($productSlug);

        if ($productType && $product) {
            $cities = $map
                ->init()
                ->setDatas(new ArrayCollection([$product]))
                ->getDatas()
            ;

            return $this->render('front/product.html.twig', [
                'product' => $product,
                'documents' => $productMedia->getDocumentsByProduct($product),
                'cities' => $cities,
            ]);
        }

        // Old urls
        if ($productTypeUrl && $productUrl) {
            return $this->redirectToRoute(
                'front_product',
                ['typeSlug' => $productTypeUrl, 'productSlug' => $productUrl],
                Response::HTTP_MOVED_PERMANENTLY
            );
        }

        return $this->goto404();
    }
}
