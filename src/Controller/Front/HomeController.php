<?php

namespace App\Controller\Front;

use App\Controller\ArtemysController;
use App\Entity\Media;
use App\Repository\PartnerRepository;
use App\Service\Front\HomeProductInterface;
use App\Service\Media\FrontThumbnailInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends ArtemysController
{
    public function index(HomeProductInterface $homeProduct, PartnerRepository $partnerRepo): Response
    {
        return $this->render('front/index.html.twig', [
            'products' => $homeProduct->get(),
            'partners' => $partnerRepo->findBy(['active' => true]),
        ]);
    }

    public function image(
        string $token,
        string $filter,
        FrontThumbnailInterface $thumbGenerator
    ): BinaryFileResponse {
        $media = $this->em->getRepository(Media::class)->findOneBy(['token' => $token]);
        $url = $thumbGenerator->miniaturize($media->getUrl(), $filter);

        return $this->file($url);
    }
}
