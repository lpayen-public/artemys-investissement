<?php

namespace App\Controller\Front;

use App\Controller\ArtemysController;
use App\Entity\Media;
use App\Entity\Product;
use App\Repository\ProductRepository;
use App\Service\Product\ProductFolderInterface;
use App\Service\Product\ProductMediaInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class DocumentController extends ArtemysController
{
    /**
     * @throws NonUniqueResultException
     */
    public function download(?Media $media): BinaryFileResponse|Response
    {
        if (null === $media) {
            return $this->goto404();
        }

        /** @var ProductRepository $repo */
        $repo = $this->em->getRepository(Product::class);
        $product = $repo->getProductByMedia($media);

        if (!$product) {
            return $this->goto404();
        }

        $order = 1;
        foreach ($product->getMedias() as $m) {
            if ($m->getType() !== $media->getType()) {
                continue;
            }
            if ($media === $m) {
                break;
            }
            ++$order;
        }

        $file = new File($media->getUrl());
        $filename = $media->getType()->getName() . ' ' . $order . '.' . $file->guessExtension();
        $response = new BinaryFileResponse($media->getUrl());
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );

        return $response;
    }

    public function downloadAll(
        ?Product $product,
        ProductMediaInterface $productMedia,
        ProductFolderInterface $productFolder
    ): Response {
        if (null === $product) {
            return $this->goto404();
        }

        $zip = new \ZipArchive();
        $zipName = 'artemys.zip';
        $zip->open('data/' . $zipName, \ZipArchive::CREATE);

        $folderPath = $productFolder
            ->set($product)
            ->getPath()
        ;
        $zip->addFile($folderPath, 'dossier.pdf');

        foreach ($productMedia->getDocumentsByProduct($product) as $type) {
            $folder = $type['name'];
            $zip->addEmptyDir($folder);
            foreach ($type['documents'] as $k => $doc) {
                $file = new File($doc['url']);
                $filename = $folder . ' ' . ($k + 1) . '.' . $file->guessExtension();
                $zip->addFile($doc['url'], $folder . '/' . $filename);
            }
        }
        $zip->close();

        $response = new Response();
        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-Type', 'application/zip');
        $response->headers->set('Content-Disposition', 'attachment;filename="' . $zipName . '"');
        $response->headers->set('Content-length', (string) \filesize('data/' . $zipName));
        $response->sendHeaders();
        $response->setContent(\file_get_contents('data/' . $zipName));
        \unlink('data/' . $zipName);

        return $response;
    }
}
