<?php

namespace App\Controller\Front;

use App\Controller\ArtemysController;
use App\Service\Customer\CustomerFormInterface;
use Symfony\Component\HttpFoundation\Response;

class ContactController extends ArtemysController
{
    public function index(CustomerFormInterface $customerForm): Response
    {
        $datas = $customerForm->save();
        if ($datas['isRedirect']) {
            return $this->redirectToRoute('front_contact');
        }

        return $this->render('front/contact.html.twig', $datas);
    }
}
