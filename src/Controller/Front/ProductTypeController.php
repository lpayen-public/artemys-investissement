<?php

namespace App\Controller\Front;

use App\Controller\ArtemysController;
use App\Entity\ProductType;
use App\Service\Map\MapInterface;
use App\Service\Slugger\SluggerUrlInterface;
use Symfony\Component\HttpFoundation\Response;

class ProductTypeController extends ArtemysController
{
    public function index(string $typeSlug, SluggerUrlInterface $sluggerUrl, MapInterface $map): Response
    {
        /**
         * @var ProductType|null $productType
         * @var string           $redirectUrl
         */
        [$productType, $redirectUrl] = $sluggerUrl->getProductType($typeSlug);

        if ($productType) {
            $cities = $map
                ->init()
                ->setDatas($productType->getProducts(true))
                ->getDatas()
            ;

            return $this->render('front/product_type.html.twig', [
                'productType' => $productType,
                'products' => $productType->getProducts(true),
                'cities' => $cities,
            ]);
        }

        if ($redirectUrl) {
            return $this->redirectToRoute(
                'front_product_type',
                ['typeSlug' => $redirectUrl],
                Response::HTTP_MOVED_PERMANENTLY
            );
        }

        return $this->goto404();
    }
}
