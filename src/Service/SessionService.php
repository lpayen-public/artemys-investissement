<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\RequestStack;

class SessionService
{
    public function __construct(private readonly RequestStack $requestStack)
    {
    }

    public function get(string $text): mixed
    {
        $session = $this->requestStack->getSession();

        return $session->get($text);
    }

    public function set(string $text, $value): void
    {
        $session = $this->requestStack->getSession();
        $session->set($text, $value);
    }
}
