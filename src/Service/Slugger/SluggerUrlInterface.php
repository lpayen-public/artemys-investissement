<?php

namespace App\Service\Slugger;

use App\Entity\Product;
use App\Entity\ProductType;

interface SluggerUrlInterface
{
    public function slugProduct(Product $product): void;

    public function slugProductType(ProductType $product): void;

    public function getProduct(string $slug): array;

    public function getProductType(string $slug): array;
}
