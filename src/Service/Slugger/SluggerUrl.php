<?php

namespace App\Service\Slugger;

use App\Entity\Product;
use App\Entity\ProductType;
use App\Entity\Url;
use App\Repository\ProductRepository;
use App\Repository\ProductTypeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\String\Slugger\SluggerInterface;

class SluggerUrl implements SluggerUrlInterface
{
    public function __construct(
        private readonly SluggerInterface $slugger,
        private readonly EntityManagerInterface $em
    ) {
    }

    public function slugProduct(Product $product): void
    {
        $this->slugEntity($product);
    }

    public function slugProductType(ProductType $type): void
    {
        $this->slugEntity($type);
    }

    private function slugEntity(Product|ProductType $entity): void
    {
        $urlSlugged = \mb_strtolower($this->slugger->slug($entity->getName()));

        if ($urlSlugged === $entity->getUrl()) {
            return;
        }

        $entity->setUrl($urlSlugged);

        /** @var Url $url */
        foreach ($entity->getUrls() as $url) {
            if ($url->getValue() === $entity->getUrl()) {
                $urlExists = $url;
                break;
            }
        }

        if (!empty($urlExists)) {
            return;
        }

        $entity->addUrl(
            (new Url())->setValue($urlSlugged)
        );
    }

    /**
     * @return array|null[]
     *
     * @throws NonUniqueResultException
     */
    public function getProduct(string $slug): array
    {
        /** @var Product|null $product */
        $product = $this->em->getRepository(Product::class)->findOneBy(['url' => $slug]);

        if ($product) {
            return [$product, $product->getUrl()];
        }

        /** @var ProductRepository $repo */
        $repo = $this->em->getRepository(Product::class);

        /** @var Product|null $product */
        $product = $repo->getProductByOldUrl($slug);

        if ($product) {
            return [null, $product->getUrl()];
        }

        return [null, null];
    }

    /**
     * @return array|null[]
     *
     * @throws NonUniqueResultException
     */
    public function getProductType(string $slug): array
    {
        /** @var ProductType|null $productType */
        $productType = $this->em->getRepository(ProductType::class)->findOneBy(['url' => $slug]);

        if ($productType) {
            return [$productType, $productType->getUrl()];
        }

        /** @var ProductTypeRepository $repo */
        $repo = $this->em->getRepository(ProductType::class);

        /** @var ProductType|null $productType */
        $productType = $repo->getProductTypeByOldUrl($slug);

        if ($productType) {
            return [null, $productType->getUrl()];
        }

        return [null, null];
    }
}
