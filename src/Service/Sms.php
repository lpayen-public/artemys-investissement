<?php

namespace App\Service;

use App\Entity\Product;
use App\Entity\ProductType;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Notifier\Exception\TransportExceptionInterface;
use Symfony\Component\Notifier\Message\SmsMessage;
use Symfony\Component\Notifier\TexterInterface;

class Sms
{
    private string $from = 'Artemys';

    public function __construct(
        private readonly TexterInterface $texter,
        private readonly EntityManagerInterface $em,
        private readonly LoggerInterface $logger
    ) {
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function sendSms(string $phone, string $text): void
    {
        $date = date('d/m/Y H:i:s');
        $text = $date . PHP_EOL . $text;
        $message = new SmsMessage('+33610252814', $text, $this->from);

        try {
            $this->texter->send($message);
            $sms = (new \App\Entity\Sms())
                ->setDate(new \DateTime())
                ->setPhone($phone)
                ->setText($text)
            ;
            $this->em->persist($sms);
            $this->em->flush();
        } catch (TransportExceptionInterface $e) {
            $this->logger->error($e->getMessage());
        }
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function newProduct(Product $product): void
    {
        $this->sendSms('33610252814', $product->getName());
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function newProductType(ProductType $productType): void
    {
        $this->sendSms('33610252814', $productType->getName());
    }

    //    private function old()
    //    {
    //        $sms = (new \App\Entity\Sms())
    //            ->setDate(new DateTime())
    //            ->setPhone($phone)
    //            ->setText($text);
    //
    //        $basic  = new Basic("5df217b5", "NAbcTBT7Bn8JGeOX");
    //        $client = new Client($basic);
    //        $vonage = new VonageSms($phone, $this->from, $text);
    //
    //        try {
    //            $response = $client->sms()->send($vonage);
    //            //            dump($response);
    //            $data = $response->current();
    //            //            dump($data);
    //            $id = $data->getMessageId();
    //            //            dump($id);
    //
    //            $sms
    //                ->setMessageId($id)
    //                ->setStatus($data->getStatus());
    //        } catch (Client\Exception\Exception $exception) {
    //            $error = $exception->getCode() . ' ' . $exception->getMessage();
    //        }
    //
    //        $this->em->persist($sms);
    //        $this->em->flush();
    //    }
}
