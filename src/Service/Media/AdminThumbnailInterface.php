<?php

namespace App\Service\Media;

use App\Entity\Partner;
use App\Entity\Product;
use App\Entity\ProductType;

interface AdminThumbnailInterface
{
    public function miniaturize(Partner|Product|ProductType $entity, array $filters): void;
}
