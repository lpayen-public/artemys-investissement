<?php

namespace App\Service\Media;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class FrontThumbnail extends ThumbnailAbstract implements FrontThumbnailInterface
{
    public function miniaturize(string $url, string $filter): string
    {
        $this->store($url, $filter);

        $url = $this->cacheManager->generateUrl(
            $url,
            $filter,
            [],
            null,
            UrlGeneratorInterface::RELATIVE_PATH
        );
        $url = \str_replace('../', '', $url);

        return (string) \str_replace('resolve/', '', $url);
    }
}
