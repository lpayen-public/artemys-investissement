<?php

namespace App\Service\Media;

interface FrontThumbnailInterface
{
    public function miniaturize(string $url, string $filter): string;
}
