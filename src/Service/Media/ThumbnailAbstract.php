<?php

namespace App\Service\Media;

use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Liip\ImagineBundle\Imagine\Data\DataManager;
use Liip\ImagineBundle\Imagine\Filter\FilterManager;

abstract class ThumbnailAbstract
{
    public function __construct(
        protected readonly CacheManager $cacheManager,
        protected readonly DataManager $dataManager,
        protected readonly FilterManager $filterManager,
    ) {
    }

    public function store(string $url, string $filter): void
    {
        if (!$this->cacheManager->isStored($url, $filter)) {
            $binary = $this->dataManager->find($filter, $url);
            $binary = $this->filterManager->applyFilter($binary, $filter);
            $this->cacheManager->store($binary, $url, $filter);
        }
    }
}
