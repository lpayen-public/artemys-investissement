<?php

namespace App\Service\Media;

use App\Entity\Media;
use App\Entity\MediaType;
use App\Entity\Partner;
use App\Entity\Product;
use App\Entity\ProductType;
use App\Utils\File;
use App\Utils\Text;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\Exception\UploadException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\KernelInterface;

abstract class MediaAbstract
{
    protected ?Request $request;
    protected string $root;

    public function __construct(
        protected readonly EntityManagerInterface $em,
        protected readonly RequestStack $requestStack,
        protected readonly KernelInterface $kernel
    ) {
        $this->request = $this->requestStack->getCurrentRequest();
    }

    protected function setUpload(
        Partner|Product|ProductType $entity,
        array $files,
        int $typeId,
        bool $isUploaded,
        int $order
    ): void {
        $dir = $this->root . $entity->getToken();

        /** @var MediaType $type */
        $type = $this->em->getRepository(MediaType::class)->find($typeId);

        /** @var UploadedFile|null $file */
        foreach ($files as $file) {
            if (null === $file || $file->getError() || !$file->guessExtension()) {
                continue;
            }

            $token = Text::token();
            $ext = $file->guessExtension();
            $name = $token . '.' . $ext;
            $url = $dir . '/' . $name;

            try {
                if ($isUploaded) {
                    $file->move($dir, $name);
                } else {
                    $root = $this->kernel->getProjectDir() . '/public/';
                    File::create($root . $dir);
                    \copy($file->getPathname(), $root . $url);
                }

                $media = (new Media())
                    ->setType($type)
                    ->setToken(Text::token())
                    ->setUrl($url)
                    ->setOrder($order)
                    ->setCreatedAt()
                    ->setUpdatedAt()
                ;

                if (\method_exists($entity, 'addMedia')) {
                    $entity->addMedia($media);
                } elseif (\method_exists($entity, 'setMedia')) {
                    $entity->setMedia($media);
                }
                ++$order;
            } catch (UploadException $e) {
                echo $e->getMessage();
            }
        }
    }
}
