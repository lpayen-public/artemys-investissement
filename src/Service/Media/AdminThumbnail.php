<?php

namespace App\Service\Media;

use App\Entity\Media;
use App\Entity\Partner;
use App\Entity\Product;
use App\Entity\ProductType;

class AdminThumbnail extends ThumbnailAbstract implements AdminThumbnailInterface
{
    public function miniaturize(Partner|Product|ProductType $entity, array $filters): void
    {
        /** @var Media $media */
        foreach ($entity->getPictures() as $media) {
            foreach ($filters as $filter) {
                $this->store($media->getUrl(), $filter);
            }
        }
    }
}
