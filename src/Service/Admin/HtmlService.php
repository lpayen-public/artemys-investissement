<?php

declare(strict_types=1);

namespace App\Service\Admin;

/**
 * Class HtmlService.
 */
class HtmlService
{
    private int $iconSize = 10;
    public const SIZE = 'sm';
    public const MARGIN_FORM = 'mb30';

    public function getMarginForm(): string
    {
        return self::MARGIN_FORM;
    }

    public function getHamburger(): string
    {
        return 'fa fa-bars';
    }

    public function getPopin(): string
    {
        return 'fa fa-expand-arrows-alt btn-fullscreen pointer ml5';
    }

    public function getTable(): string
    {
        return 'table table-bordered table-striped table-hover';
    }

    public function getTableSimple(): string
    {
        return 'table table-simple';
    }

    public function getTableAction(): string
    {
        return $this->getTable() . ' table-action';
    }

    public function getDatatable(): string
    {
        return $this->getTable() . ' datatable';
    }

    public function getDatatableAction(): string
    {
        return $this->getDatatable() . ' table-action';
    }

    public function getBtnShow(string $size = null, bool $isOutline = true): string
    {
        $size = $size ?: self::SIZE;

        return 'btn ' . $this->getFormatBtn($isOutline) . '-primary btn-' . $size;
    }

    public function getIconShow(): string
    {
        return 'fas fa-eye fs' . $this->iconSize;
    }

    public function getBtnUpdate(string $size = null, bool $isOutline = true): string
    {
        $size = $size ?: self::SIZE;

        return 'btn ' . $this->getFormatBtn($isOutline) . '-warning btn-' . $size;
    }

    public function getIconUpdate(): string
    {
        return 'fas fa-pen fs' . $this->iconSize;
    }

    public function getBtnAdd(string $size = null, bool $isOutline = true): string
    {
        $size = $size ?: self::SIZE;

        return 'btn ' . $this->getFormatBtn($isOutline) . '-info btn-' . $size;
    }

    public function getIconAdd(): string
    {
        return 'fas fa-plus fs' . $this->iconSize;
    }

    public function getBtnSave(string $size = null, bool $isOutline = true): string
    {
        $size = $size ?: self::SIZE;

        return 'btn ' . $this->getFormatBtn($isOutline) . '-success btn-' . $size;
    }

    public function getBtnBack(string $size = null, bool $isOutline = true): string
    {
        $size = $size ?: self::SIZE;

        return 'btn ' . $this->getFormatBtn($isOutline) . '-secondary btn-' . $size;
    }

    public function getBtnLight(string $size = null, bool $isOutline = true): string
    {
        $size = $size ?: self::SIZE;

        return 'btn ' . $this->getFormatBtn($isOutline) . '-white btn-' . $size;
    }

    public function getBtnOption(string $size = null, bool $isOutline = true): string
    {
        return $this->getBtnBack($size, $isOutline);
    }

    public function getBtnDelete(string $size = null, bool $isOutline = true): string
    {
        $size = $size ?: self::SIZE;

        return 'btn ' . $this->getFormatBtn($isOutline) . '-danger btn-' . $size;
    }

    public function getIconDelete(): string
    {
        return 'fa fa-times fs' . $this->iconSize;
    }

    public static function getDefaultSelectValue(): string
    {
        return 'Choisir';
    }

    public function getTextAdd(): string
    {
        return 'Créer';
    }

    public function getTextUpdate(): string
    {
        return 'Modifier';
    }

    public function getTextDelete(): string
    {
        return 'Supprimer';
    }

    public function getTextSave(): string
    {
        return 'Enregistrer';
    }

    public function getTextBack(): string
    {
        return 'Retour';
    }

    public function getIconSave(): string
    {
        return '<i class="fas fa-save fs' . $this->iconSize . '"></i>';
    }

    private function getFormatBtn(bool $isOutline = true): string
    {
        return 'btn' . ($isOutline ? '-outline' : '');
    }
}
