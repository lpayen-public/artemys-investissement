<?php

namespace App\Service\Admin;

use App\Repository\FeatureRepository;
use App\Repository\ProductRepository;
use App\Repository\ProductTypeRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class SearchService
{
    private ?Request $request;

    public function __construct(
        private readonly RequestStack $requestStack,
        private readonly ProductRepository $productRepo,
        private readonly ProductTypeRepository $productTypeRepo,
        private readonly FeatureRepository $featureRepo,
    ) {
        $this->request = $this->requestStack->getCurrentRequest();
    }

    public function search(): array
    {
        $search = $this->request->get('search');
        $products = $this->productRepo->search($search);
        $productTypes = $this->productTypeRepo->search($search);
        $features = $this->featureRepo->search($search);
        $nbResults = count($products) + count($productTypes) + count($features);

        return [
            'search' => $search,
            'products' => $products,
            'productTypes' => $productTypes,
            'features' => $features,
            'nbResults' => $nbResults,
        ];
    }
}
