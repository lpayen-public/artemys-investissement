<?php

declare(strict_types=1);

namespace App\Service\Admin;

use App\Service\EnvironmentService;
use App\Service\UserService;
use Symfony\Component\Routing\RouterInterface;

class MenuService
{
    private ?string $route;
    private array $menus = [];
    private string $activeCssName = 'active';

    /**
     * @throws \Exception
     */
    public function __construct(
        private readonly RouterInterface $router,
        private readonly EnvironmentService $envService,
        private readonly UserService $userService
    ) {
        $this->route = $this->envService->getRoute();
        $this->setHomeMenu();
        //        $this->setAdminMenu();
    }

    /**
     * Called by templates/nav_menu.html.twig.
     *
     * @return array<string, mixed>
     */
    public function getMenus(): array
    {
        return $this->menus;
    }

    private function getClass(?string $routeLike, array $routesIn = [], string $excludeRouteLike = null): string
    {
        if (!$this->route) {
            return '';
        }

        if ($excludeRouteLike && preg_match('#' . $excludeRouteLike . '#', $this->route)) {
            return '';
        }

        if ($routeLike && preg_match('#' . $routeLike . '#', $this->route)) {
            return $this->activeCssName;
        }

        if (in_array($this->route, $routesIn, true)) {
            return $this->activeCssName;
        }

        return '';
    }

    private function setHomeMenu(): void
    {
        $this->menus['accueil'] = [
            'name' => 'Accueil',
            'link' => $this->router->generate('admin_index'),
            'icon' => 'ri-home-line',
            'class' => $this->getClass(null, ['admin_index']),
            'children' => [],
        ];
        $this->menus['product'] = [
            'name' => 'Biens',
            'link' => $this->router->generate('admin_product_index'),
            'icon' => 'ri-map-pin-line',
            'class' => $this->getClass('admin_product_', [], 'admin_product_type_'),
            'children' => [],
        ];
        $this->menus['productType'] = [
            'name' => 'Types de bien',
            'link' => $this->router->generate('admin_product_type_index'),
            'icon' => 'ri-building-2-line',
            'class' => $this->getClass('admin_product_type_'),
            'children' => [],
        ];
        $this->menus['contact'] = [
            'name' => 'Contacts',
            'link' => $this->router->generate('admin_customer_index'),
            'icon' => 'ri-user-6-line',
            'class' => $this->getClass('admin_customer_'),
            'children' => [],
        ];
        $this->menus['partner'] = [
            'name' => 'Partenaires',
            'link' => $this->router->generate('admin_partner_index'),
            'icon' => 'ri-shake-hands-line',
            'class' => $this->getClass('admin_partner_'),
            'children' => [],
        ];

        if (!$this->userService->isSuperAdmin()) {
            return;
        }

        $this->menus['feature'] = [
            'name' => 'Caractéristiques',
            'link' => $this->router->generate('admin_feature_index'),
            'icon' => 'ri-palette-line',
            'class' => $this->getClass('admin_feature_'),
            'children' => [],
        ];
        $this->menus['question'] = [
            'name' => 'Questions',
            'link' => $this->router->generate('admin_question_index'),
            'icon' => 'ri-list-settings-line',
            'class' => $this->getClass('admin_question_'),
            'children' => [],
        ];
        $this->menus['user'] = [
            'name' => 'Utilisateurs',
            'link' => $this->router->generate('admin_user_index'),
            'icon' => 'ri-user-line',
            'class' => $this->getClass('admin_user_'),
            'children' => [],
        ];
        $this->menus['webmaster'] = [
            'name' => 'Webmastering',
            'link' => $this->router->generate('admin_webmaster_index'),
            'icon' => 'ri-tools-line',
            'class' => $this->getClass('admin_webmaster_'),
            'children' => [],
        ];
        //        $this->menus['test']      = [
        //            'name'     => 'Test',
        //            'link'     => '',
        //            'icon'     => 'ri-line-chart-line',
        //            'class'    => $this->getClass('admin_webmaster_'),
        //            'children' => [
        //                [
        //                    'name'     => 'Test 1',
        //                    'link'     => $this->router->generate('admin_webmaster_index'),
        //                    'icon'     => 'ri-bubble-chart-line',
        //                    'class'    => $this->getClass('admin_webmaster_'),
        //                    'children' => [],
        //                ],
        //                [
        //                    'name'     => 'Test 2',
        //                    'link'     => $this->router->generate('admin_webmaster_index'),
        //                    'icon'     => 'ri-bar-chart-line',
        //                    'class'    => $this->getClass('admin_webmaster_'),
        //                    'children' => [],
        //                ],
        //            ],
        //        ];
    }

    /*
     * @throws \Exception
     */
    //    private function setAdminMenu(): void
    //    {
    //        $this->menus['admin'] = [
    //            'name'     => 'ADMINISTRATION',
    //            'link'     => null,
    //            'icon'     => 'fas fa-cogs',
    //            'class'    => $this->getClass('admin_'),
    //            'children' => [],
    //        ];
    //
    //        if ($this->authAdminService->canAccessUserModule()) {
    //            $this->menus['admin']['children'][] = [
    //                'name'     => 'Utilisateurs',
    //                'link'     => $this->router->generate('admin_user_index'),
    //                'icon'     => null,
    //                'class'    => $this->getClass('admin_user_'),
    //                'children' => [],
    //            ];
    //        }
    //
    //        if ($this->authAdminService->canAccessOfficeModule()) {
    //            $this->menus['admin']['children'][] = [
    //                'name'     => 'Cabinets',
    //                'link'     => $this->router->generate('admin_user_index'),
    //                'icon'     => null,
    //                'class'    => $this->getClass('admin_office_'),
    //                'children' => [],
    //            ];
    //        }
    //
    //        if (!count($this->menus['admin']['children'])) {
    //            unset($this->menus['admin']);
    //        }
    //    }
}
