<?php

namespace App\Service\Front;

interface HomeProductInterface
{
    public function get(int $nbProducts = 6): array;
}
