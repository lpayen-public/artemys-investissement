<?php

namespace App\Service\Front;

use App\Entity\Product;
use App\Repository\ProductRepository;

class HomeProduct implements HomeProductInterface
{
    public function __construct(private readonly ProductRepository $productRepo)
    {
    }

    public function get(int $nbProducts = 6): array
    {
        $types = [];
        $tab = [];
        foreach ($this->getProducts() as $product) {
            $types[$product->getType()->getId()][] = $product;
        }

        $i = 0;
        $limit = $nbProducts * 2;
        while (count($tab) < $nbProducts && $i < $limit) {
            [$tab, $types] = $this->handle($types, $tab, $nbProducts);
            ++$i;
        }

        return $tab;
    }

    private function handle(array $types, array $tab, int $nbProducts): array
    {
        foreach ($types as $i => $products) {
            foreach ($products as $j => $product) {
                $tab[] = $product;
                unset($types[$i][$j]);
                if (count($tab) >= $nbProducts) {
                    break 2;
                }
                continue 2;
            }
        }

        return [$tab, $types];
    }

    /**
     * @return Product[]
     */
    private function getProducts(): array
    {
        return $this->productRepo->findBy(['active' => true], ['createdAt' => 'DESC']);
    }
}
