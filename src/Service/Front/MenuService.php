<?php

namespace App\Service\Front;

use App\Entity\ProductType;
use App\Service\EnvironmentService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;

class MenuService
{
    private ?Request $request;
    private ?string $route;
    private string $activeCssName = 'active';

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly RouterInterface $router,
        private readonly EnvironmentService $envService,
        RequestStack $requestStack,
    ) {
        $this->route = $this->envService->getRoute();
        $this->request = $requestStack->getCurrentRequest();
    }

    private function getClass(array $routesIn = [], string $slug = null): string
    {
        if (!$this->route) {
            return '';
        }

        if (in_array($this->route, $routesIn, true)) {
            return $this->activeCssName;
        }

        if ($slug && false !== \strrpos($this->request->getRequestUri(), $slug)) {
            return $this->activeCssName;
        }

        return '';
    }

    /**
     * @return array[]
     */
    public function get(): array
    {
        $tab = [
            'accueil' => [
                'name' => 'Accueil',
                'link' => $this->router->generate('front_index'),
                'class' => $this->getClass(['front_index']),
            ],
        ];

        $productTypes = $this->em->getRepository(ProductType::class)->findBy(['active' => true]);
        foreach ($productTypes as $productType) {
            $tab[$productType->getUrl()] = [
                'name' => $productType->getName(),
                'link' => $this->router->generate('front_product_type', ['typeSlug' => $productType->getUrl()]),
                'class' => $this->getClass([], $productType->getUrl()),
            ];
        }

        $tab['contact'] = [
            'name' => 'Contact',
            'link' => $this->router->generate('front_contact'),
            'class' => $this->getClass(['front_contact']),
        ];

        if ($this->envService->isLuc()) {
            $tab['admin'] = [
                'name' => 'Admin',
                'link' => $this->router->generate('admin_index'),
                'class' => $this->getClass(['admin_index']),
                'targetBank' => true,
            ];
        }

        return $tab;
    }
}
