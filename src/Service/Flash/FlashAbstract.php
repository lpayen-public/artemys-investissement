<?php

namespace App\Service\Flash;

use Symfony\Component\HttpFoundation\RequestStack;

abstract class FlashAbstract
{
    public function __construct(protected readonly RequestStack $requestStack)
    {
    }

    protected function execute(string $type, mixed $message): void
    {
        // @phpstan-ignore-next-line
        $this->requestStack->getSession()->getFlashBag()->add($type, $message);
    }
}
