<?php

namespace App\Service\Flash;

class Flash extends FlashAbstract implements FlashInterface
{
    public function success(string $message, bool $withIcon = true): void
    {
        $message = ($withIcon ? '<i class="ri-check-line fs16 mr10"></i>' : '') . $message;
        $this->execute('success', $message);
    }

    public function error(string $message, bool $withIcon = true): void
    {
        $message = ($withIcon ? '<i class="ri-close-line fs16 mr10"></i>' : '') . $message;
        $this->execute('danger', $message);
    }

    public function info(string $message, bool $withIcon = true): void
    {
        $message = ($withIcon ? '<i class="ri-error-warning-line fs16 mr10"></i>' : '') . $message;
        $this->execute('info', $message);
    }

    public function warning(string $message, bool $withIcon = true): void
    {
        $message = ($withIcon ? '<i class="ri-alarm-warning-line fs16 mr10"></i>' : '') . $message;
        $this->execute('warning', $message);
    }
}
