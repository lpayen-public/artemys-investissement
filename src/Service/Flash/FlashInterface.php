<?php

namespace App\Service\Flash;

interface FlashInterface
{
    public function success(string $message, bool $withIcon = true): void;

    public function error(string $message, bool $withIcon = true): void;

    public function info(string $message, bool $withIcon = true): void;

    public function warning(string $message, bool $withIcon = true): void;
}
