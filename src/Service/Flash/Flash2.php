<?php

namespace App\Service\Flash;

class Flash2 extends FlashAbstract implements FlashInterface
{
    public function success(string $message, bool $withIcon = true): void
    {
        $this->execute('success', $message);
    }

    public function error(string $message, bool $withIcon = true): void
    {
        $this->execute('danger', $message);
    }

    public function info(string $message, bool $withIcon = true): void
    {
        $this->execute('info', $message);
    }

    public function warning(string $message, bool $withIcon = true): void
    {
        $this->execute('warning', $message);
    }
}
