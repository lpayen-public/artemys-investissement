<?php

namespace App\Service\User;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class UserFilter implements UserFilterInterface
{
    private ?Request $request;

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly RequestStack $requestStack,
        private readonly PaginatorInterface $paginator
    ) {
        $this->request = $this->requestStack->getCurrentRequest();
    }

    public function index(): array
    {
        $page = $this->request->query->getInt('page', 1);

        $repo = $this->em->getRepository(User::class)->findBy([], ['lastname' => 'ASC']);
        $users = $this->paginator->paginate($repo, $page, 10);
        $nbUsers = count($repo);

        if (!count($users) && $page > 1) {
            $this->request->query->set('page', 1);

            return $this->index();
        }

        return [
            'users' => $users,
            'nbUsers' => $nbUsers,
        ];
    }
}
