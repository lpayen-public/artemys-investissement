<?php

namespace App\Service\User;

use App\Entity\User;

interface UserPasswordInterface
{
    public function hash(User $user, string $password): string;
}
