<?php

namespace App\Service\User;

use App\Entity\User;

interface UserFormInterface
{
    public function init(User $User): void;

    public function setForm(User $User, bool $isForProfil = false): array;

    public function delete(User $User): void;
}
