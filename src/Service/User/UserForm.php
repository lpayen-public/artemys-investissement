<?php

namespace App\Service\User;

use App\Entity\User;
use App\Entity\UserConfiguration;
use App\Event\UserEvent;
use App\Form\UserType;
use App\Repository\UserConfigurationRepository;
use App\Service\ConfigurationService;
use App\Utils\Text;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class UserForm implements UserFormInterface
{
    private ?Request $request;

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly RequestStack $requestStack,
        private readonly FormFactoryInterface $formFactory,
        private readonly CsrfTokenManagerInterface $csrfToken,
        private readonly EventDispatcherInterface $dispatcher,
        private readonly ConfigurationService $configuration
    ) {
        $this->request = $this->requestStack->getCurrentRequest();
    }

    public function init(User $user): void
    {
        if (!$user->getToken()) {
            $user->setToken(Text::token());
        }
    }

    public function setForm(User $user, bool $isForProfil = false): array
    {
        $this->init($user);

        $form = $this->formFactory->create(UserType::class, $user, [
            'isForProfil' => $isForProfil,
            'color' => $this->getUserColor($user),
        ]);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $eventType = $user->getId()
                ? UserEvent::USER_UPDATE
                : UserEvent::USER_ADD;

            $this->em->persist($user);
            $this->em->flush();

            $this->configuration->setColor($this->request->get('user')['color'], $user);
            $this->dispatcher->dispatch(new UserEvent($user), $eventType);

            $isRedirect = true;
        }

        return [
            'user' => $user,
            'form' => $form,
            'isRedirect' => $isRedirect ?? false,
        ];
    }

    private function getUserColor(?User $user): string
    {
        /** @var UserConfigurationRepository $repo */
        $repo = $this->em->getRepository(UserConfiguration::class);
        $userColor = $repo->getUserColor($user);

        if ($userColor) {
            return $userColor;
        }

        $confColor = $this->configuration->getColor(false);

        return $confColor ?: '#2980B9';
    }

    public function delete(User $user): void
    {
        $token = new CsrfToken(
            'deleteUser' . $user->getToken(),
            $this->request->request->get('_token')
        );

        if (!$this->csrfToken->isTokenValid($token)) {
            return;
        }

        $this->em->remove($user);
        $this->em->flush();
    }
}
