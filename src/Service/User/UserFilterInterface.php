<?php

namespace App\Service\User;

interface UserFilterInterface
{
    public function index(): array;
}
