<?php

namespace App\Service\Webmaster;

interface WebmasterLogInterface
{
    public function get(): array;

    public function read(string $path): ?string;

    public function delete(string $path): void;
}
