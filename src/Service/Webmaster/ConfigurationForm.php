<?php

namespace App\Service\Webmaster;

use App\Entity\Configuration;
use App\Form\ConfigurationsType;
use App\Service\Flash\FlashInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class ConfigurationForm implements ConfigurationFormInterface
{
    private ?Request $request;

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly RequestStack $requestStack,
        private readonly FormFactoryInterface $formFactory,
        private readonly FlashInterface $flash
    ) {
        $this->request = $this->requestStack->getCurrentRequest();
    }

    public function setForm(): array
    {
        $confs = $this->em->getRepository(Configuration::class)->findAll();

        $form = $this->formFactory->create(ConfigurationsType::class, ['configurations' => $confs]);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->flash->success('Les configurations ont été enregistrées avec succès');
            $isRedirect = true;
        }

        return [
            'form' => $form,
            'isRedirect' => $isRedirect ?? false,
        ];
    }
}
