<?php

namespace App\Service\Webmaster;

use Symfony\Component\HttpKernel\KernelInterface;

class WebmasterLog implements WebmasterLogInterface
{
    public function __construct(private readonly KernelInterface $kernel)
    {
    }

    public function get(): array
    {
        $logFiles = [];
        $last = [];
        $files = [];

        // Liste des morceaux de texte qu'on a défini dans la conf EXCLUDE_LOGS comme étant à exclure des logs
        $excludes = \preg_split('#(;)#', $this->getExcludeErrors());
        foreach ($excludes as $k => $exclude) {
            $exclude = $excludes[$k] = \trim($exclude);
            if ('' == $exclude) {
                unset($excludes[$k]);
            }
        }

        // Liste des fichiers du plus récent au plus ancien
        foreach (\array_reverse(\glob($this->getRootLog() . '*')) as $file) {
            if (!\preg_match('#(.log)#', $file)) {
                continue;
            }

            $fileName = \str_replace($this->getRootLog(), '', $file);
            $date = \preg_replace('#([^\d]+)#', '', $fileName);
            $files[] = $fileName;

            $dt = $date
                ? \DateTime::createFromFormat('Ymd', $date)
                : new \DateTime();

            $year = $dt->format('Y');
            $month = $dt->format('n');
            $day = $dt->format('d');
            $env = \str_replace('.log', '', \preg_replace('#([\d-]+)#', '', $fileName));
            $lines = \file($file);
            $filename = ($date ? $year . '-' . $dt->format('m') . '-' . $day . '-' : '') . $env . '.log';

            foreach ($lines as $line) {
                // LOG_EXCLUDE_ERROR est une conf en DB et ne doit pas être considérée comme une erreur
                if (!\preg_match('#(ERROR|CRITICAL)#', $line) || \preg_match('#(LOG_EXCLUDE_ERROR)#', $line)) {
                    continue;
                }

                // On a un morceau de texte qu'on a défini dans la conf EXCLUDE_LOGS comme étant à exclure des logs
                // On passe donc à la ligne suivante des logs
                foreach ($excludes as $exclude) {
                    if (str_contains($line, $exclude)) {
                        continue 2;
                    }
                }

                $errorType = \preg_match('#(ERROR)#', $line)
                    ? 'ERROR'
                    : 'CRITICAL';

                $isCritical = (bool) \preg_match('#(CRITICAL)#', $line);

                $lineDate = \substr($line, 0, 35);
                $hour = \substr($lineDate, 12, 8);
                $line = \str_replace($lineDate, '', $line);
                $line = \preg_replace('#(at )#', 'at <br>', $line);
                $text = \str_replace(['request.ERROR: ', 'request.CRITICAL: '], ['', ''], $line);
                $link = \str_replace($this->getRootLog(), '', $file);

                $text = \str_replace('//', '/', $text);
                $text = \str_replace('\\\\', '\\', $text);

                $text = \preg_replace('#(line )([\d]+)#', '<span class="num-line radius3">$1$2</span>', $text);
                $text = \preg_replace(
                    '#(wp-login|.env|dashboard)#',
                    '<span class="type-error radius3">$1</span>',
                    $text
                );
                $text = \preg_replace(
                    '# ([a-zA-z]+)(Bundle)#',
                    ' <span class="type-error-bundle radius3">$1$2</span>',
                    $text
                );
                $text = \preg_replace(
                    '#(Unable to find template|Compilation failed)#',
                    '<span class="type-error-twig radius3">$1</span>',
                    $text
                );
                $text = \preg_replace(
                    '#(An exception occurred while executing)#',
                    '<span class="type-error-dql radius3">$1</span>',
                    $text
                );
                $text = \preg_replace(
                    '#Variable "([\w]+)" does not exist#',
                    '<span class="type-error-twig radius3">Variable "$1" does not exist</span>',
                    $text
                );
                $text = \preg_replace(
                    '#(must be of the type)#',
                    '<span class="type-error-twig radius3">$1</span>',
                    $text
                );
                $text = \preg_replace('#(No route found)#', '<span class="type-error-twig radius3">$1</span>', $text);

                $text = \preg_replace(
                    '#\\\\([a-zA-z]{1,})\\\\([a-zA-z]{1,})Service.php#',
                    '$1\<span class="type-error-service radius3">$2Service.php</span>',
                    $text
                );
                $text = \preg_replace(
                    '#\\\\([a-zA-z]{1,})\\\\([a-zA-z]{1,}).html.twig#',
                    '$1\<span class="type-error-service radius3">$2Service.php</span>',
                    $text
                );

                $text = \preg_replace(
                    '#([a-zA-z]{1,})Service\\\\([a-zA-z]{1,})(Service::)([a-zA-z]{1,})\\(\\)#',
                    '$1Service\<span class="type-error-service radius3">$2$3$4$5()</span>',
                    $text
                );

                if (!isset($logFiles[$year][$month][$day][$env])) {
                    $logFiles[$year][$month][$day][$env] = [
                        'filename' => $filename,
                        'link' => $link,
                        'lines' => [],
                    ];
                }

                $logFiles[$year][$month][$day][$env]['lines'][] = [
                    'text' => $text,
                    'errorType' => $errorType,
                    'hour' => $hour,
                    'isCritical' => $isCritical,
                ];
            }

            if (!$date) {
                $last = $logFiles[$year][$month][$day] ?? null;
            }
        }

        return [
            'logFiles' => $logFiles,
            'last' => $last,
            'files' => $files,
        ];
    }

    public function read(string $path): ?string
    {
        $file = $this->getRootLog() . $path;
        $lines = \file($file);
        $text = null;

        foreach ($lines as $line) {
            $text .= $line . '<br>';
        }

        return $text;
    }

    public function delete(string $path): void
    {
        $file = $this->getRootLog() . $path;

        if (\file_exists($file)) {
            \unlink($file);
        }
    }

    private function getExcludeErrors(): string
    {
        return '/.git;/apple-touch-icon;/robots.txt;/app-ads.txt;/ads.txt;/summernote-bs4.js.map;wp-login.php';
    }

    private function getRootLog(): string
    {
        return $this->kernel->getProjectDir() . '/var/log/';
    }
}
