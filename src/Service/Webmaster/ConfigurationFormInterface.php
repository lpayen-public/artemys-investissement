<?php

namespace App\Service\Webmaster;

interface ConfigurationFormInterface
{
    public function setForm(): array;
}
