<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\KernelInterface;

class EnvironmentService
{
    private ?Request $request;
    private array $lucIps = ['127.0.0.1', '89.157.147.217'];

    public function __construct(
        private readonly KernelInterface $kernel,
        private readonly RequestStack $requestStack
    ) {
        $this->request = $this->requestStack->getCurrentRequest();
    }

    public function isProd(): bool
    {
        return 'prod' === $this->kernel->getEnvironment();
    }

    public function isDev(): bool
    {
        return !$this->isProd();
    }

    public function isLocal(): bool
    {
        return '127.0.0.1' === $this->request->getClientIp();
    }

    public function isLuc(): bool
    {
        return \in_array($this->request->getClientIp(), $this->lucIps);
    }

    /**
     * @return string|string[]
     */
    public function getMonth(int $id = null, bool $toLower = false): array|string
    {
        $tab = [
            1 => 'Janvier',
            'Février',
            'Mars',
            'Avril',
            'Mai',
            'Juin',
            'Juillet',
            'Août',
            'Septembre',
            'Octobre',
            'Novembre',
            'Décembre',
        ];

        if ($id) {
            return $toLower ? \strtolower($tab[$id]) : $tab[$id];
        }

        if ($toLower) {
            foreach ($tab as $k => &$v) {
                $v = \strtolower($v);
            }
        }

        return $tab;
    }

    public function getRoute(): ?string
    {
        return $this->request?->get('_route');
    }

    /**
     * Rafraichissement du cache des CSS et JS.
     *
     * @throws \Exception
     */
    public function getDateCache(): string
    {
        $cache = $this->isProd() ? date('ymd') : date('YmdHis');

        return '?' . $cache;
    }
}
