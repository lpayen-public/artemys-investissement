<?php

namespace App\Service\Payment;

class SocieteGenerale extends PaymentAbstract implements PaymentInterface
{
    public function pay(float $amount): void
    {
        $this->debug->pr('Société Générale', 'Provider');
        $this->debug->pr($this->getPublicKey(), 'Public key');
        $this->debug->pr($this->getPrivateKey(), 'Private key');
    }
}
