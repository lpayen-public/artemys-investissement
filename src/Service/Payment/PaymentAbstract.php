<?php

namespace App\Service\Payment;

use App\Utils\Debug;

class PaymentAbstract
{
    private string $publicKey;
    private string $privateKey;

    public function __construct(array $keys, protected readonly Debug $debug)
    {
        $this->publicKey = $keys['publicKey'];
        $this->privateKey = $keys['privateKey'];
    }

    public function getPublicKey(): string
    {
        return $this->publicKey;
    }

    public function getPrivateKey(): string
    {
        return $this->privateKey;
    }
}
