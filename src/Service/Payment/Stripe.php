<?php

namespace App\Service\Payment;

class Stripe extends PaymentAbstract implements PaymentInterface
{
    public function pay(float $amount): void
    {
        $this->debug->pr('Stripe', 'Provider');
        $this->debug->pr($this->getPublicKey(), 'Public key');
        $this->debug->pr($this->getPrivateKey(), 'Private key');
    }
}
