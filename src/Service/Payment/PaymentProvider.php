<?php

namespace App\Service\Payment;

use Psr\Log\LoggerInterface;

class PaymentProvider
{
    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly Stripe $stripe,
        private readonly Paypal $paypal,
        private readonly SocieteGenerale $societeGenerale,
        private readonly CreditAgricole $creditAgricole
    ) {
    }

    public function gotoPayment(int $key, float $amount): void
    {
        $this->pay($this->getProvider($key), $amount);
    }

    private function pay(PaymentInterface $payment, float $amount): void
    {
        $payment->pay($amount);
    }

    private function getProvider(int $key): PaymentInterface
    {
        switch ($key) {
            case 1:
                return $this->stripe;

            case 2:
                return $this->paypal;

            case 3:
                return $this->societeGenerale;

            case 4:
                return $this->creditAgricole;

            default:
                $this->logger->critical(
                    "XXXXXXXXXXX Tentative de connexion à un Payment Provider inexistant (clé {$key})"
                );

                return $this->stripe;
        }
    }
}
