<?php

namespace App\Service\Product;

use App\Entity\Product;
use App\Form\ProductFilterType;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class ProductFilter implements ProductFilterInterface
{
    private ?Request $request;

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly RequestStack $requestStack,
        private readonly FormFactoryInterface $formFactory,
        private readonly PaginatorInterface $paginator
    ) {
        $this->request = $this->requestStack->getCurrentRequest();
    }

    public function index(): array
    {
        $page = $this->request->query->getInt('page', 1);
        $form = $this->formFactory->create(ProductFilterType::class, [
            'page' => $page,
        ]);
        $form->handleRequest($this->request);

        $datas = [];
        if ($form->isSubmitted() && $form->isValid()) {
            $datas = $form->getData();
        }

        /** @var ProductRepository $repo */
        $repo = $this->em->getRepository(Product::class);
        $productsRepo = $repo->getByFilters($datas);
        $products = $this->paginator->paginate($productsRepo, $page, 10);

        if (!count($products) && $page > 1) {
            $this->request->query->set('page', 1);

            return $this->index();
        }

        return [
            'products' => $products,
            'nbPoducts' => count($productsRepo),
            'form' => $form,
        ];
    }
}
