<?php

namespace App\Service\Product;

use App\Entity\Product;

interface ProductFolderInterface
{
    public function set(Product $product): static;

    public function getPath(): ?string;

    public function render(string $format = 'I');
}
