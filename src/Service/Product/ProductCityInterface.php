<?php

namespace App\Service\Product;

use App\Entity\Product;

interface ProductCityInterface
{
    public function search(): array;

    public function save(Product $product): void;
}
