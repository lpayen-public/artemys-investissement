<?php

namespace App\Service\Product;

use App\Entity\Product;

interface ProductMediaInterface
{
    public function upload(
        Product $product,
        bool $isUploaded = true,
        array $pictures = [],
        array $brochures = [],
        array $plans = []
    ): void;

    public function delete(): void;

    public function order(): ?Product;

    public function getDocumentsByProduct(Product $product): array;
}
