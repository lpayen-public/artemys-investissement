<?php

namespace App\Service\Product;

use App\Entity\Product;
use App\Form\ProductPlotType;
use App\Service\Flash\FlashInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;

class ProductPlotForm implements ProductPlotFormInterface
{
    private ?Request $request;

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly RequestStack $requestStack,
        private readonly FormFactoryInterface $formFactory,
        private readonly FlashInterface $flash,
        private readonly RouterInterface $router
    ) {
        $this->request = $this->requestStack->getCurrentRequest();
    }

    public function setForm(Product $product): array
    {
        $form = $this->formFactory->create(ProductPlotType::class, $product);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->flash->success('Les lots ont été enregistrés avec succès');
            $redirect = $this->getUrl($product);
        }

        return [
            'product' => $product,
            'form' => $form,
            'redirect' => $redirect ?? null,
        ];
    }

    private function getUrl(Product $product): string
    {
        if ($this->request->get('formStay')) {
            return $this->router->generate(
                'admin_product_edit_plot',
                ['token' => $product->getToken()]
            );
        }

        return $this->router->generate('admin_product_index');
    }
}
