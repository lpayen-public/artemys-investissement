<?php

namespace App\Service\Product;

interface ProductFilterInterface
{
    public function index(): array;
}
