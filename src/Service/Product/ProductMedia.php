<?php

namespace App\Service\Product;

use App\Entity\Media;
use App\Entity\MediaType;
use App\Entity\Product;
use App\Repository\ProductRepository;
use App\Service\Media\MediaAbstract;
use App\Utils\File;
use Doctrine\ORM\NonUniqueResultException;

class ProductMedia extends MediaAbstract implements ProductMediaInterface
{
    protected string $root = 'data/product/';

    public function upload(
        Product $product,
        bool $isUploaded = true,
        array $pictures = [],
        array $brochures = [],
        array $plans = []
    ): void {
        if (null !== $this->request) {
            $pictures = $this->request->files->get('product')['pictures'] ?? $pictures;
            $brochures = $this->request->files->get('product')['brochures'] ?? $brochures;
            $plans = $this->request->files->get('product')['plans'] ?? $plans;
        }
        $this->uploadByType($product, $pictures, MediaType::PICTURE, $isUploaded);
        $this->uploadByType($product, $brochures, MediaType::BROCHURE, $isUploaded);
        $this->uploadByType($product, $plans, MediaType::PLAN, $isUploaded);
    }

    private function uploadByType(Product $product, array $files, int $typeId, bool $isUploaded = true): void
    {
        if (!\count($files)) {
            return;
        }

        $order = 0;
        foreach ($product->getMedias() as $media) {
            if ($typeId === $media->getType()->getId()) {
                $order = $media->getOrder() + 1;
            }
        }

        $this->setUpload($product, $files, $typeId, $isUploaded, $order);
    }

    /**
     * @throws NonUniqueResultException
     */
    public function delete(): void
    {
        $mediaToken = $this->request->request->get('mediaToken');

        /** @var Media|null $media */
        $media = $mediaToken
            ? $this->em->getRepository(Media::class)->findOneBy(['token' => $mediaToken])
            : null;

        if (!$media) {
            return;
        }

        /** @var ProductRepository $repo */
        $repo = $this->em->getRepository(Product::class);
        $product = $repo->getProductByMedia($media);

        if (!$product) {
            return;
        }

        if (MediaType::PICTURE === $media->getType()->getId() && \count($product->getPictures()) <= 1) {
            return;
        }

        $url = $media->getUrl();
        $product->removeMedia($media);
        $this->em->remove($media);
        $this->em->flush();
        File::delete($url);
    }

    /**
     * @throws NonUniqueResultException
     */
    public function order(): ?Product
    {
        $mediaToken = $this->request->request->get('mediaToken');
        $direction = $this->request->request->get('direction');
        $order = 'up' === $direction ? 'DESC' : 'ASC';
        $otherMedia = null;
        $isNext = false;

        /** @var Media|null $media */
        $media = $mediaToken
            ? $this->em->getRepository(Media::class)->findOneBy(['token' => $mediaToken])
            : null;

        if (!$media || !$direction) {
            return null;
        }

        /** @var ProductRepository $repo */
        $repo = $this->em->getRepository(Product::class);
        $product = $repo->getProductByMedia($media);

        if (!$product) {
            return null;
        }

        $typeId = $media->getType()->getId();
        $medias = $product->getMedias()->toArray();

        if ('DESC' === $order) {
            $medias = \array_reverse($medias);
        }

        /** @var Media $m */
        foreach ($medias as $m) {
            if ($typeId !== $m->getType()->getId()) {
                continue;
            }
            if ($m === $media) {
                $isNext = true;

                continue;
            }
            if ($isNext) {
                $otherMedia = $m;
                break;
            }
        }

        if ($otherMedia) {
            $order = $media->getOrder();
            $media->setOrder($otherMedia->getOrder());
            $otherMedia->setOrder($order);
            $this->em->flush();
        }

        $this->em->refresh($product);

        return $product;
    }

    public function getDocumentsByProduct(Product $product): array
    {
        $tab = [];
        /** @var Media $media */
        foreach ($product->getMedias() as $media) {
            $type = $media->getType();
            if (MediaType::PICTURE === $type->getId()) {
                continue;
            }
            if (!isset($tab[$media->getType()->getToken()])) {
                $tab[$media->getType()->getToken()] = [
                    'name' => $media->getType()->getName(),
                    'icon' => $media->getType()->getIcon(),
                    'documents' => [],
                ];
            }
            $tab[$media->getType()->getToken()]['documents'][] = [
                'token' => $media->getToken(),
                'url' => $media->getUrl(),
            ];
        }

        return $tab;
    }
}
