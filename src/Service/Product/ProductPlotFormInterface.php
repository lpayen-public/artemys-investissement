<?php

namespace App\Service\Product;

use App\Entity\Product;

interface ProductPlotFormInterface
{
    public function setForm(Product $product): array;
}
