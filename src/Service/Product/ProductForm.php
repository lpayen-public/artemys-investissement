<?php

namespace App\Service\Product;

use App\Entity\Product;
use App\Entity\ProductQuestion;
use App\Entity\ProductResponse;
use App\Event\ProductEvent;
use App\Form\ProductType;
use App\Service\Flash\FlashInterface;
use App\Service\Media\AdminThumbnailInterface;
use App\Service\Slugger\SluggerUrlInterface;
use App\Utils\Text;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class ProductForm implements ProductFormInterface
{
    private ?Request $request;

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly EventDispatcherInterface $dispatcher,
        private readonly RequestStack $requestStack,
        private readonly FormFactoryInterface $formFactory,
        private readonly ProductMediaInterface $media,
        private readonly CsrfTokenManagerInterface $csrfToken,
        private readonly SluggerUrlInterface $url,
        private readonly ProductCityInterface $city,
        private readonly AdminThumbnailInterface $thumbnail,
        private readonly FlashInterface $flash,
        private readonly RouterInterface $router
    ) {
        $this->request = $this->requestStack->getCurrentRequest();
    }

    public function init(Product $product): void
    {
        if (!$product->getToken()) {
            $product->setToken(Text::token());
        }

        $questions = $this->em->getRepository(ProductQuestion::class)->findAll();

        /** @var ProductQuestion $question */
        foreach ($questions as $question) {
            $hasResponse = $product->getResponses()->filter(function ($response) use ($question) {
                return $response->getQuestion() === $question;
            });

            if (!\count($hasResponse)) {
                $response = (new ProductResponse())
                    ->setProduct($product)
                    ->setQuestion($question)
                    ->setValue('')
                ;

                $product->addResponse($response);
            }
        }
    }

    public function setForm(Product $product): array
    {
        $this->init($product);

        $form = $this->formFactory->create(ProductType::class, $product);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $eventType = $product->getId()
                ? ProductEvent::PRODUCT_UPDATE
                : ProductEvent::PRODUCT_ADD;

            $this->city->save($product);
            $this->media->upload($product);
            $this->url->slugProduct($product);
            $this->thumbnail->miniaturize($product, ['thumb', 'big', 'full']);

            $this->em->persist($product);
            $this->em->flush();

            $this->flash->success('Le bien a été enregistré avec succès');
            $this->dispatcher->dispatch(new ProductEvent($product), $eventType);
            $redirect = $this->getUrl($product);
        }

        return [
            'product' => $product,
            'form' => $form,
            'redirect' => $redirect ?? null,
        ];
    }

    public function delete(Product $product): void
    {
        $token = new CsrfToken(
            'delete' . $product->getId(),
            $this->request->request->get('_token')
        );

        if (!$this->csrfToken->isTokenValid($token)) {
            return;
        }

        $this->dispatcher->dispatch(
            new ProductEvent($product),
            ProductEvent::PRODUCT_DELETE
        );

        $this->em->remove($product);
        $this->em->flush();
        $this->flash->success('Le bien a été supprimé avec succès');
    }

    private function getUrl(Product $product): string
    {
        if ($this->request->get('formStay')) {
            return $this->router->generate(
                'admin_product_edit',
                ['token' => $product->getToken()]
            );
        }

        return $this->router->generate('admin_product_index');
    }
}
