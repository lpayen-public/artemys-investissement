<?php

namespace App\Service\Product;

use App\Entity\City;
use App\Entity\Product;
use App\Repository\CityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class ProductCity implements ProductCityInterface
{
    private ?Request $request;

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly RequestStack $requestStack
    ) {
        $this->request = $this->requestStack->getCurrentRequest();
    }

    public function search(): array
    {
        $tab = [];
        $search = $this->request->request->get('search');

        /** @var CityRepository $repo */
        $repo = $this->em->getRepository(City::class);
        $cities = $repo->like($search);

        foreach ($cities as $city) {
            $tab[] = [
                'token' => $city->getToken(),
                'name' => $city->getNameZipcode(false),
            ];
        }

        $nb = 10;
        $nbCities = count($tab);
        $tab = \array_slice($tab, 0, $nb);

        return [
            'cities' => $tab,
            'nbCities' => $nbCities,
            'nbDisplayed' => $nb,
        ];
    }

    public function save(Product $product): void
    {
        $token = $this->request->get('product')['cityToken'] ?? null;

        if (null === $token || $token === $product->getCity()?->getToken()) {
            return;
        }

        /** @var City|null $city */
        $city = $this->em->getRepository(City::class)->findOneBy(['token' => $token]);

        if (null === $city) {
            return;
        }

        $product->setCity($city);
    }
}
