<?php

namespace App\Service\Product;

use App\Entity\Product;

interface ProductFormInterface
{
    public function init(Product $product): void;

    public function setForm(Product $product): array;

    public function delete(Product $product): void;
}
