<?php

namespace App\Service\Product;

use App\Entity\Media;
use App\Entity\Product;
use App\Utils\File;
use Doctrine\Common\Collections\ArrayCollection;
use setasign\Fpdi\PdfParser\CrossReference\CrossReferenceException;
use setasign\Fpdi\PdfParser\Filter\FilterException;
use setasign\Fpdi\PdfParser\PdfParserException;
use setasign\Fpdi\PdfParser\Type\PdfTypeException;
use setasign\Fpdi\PdfReader\PdfReaderException;
use setasign\Fpdi\Tcpdf\Fpdi;
use Symfony\Component\HttpKernel\KernelInterface;

class ProductFolder extends Fpdi implements ProductFolderInterface
{
    protected string $root = 'data/product/';
    protected string $filename = 'dossier.pdf';
    private ?string $folderPath = null;
    private ?string $filePath = null;
    private ?Product $product = null;

    public function __construct(
        private readonly KernelInterface $kernel,
    ) {
        parent::__construct();
    }

    private function init(Product $product): void
    {
        $this->product = $product;
        $root = $this->kernel->getProjectDir() . '/public/';
        $dir = $this->root . $this->product->getToken();
        $this->folderPath = $root . $dir . '/';
        $this->filePath = $this->folderPath . $this->filename;
        $this->create();
    }

    public function create(): void
    {
        File::create($this->folderPath);
        if (!\file_exists($this->filePath)) {
            \touch($this->filePath);
        }

        $this->SetAuthor('ARTEMYS Investissement');
        $this->SetTitle('Dossier ' . $this->product->getName());
        $this->SetSubject('');
        $this->setFontSubsetting(true);
        $this->SetFont('', '', 7);
        $this->setHeaderFont([PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN]);
        $this->setFooterFont([PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA]);
        $this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $this->SetHeaderMargin(0);
        $this->SetFooterMargin(15);
        $this->SetAutoPageBreak(true, 15);
        $this->SetFont('helvetica', '', 10, '', true);
        $this->rollbackTransaction(true);
        $this->SetMargins(15, 15, 15, true);
    }

    public function getPath(): ?string
    {
        return $this->product ? $this->filePath : null;
    }

    /**
     * @throws PdfTypeException
     * @throws CrossReferenceException
     * @throws PdfReaderException
     * @throws PdfParserException
     * @throws FilterException
     */
    public function set(Product $product): static
    {
        $this->init($product);

        $this->startPageGroup();
        $this->AddPage();

        $this->Image('data/ressource/logo_test.jpg', 40, 20, 130, 130);

        $this->setY(180);
        $this->setFont('helvetica', ' ', 30, '', true);
        $this->setColor('text', 100, 100, 100);
        $this->writeHTML('Dossier', true, false, false, false, 'C');

        $this->setY(192);
        $this->setFont('helvetica', 'b', 20, '', true);
        $this->setColor('text', 134, 172, 148);
        $this->writeHTML($this->product->getName(), true, false, false, false, 'C');

        $this->startPageGroup();

        $this->addAttachments($product->getBrochures());
        $this->addAttachments($product->getPlans());

        return $this;
    }

    /**
     * @throws CrossReferenceException
     * @throws PdfReaderException
     * @throws PdfParserException
     * @throws FilterException
     * @throws PdfTypeException
     */
    private function addAttachments(ArrayCollection $docs): void
    {
        /** @var Media $doc */
        foreach ($docs as $doc) {
            [$nbPages, $orient, $format] = $this->getInfosAttachment($doc->getUrl());
            for ($i = 1; $i <= $nbPages; ++$i) {
                $this->AddPage($orient, $format);
                $this->useTemplate($this->importPage($i));
            }
        }
    }

    /**
     * @throws CrossReferenceException
     * @throws PdfReaderException
     * @throws PdfParserException
     * @throws FilterException
     * @throws PdfTypeException
     */
    private function getInfosAttachment(string $url): array
    {
        $nbPages = $this->setSourceFile($url);

        // Gestion du format portrait ou paysage du rapport de vt
        $datas = $this->getImportedPageSize($this->importPage(1));
        $width = !empty($datas['width']) ? $datas['width'] : null;
        $height = !empty($datas['height']) ? $datas['height'] : null;
        $orient = $width && $height && $width >= $height ? 'L' : 'P';

        // A4 ou A3
        $format = 'A4';
        $largest = $width >= $height ? $width : $height;
        // Le bord le plus long d'un A4 fait 297 (mm)
        // On teste donc que s'il est plus grand que 300 (petite marge supp), c'est un A3
        if ($largest > 300) {
            $format = 'A3';
        }

        return [$nbPages, $orient, $format];
    }

    public function header(): void
    {
        $myX = $this->getPageWidth() - 5;
        $myY = $this->getPageHeight() - 5;
        $text = $this->product->getToken() . ' - ' . $this->PageNo() . ' / ' . $this->getAliasNbPages();
        $this->setTextColor(200, 200, 200);
        $this->startTransform();
        $this->rotate(90, $myX, $myY);
        $this->setFont('', '', 5);
        $this->text($myX, $myY, $text);
        $this->stopTransform();
        $this->setTextColor(0, 0, 0);
    }

    public function footer(): void
    {
    }

    public function render(string $format = 'I'): void
    {
        $this->Output($this->filePath, 'F');
        $this->Output($this->filePath, $format);
    }
}
