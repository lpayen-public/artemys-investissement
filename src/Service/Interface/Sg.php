<?php

namespace App\Service\Interface;

class Sg extends PaymentAbstract implements PaymentInterface
{
    public function supports(string $slug): bool
    {
        return 'sg' === $slug;
    }

    public function test(float $total): float
    {
        $this->debug->pr('SOCIETE GENERALE');

        return $total * 1.1;
    }
}
