<?php

namespace App\Service\Interface;

interface PaymentInterface
{
    public function supports(string $slug): bool;

    public function test(float $total): float;
}
