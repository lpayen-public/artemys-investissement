<?php

namespace App\Service\Interface;

class PaymentHandler
{
    private iterable $banks;

    public function __construct(iterable $banks)
    {
        $this->banks = $banks;
    }

    public function pay(string $slug, float $amount): float
    {
        foreach ($this->banks as $bank) {
            if ($bank->supports($slug)) {
                return $bank->test($amount);
            }
        }

        return $amount;
    }
}
