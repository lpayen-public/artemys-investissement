<?php

namespace App\Service\Interface;

use App\Utils\Debug;

abstract class PaymentAbstract
{
    public function __construct(protected readonly Debug $debug)
    {
    }
}
