<?php

namespace App\Service\Interface;

class Ca extends PaymentAbstract implements PaymentInterface
{
    public function supports(string $slug): bool
    {
        return 'ca' === $slug;
    }

    public function test(float $total): float
    {
        $this->debug->pr('CREDIT AGRICOLE');

        return $total * 1.2;
    }
}
