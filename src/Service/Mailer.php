<?php

namespace App\Service;

use App\Entity\Customer;
use App\Entity\Product;
use App\Entity\ProductType;
use App\Entity\User;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\File;

class Mailer
{
    private string $from = 'luc.payen@live.com';

    public function __construct(
        private readonly MailerInterface $mailer,
        private readonly LoggerInterface $logger,
        private readonly KernelInterface $kernel
    ) {
    }

    private function sendEmail(
        array|string $to,
        string $subject,
        string $template,
        array $params = [],
        array|string $pjs = null
    ): void {
        $dir = $this->kernel->getProjectDir() . '/public/';
        $part = new DataPart(new File($dir . 'logo_front.svg'));
        $part->setContentId('@logo');

        $email = (new TemplatedEmail())
            ->from(new Address($this->from, 'Artemys'))
            ->subject($subject . $this->getTime())
//            ->addPart($part->asInline())
            ->addPart((new DataPart(fopen($dir . 'logo_front.svg', 'r'), 'logo', 'image/svg+xml'))->asInline())
            ->htmlTemplate($template)
            ->context($params)
        ;

        $tos = \is_array($to) ? $to : [$to];
        foreach ($tos as $to) {
            $email->addTo($to);
        }

        if (null !== $pjs) {
            $pjs = \is_array($pjs) ? $pjs : [$pjs];
            foreach ($pjs as $pj) {
                $email->addPart(new DataPart(new File($pj)));
            }
        }

        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {
            $this->logger->error($e->getMessage());
        }
    }

    public function addCustomer(Customer $customer): void
    {
        $to = ['luc.payen.dev@gmail.com', 'luc.payen@live.com'];
        $this->sendEmail(
            $to,
            'Nouveau contact',
            'admin/email/customer/add.html.twig',
            ['customer' => $customer]
        );
    }

    public function addProduct(Product $product): void
    {
        $this->sendEmail(
            'luc.payen.dev@gmail.com',
            'Nouveau bien',
            'admin/email/product/add.html.twig',
            ['product' => $product]
        );
    }

    public function addProductType(ProductType $productType): void
    {
        $this->sendEmail(
            'luc.payen.dev@gmail.com',
            'Nouveau type de bien',
            'admin/email/productType/add.html.twig',
            ['productType' => $productType]
        );
    }

    public function addUser(User $user): void
    {
        $this->sendEmail(
            'luc.payen.dev@gmail.com',
            'Nouvel utilisateur',
            'admin/email/user/add.html.twig',
            ['user' => $user]
        );
    }

    public function test(): void
    {
        $this->sendEmail(
            'luc.payen@live.com',
            'Test',
            'admin/email/test/test.html.twig',
        );
    }

    private function getTime(): string
    {
        return ' ' . (new \DateTime())->format('H\hi');
    }
}
