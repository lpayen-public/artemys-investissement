<?php

namespace App\Service\Map;

use App\Entity\Product;
use Doctrine\Common\Collections\Collection;

interface MapInterface
{
    public function init(): static;

    public function setDatas(Collection $products): static;

    public function pin(Product $product): static;

    public function calculate(): static;

    public function getDatas(): array;
}
