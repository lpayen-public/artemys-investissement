<?php

namespace App\Service\Map;

use App\Entity\Product;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Routing\RouterInterface;

class Map implements MapInterface
{
    private array $datas = [];
    private float $minLong = 0;
    private float $maxLong = 0;
    private float $moyLong = 0;
    private float $diffLong = 0;
    private float $minLat = 0;
    private float $maxLat = 0;
    private float $moyLat = 0;
    private float $diffLat = 0;
    private int $zoom = 10;
    private int $minZoom = 6;
    private int $maxZoom = 12;

    public function __construct(private readonly RouterInterface $router)
    {
    }

    public function init(): static
    {
        return $this;
    }

    public function setDatas(Collection $products): static
    {
        /** @var Product $product */
        foreach ($products as $product) {
            $this->pin($product);
        }

        $this->calculate();

        return $this;
    }

    public function pin(Product $product): static
    {
        $city = $product->getCity();
        $long = $city->getLongitude();
        $lat = $city->getLatitude();
        $link = $this->router->generate('front_product', [
            'typeSlug' => $product->getType()->getUrl(),
            'productSlug' => $product->getUrl(),
        ]);

        $this->datas[] = [
            'longitude' => $long,
            'latitude' => $lat,
            'className' => $product->getToken(),
            'text' => $product->getCityZipcode(),
            'link' => $link,
        ];

        $this->setMinLongitude($long);
        $this->setMaxLongitude($long);
        $this->setMinLatitude($lat);
        $this->setMaxLatitude($lat);

        return $this;
    }

    public function calculate(): static
    {
        foreach ($this->datas as $data) {
            $this->moyLong += $data['longitude'];
            $this->moyLat += $data['latitude'];
        }

        $this->diffLong = abs($this->maxLong - $this->minLong);
        $this->diffLat = abs($this->maxLat - $this->minLat);
        $this->zoom = (int) ($this->diffLong - 1);
        $this->moyLat = $this->minLat + abs($this->maxLat - $this->minLat) / 2;
        $this->moyLong = $this->minLong + abs($this->maxLong - $this->minLong) / 2;

        return $this;
    }

    public function getDatas(): array
    {
        return [
            'datas' => $this->datas,
            'minLong' => $this->minLong,
            'maxLong' => $this->maxLong,
            'moyLong' => $this->moyLong,
            'diffLong' => $this->diffLong,
            'minLat' => $this->minLat,
            'maxLat' => $this->maxLat,
            'moyLat' => $this->moyLat,
            'diffLat' => $this->diffLat,
            'zoom' => $this->zoom,
            'minZoom' => $this->minZoom,
            'maxZoom' => $this->maxZoom,
        ];
    }

    private function getMinLatitude(): float
    {
        return $this->minLat;
    }

    private function setMinLatitude($lat): void
    {
        if (!$this->getMinLatitude() || $this->getMinLatitude() > $lat) {
            $this->minLat = $lat;
        }
    }

    private function getMaxLatitude(): float
    {
        return $this->maxLat;
    }

    private function setMaxLatitude($lat): void
    {
        if (!$this->getMaxLatitude() || $this->getMaxLatitude() < $lat) {
            $this->maxLat = $lat;
        }
    }

    private function getMinLongitude(): float
    {
        return $this->minLong;
    }

    private function setMinLongitude($long): void
    {
        if (!$this->getMinLongitude() || $this->getMinLongitude() > $long) {
            $this->minLong = $long;
        }
    }

    private function getMaxLongitude(): float
    {
        return $this->maxLong;
    }

    private function setMaxLongitude($long): void
    {
        if (!$this->getMaxLongitude() || $this->getMaxLongitude() < $long) {
            $this->maxLong = $long;
        }
    }
}
