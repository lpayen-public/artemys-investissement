<?php

namespace App\Service;

use App\Entity\Configuration;
use App\Entity\User;
use App\Entity\UserConfiguration;
use App\Repository\ConfigurationRepository;
use App\Repository\UserConfigurationRepository;
use Doctrine\ORM\EntityManagerInterface;

class ConfigurationService
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly UserService $userService,
        private readonly SessionService $session,
        private readonly ConfigurationRepository $configurationRepo,
        private readonly UserConfigurationRepository $userConfigurationRepo
    ) {
    }

    public function load(): void
    {
        $user = $this->userService->getActiveUser();
        $confs = \array_merge(
            $this->configurationRepo->getDQL(),
            $this->userConfigurationRepo->getDQL($user)
        );

        foreach ($confs as $k => $v) {
            $this->session->set($k, $v);
        }
    }

    public function get(string $key, bool $bySession = true): ?string
    {
        if ($bySession) {
            return $this->session->get($key);
        }

        $conf = $this->getConf($key);

        return $conf?->getValue();
    }

    public function getColor(bool $bySession = true): string
    {
        return $this->get(Configuration::WINDOW_COLOR, $bySession);
    }

    public function setColor(?string $value, User $user = null): void
    {
        $this->setConf(Configuration::WINDOW_COLOR, $value, $user);
    }

    public function getSize(): int
    {
        return (int) $this->get(Configuration::WINDOW_SIZE);
    }

    public function setSize(?string $value): void
    {
        $this->setConf(Configuration::WINDOW_SIZE, $value);
    }

    public function setHamburger(): void
    {
        $value = 'open' !== $this->getHamburger() ? 'open' : '';
        $this->setConf(Configuration::WINDOW_HAMBURGER, $value);
    }

    public function getHamburger(): string
    {
        return $this->get(Configuration::WINDOW_HAMBURGER);
    }

    private function getConf(string $key): ?Configuration
    {
        return $this->em->getRepository(Configuration::class)->findOneBy(['name' => $key]);
    }

    private function setConf(string $key, ?string $value, User $user = null): void
    {
        $isActiveUser = !$user || $user === $this->userService->getActiveUser();
        $user = $user ?: $this->userService->getActiveUser();

        if (null === $value || !$user) {
            return;
        }

        $conf = $this->getConf($key);

        if (null === $conf) {
            return;
        }

        /** @var UserConfiguration|null $userConf */
        $userConf = $this->em->getRepository(UserConfiguration::class)->findOneBy([
            'user' => $user,
            'configuration' => $conf,
        ]);

        if (!$userConf) {
            $userConf = (new UserConfiguration())
                ->setUser($user)
                ->setConfiguration($conf)
            ;

            $this->em->persist($userConf);
        }

        if ($isActiveUser) {
            $this->session->set($key, $value);
        }
        $userConf->setValue($value);
        $this->em->flush();
    }
}
