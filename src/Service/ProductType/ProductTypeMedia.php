<?php

namespace App\Service\ProductType;

use App\Entity\MediaType;
use App\Entity\ProductType;
use App\Service\Media\MediaAbstract;

class ProductTypeMedia extends MediaAbstract implements ProductTypeMediaInterface
{
    protected string $root = 'data/product-type/';

    public function upload(ProductType $productType, bool $isUploaded = true, array $pictures = []): void
    {
        if (null !== $this->request) {
            $background = $this->request->files->get('product_type')['background'];
            $pictures = isset($background) ? [$background] : $pictures;
        }

        if (!\count($pictures)) {
            return;
        }

        foreach ($productType->getMedias() as $media) {
            $productType->removeMedia($media);
        }

        $this->setUpload($productType, $pictures, MediaType::PICTURE, $isUploaded, 0);
    }
}
