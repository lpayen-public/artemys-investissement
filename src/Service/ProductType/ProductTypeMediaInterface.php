<?php

namespace App\Service\ProductType;

use App\Entity\ProductType;

interface ProductTypeMediaInterface
{
    public function upload(
        ProductType $product,
        bool $isUploaded = true,
        array $pictures = []
    ): void;
}
