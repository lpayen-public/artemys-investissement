<?php

namespace App\Service\ProductType;

use App\Entity\ProductType;

interface ProductTypeFormInterface
{
    public function init(ProductType $productType): void;

    public function setForm(ProductType $productType): array;

    public function delete(ProductType $productType): void;
}
