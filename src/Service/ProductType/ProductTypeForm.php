<?php

namespace App\Service\ProductType;

use App\Entity\Product;
use App\Entity\ProductType;
use App\Event\ProductTypeEvent;
use App\Form\ProductTypeType;
use App\Service\Flash\FlashInterface;
use App\Service\Media\AdminThumbnailInterface;
use App\Service\Slugger\SluggerUrlInterface;
use App\Utils\Text;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class ProductTypeForm implements ProductTypeFormInterface
{
    private ?Request $request;

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly EventDispatcherInterface $dispatcher,
        private readonly RequestStack $requestStack,
        private readonly FormFactoryInterface $formFactory,
        private readonly CsrfTokenManagerInterface $csrfToken,
        private readonly SluggerUrlInterface $url,
        private readonly ProductTypeMediaInterface $media,
        private readonly AdminThumbnailInterface $thumbnail,
        private readonly FlashInterface $flash,
        private readonly RouterInterface $router
    ) {
        $this->request = $this->requestStack->getCurrentRequest();
    }

    public function init(ProductType $productType): void
    {
        if (!$productType->getToken()) {
            $productType->setToken(Text::token());
        }
    }

    public function setForm(ProductType $productType): array
    {
        $this->init($productType);

        $form = $this->formFactory->create(ProductTypeType::class, $productType);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $eventType = $productType->getId()
                ? ProductTypeEvent::PRODUCT_TYPE_UPDATE
                : ProductTypeEvent::PRODUCT_TYPE_ADD;

            $this->url->slugProductType($productType);
            $this->media->upload($productType);
            $this->thumbnail->miniaturize($productType, ['full']);

            $this->em->persist($productType);
            $this->em->flush();

            $this->flash->success('Le type de bien a été enregistré avec succès');
            $this->dispatcher->dispatch(new ProductTypeEvent($productType), $eventType);
            $redirect = $this->getUrl($productType);
        }

        return [
            'productType' => $productType,
            'form' => $form,
            'redirect' => $redirect ?? null,
        ];
    }

    public function delete(ProductType $productType): void
    {
        $token = new CsrfToken(
            'delete' . $productType->getId(),
            $this->request->request->get('_token')
        );

        if (!$this->csrfToken->isTokenValid($token)) {
            return;
        }

        //        $this->dispatcher->dispatch(
        //            new ProductEvent($product),
        //            ProductEvent::PRODUCT_DELETE
        //        );

        $this->em->remove($productType);
        $this->em->flush();
        $this->flash->success('Le type de bien a été supprimé avec succès');
    }

    private function getUrl(ProductType $productType): string
    {
        if ($this->request->get('formStay')) {
            return $this->router->generate(
                'admin_product_type_edit',
                ['token' => $productType->getToken()]
            );
        }

        return $this->router->generate('admin_product_type_index');
    }
}
