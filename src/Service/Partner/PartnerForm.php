<?php

namespace App\Service\Partner;

use App\Entity\Partner;
use App\Form\PartnerType;
use App\Service\Flash\FlashInterface;
use App\Service\Media\AdminThumbnailInterface;
use App\Utils\Text;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class PartnerForm implements PartnerFormInterface
{
    private ?Request $request;

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly RequestStack $requestStack,
        private readonly FormFactoryInterface $formFactory,
        private readonly CsrfTokenManagerInterface $csrfToken,
        private readonly PartnerMediaInterface $media,
        private readonly AdminThumbnailInterface $thumbnail,
        private readonly FlashInterface $flash
    ) {
        $this->request = $this->requestStack->getCurrentRequest();
    }

    public function init(Partner $partner): void
    {
        if (!$partner->getToken()) {
            $partner->setToken(Text::token());
        }
    }

    public function setForm(Partner $partner): array
    {
        $this->init($partner);

        $form = $this->formFactory->create(PartnerType::class, $partner);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->media->upload($partner);
            $this->thumbnail->miniaturize($partner, ['thumb', 'thumb30']);
            $this->em->persist($partner);
            $this->em->flush();
            $this->flash->success('Le partenaire a été enregistré avec succès');
            $isRedirect = true;
        }

        return [
            'partner' => $partner,
            'form' => $form,
            'isRedirect' => $isRedirect ?? false,
        ];
    }

    public function delete(Partner $partner): void
    {
        $token = new CsrfToken(
            'delete' . $partner->getId(),
            $this->request->request->get('_token')
        );

        if (!$this->csrfToken->isTokenValid($token)) {
            return;
        }

        $this->em->remove($partner);
        $this->em->flush();
        $this->flash->success('Le partenaire a été supprimé avec succès');
    }
}
