<?php

namespace App\Service\Partner;

use App\Entity\MediaType;
use App\Entity\Partner;
use App\Service\Media\MediaAbstract;

class PartnerMedia extends MediaAbstract implements PartnerMediaInterface
{
    protected string $root = 'data/partner/';

    public function upload(Partner $partner, bool $isUploaded = true, array $pictures = []): void
    {
        if (null !== $this->request) {
            $background = $this->request->files->get('partner')['background'];
            $pictures = isset($background) ? [$background] : $pictures;
        }

        if (!\count($pictures)) {
            return;
        }

        $this->setUpload($partner, $pictures, MediaType::PICTURE, $isUploaded, 0);
    }
}
