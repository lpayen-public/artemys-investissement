<?php

namespace App\Service\Partner;

use App\Entity\Partner;

interface PartnerFormInterface
{
    public function init(Partner $partner): void;

    public function setForm(Partner $partner): array;

    public function delete(Partner $partner): void;
}
