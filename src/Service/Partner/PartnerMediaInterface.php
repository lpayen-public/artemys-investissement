<?php

namespace App\Service\Partner;

use App\Entity\Partner;

interface PartnerMediaInterface
{
    public function upload(
        Partner $product,
        bool $isUploaded = true,
        array $pictures = []
    ): void;
}
