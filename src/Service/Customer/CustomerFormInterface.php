<?php

namespace App\Service\Customer;

interface CustomerFormInterface
{
    public function save(): array;
}
