<?php

namespace App\Service\Customer;

use App\Entity\Customer;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class CustomerFilter implements CustomerFilterInterface
{
    private ?Request $request;

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly RequestStack $requestStack,
        private readonly PaginatorInterface $paginator
    ) {
        $this->request = $this->requestStack->getCurrentRequest();
    }

    public function index(): array
    {
        $page = $this->request->query->getInt('page', 1);

        $repo = $this->em->getRepository(Customer::class)->findBy([], ['id' => 'DESC']);
        $customers = $this->paginator->paginate($repo, $page, 10);
        $nbCustomers = count($repo);

        if (!count($customers) && $page > 1) {
            $this->request->query->set('page', 1);

            return $this->index();
        }

        return [
            'customers' => $customers,
            'nbCustomers' => $nbCustomers,
        ];
    }
}
