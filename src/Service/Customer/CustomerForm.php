<?php

namespace App\Service\Customer;

use App\Entity\Customer;
use App\Event\CustomerEvent;
use App\Form\CustomerType;
use App\Service\Flash\FlashInterface;
use App\Service\Mailer;
use App\Utils\Text;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\KernelInterface;

class CustomerForm implements CustomerFormInterface
{
    private ?Request $request;

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly EventDispatcherInterface $dispatcher,
        private readonly RequestStack $requestStack,
        private readonly FormFactoryInterface $formFactory,
        private readonly Mailer $mailer,
        private readonly KernelInterface $kernel,
        private readonly FlashInterface $flash
    ) {
        $this->request = $this->requestStack->getCurrentRequest();
    }

    public function save(): array
    {
        $customer = (new Customer())
            ->setToken(Text::token())
        ;

        $form = $this->formFactory->create(CustomerType::class, $customer);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($customer);
            $this->em->flush();

            if ('prod' === $this->kernel->getEnvironment()) {
                $this->dispatcher->dispatch(
                    new CustomerEvent($customer),
                    CustomerEvent::CUSTOMER_ADD
                );
            } else {
                $this->mailer->addCustomer($customer);
            }

            $form->initialize();
            $isRedirect = true;

            $message = $customer->getName() . ',' . PHP_EOL;
            $message .= 'Nous avons bien enregistré votre message et vous en remercions.' . PHP_EOL;
            $message .= 'Nous vous recontacterons prochainement.' . PHP_EOL;
            $message .= 'L\'équipe ARTEMYS Investissement';
            $this->flash->success($message, false);
        }

        return [
            'customer' => $customer,
            'form' => $form,
            'isRedirect' => $isRedirect ?? false,
        ];
    }
}
