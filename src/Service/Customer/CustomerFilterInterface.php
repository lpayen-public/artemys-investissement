<?php

namespace App\Service\Customer;

interface CustomerFilterInterface
{
    public function index(): array;
}
