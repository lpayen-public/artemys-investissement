<?php

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class UserService
{
    private ?UserInterface $user;

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly Security $security,
    ) {
        $this->user = $this->security->getUser();
    }

    public function getActiveUser(): null|User
    {
        return $this->user
            ? $this->em->getRepository(User::class)->find($this->user)
            : null;
    }

    public function getName(): string
    {
        if (!($user = $this->getActiveUser())) {
            return '';
        }

        return $user->getFirstname() . ' ' . $user->getLastname();
    }

    public function getJobName(): string
    {
        if (!($user = $this->getActiveUser())) {
            return '';
        }

        return $user->getJobName() ?: '';
    }

    public function isSuperAdmin(): bool
    {
        if (!($user = $this->getActiveUser())) {
            return false;
        }

        return \in_array('ROLE_SUPER_ADMIN', $user->getRoles());
    }

    public function isAtLeastAdmin(): bool
    {
        if (!($user = $this->getActiveUser())) {
            return false;
        }

        return $this->isSuperAdmin() || \in_array('ROLE_ADMIN', $user->getRoles());
    }

    public function isAdmin(): bool
    {
        if (!($user = $this->getActiveUser())) {
            return false;
        }

        return \in_array('ROLE_ADMIN', $user->getRoles());
    }
}
