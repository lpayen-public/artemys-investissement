<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240104095738 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE media_type DROP FOREIGN KEY FK_4F9988FA896DBBDE');
        $this->addSql('ALTER TABLE media_type DROP FOREIGN KEY FK_4F9988FAB03A8386');
        $this->addSql('DROP INDEX IDX_4F9988FAB03A8386 ON media_type');
        $this->addSql('DROP INDEX IDX_4F9988FA896DBBDE ON media_type');
        $this->addSql('ALTER TABLE media_type DROP created_by_id, DROP updated_by_id, DROP token, DROP created_at, DROP updated_at, DROP active');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE media_type ADD created_by_id INT DEFAULT NULL, ADD updated_by_id INT DEFAULT NULL, ADD token VARCHAR(255) NOT NULL, ADD created_at DATETIME NOT NULL, ADD updated_at DATETIME NOT NULL, ADD active TINYINT(1) DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE media_type ADD CONSTRAINT FK_4F9988FA896DBBDE FOREIGN KEY (updated_by_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE media_type ADD CONSTRAINT FK_4F9988FAB03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_4F9988FAB03A8386 ON media_type (created_by_id)');
        $this->addSql('CREATE INDEX IDX_4F9988FA896DBBDE ON media_type (updated_by_id)');
    }
}
