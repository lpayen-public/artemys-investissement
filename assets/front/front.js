import '../routing.js';
import flatpickr from 'flatpickr';
import flatpickrFr from 'flatpickr/dist/l10n/fr';
flatpickr.localize(flatpickrFr.fr);

const $ = require('jquery');
global.$ = global.jQuery = $;
require('jqueryui');

import lightbox from 'lightbox2';
lightbox.option({
    'alwaysShowNavOnTouchDevices': true,
    'wrapAround': true
});

import './styles/front.scss';
import './card';
import './product';

$(document).on('click', '#js-nav', function () {
    $('#nav-popin').removeClass('d-none');
});
$(document).on('click', '#nav-popin-close', function () {
    $('#nav-popin').addClass('d-none');
});

function displayResolution () {
    return;
    const w = $('body').width();
    const h = $('body').height();
    const html = '<span class="fs14 ml10 c-9 fw400 js-resolution"> (' + w + '/' + h + ')</span>';
    $('.js-resolution').remove();
    $('.logo-text').append(html);
}

$(window).resize(function () {
    displayResolution();
});

displayResolution();
