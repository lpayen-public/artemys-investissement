const $ = require("jquery");

function setHeightProduct() {
    const h = 9 *parseInt($('.js-product-size').outerWidth())/ 16;
    $('.js-product-size').css({height:h});
}

$(window).resize(function () {
    setHeightProduct();
});
setHeightProduct();
