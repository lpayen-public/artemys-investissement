const speed = 500;
let $parent;
let width;
let index;

function initProductSlider() {
    $('.card-product').each(function () {
        const $this = $(this);
        const $cardProductImgs = $this.find('.card-product-header');
        const $cardProductThumbs = $this.find('.card-product-thumbs');
        const $cardImgsWidth = Math.ceil(parseFloat($cardProductImgs.width()));
        const nbPictures = getNbImgs($cardProductImgs);
        $cardProductImgs.attr('data-width', $cardImgsWidth);
        if (nbPictures <= 1) {
            return;
        }
        let index = $cardProductImgs.attr('data-index');
        if ('undefined' === typeof index) {
            index = 0;
            $cardProductImgs.attr('data-index', index);
        }
        let nb = 0;
        $cardProductImgs.find('.card-product-img').each(function () {
            const $img = $(this);
            const left = $cardImgsWidth * nb - index * $cardImgsWidth;
            $img
                .attr('data-num', nb)
                .attr('data-left', left)
                .css({
                    width: $cardImgsWidth + 'px',
                    top: 0,
                    left: left,
                    position: 'absolute'
                });
            nb++;
        });
    });
}

function getNbImgs(e) {
    return e.find('.js-card-product-img').length;
}

function disableBtns() {
    $('.card-product-header').addClass('disabled');
}

function enableBtns() {
    $('.card-product-header').removeClass('disabled');
}

function setActiveThumb($parent, index) {
    $parent.find('.card-product-thumb').removeClass('active');
    $parent.find('.card-product-thumb[data-index="' + index + '"]').addClass('active');
}

function getParentCard(e) {
    const $parent = e.closest('.card-product-header');
    const w = parseInt($parent.attr('data-width'));
    const i = parseInt($parent.attr('data-index'));
    return [$parent, w, i];
}

$(document).on('click', '.card-product-arrow', function (e) {
    e.preventDefault();
    e.stopPropagation();
    const $this = $(this);
    [$parent, width, index] = getParentCard($this);
    if ($parent.hasClass('disabled')) {
        return;
    }
    const isRight = $this.hasClass('js-product-right');
    const indexOld = index;
    let indexNew;
    if (isRight) {
        index++;
        indexNew = index > getNbImgs($parent) - 1 ? 0 : index;
    } else {
        index--;
        indexNew = index >= 0 ? index : getNbImgs($parent) - 1;
    }
    $parent.attr('data-index', indexNew);
    const $actual = $parent.find('.card-product-img[data-num="' + indexOld + '"]');
    const $next = $parent.find('.card-product-img[data-num="' + indexNew + '"]');
    let actualLeft = parseInt($actual.attr('data-left'));
    let newLeft = isRight ? actualLeft + width : actualLeft - width;
    $next
        .css({left: newLeft})
        .animate({left: actualLeft + 'px'}, speed, function () {
            enableBtns();
        })
        .attr('data-left', actualLeft);
    actualLeft = isRight ? actualLeft - width : actualLeft + width;
    $actual
        .animate({left: actualLeft + 'px'}, speed)
        .attr('data-left', actualLeft);
    disableBtns();
    setActiveThumb($parent, indexNew);
});

$(document).on('click', '.card-product-thumbs', function (e) {
    e.preventDefault();
    e.stopPropagation();
});

$(document).on('click', '.card-product-thumb', function (e) {
    e.preventDefault();
    e.stopPropagation();
    const $this = $(this).hasClass('card-product-thumb') ? $(this) : $(this).parent();
    [$parent, width, index] = getParentCard($this);
    if ($parent.hasClass('disabled')) {
        return;
    }
    let indexNew = parseInt($this.attr('data-index'));
    $parent.attr('data-index', indexNew);
    if (indexNew == index) {
        return;
    }
    const $actual = $parent.find('.card-product-img[data-num="' + index + '"]');
    const $next = $parent.find('.card-product-img[data-num="' + indexNew + '"]');
    let actualLeft = parseInt($actual.attr('data-left'));
    let newLeft = indexNew > index ? actualLeft + width : actualLeft - width;
    $next
        .css({left: newLeft})
        .animate({left: actualLeft + 'px'}, speed, function () {
            enableBtns();
        })
        .attr('data-left', actualLeft);
    actualLeft = indexNew > index ? actualLeft - width : actualLeft + width;
    $actual
        .animate({left: actualLeft + 'px'}, speed)
        .attr('data-left', actualLeft);
    disableBtns();
    setActiveThumb($parent, indexNew);
});

$(window).resize(function () {
    initProductSlider();
});

initProductSlider();
