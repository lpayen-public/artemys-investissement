const $productPlot = $('#js-product-plots');
const $productPlotContent = $('#js-product-plots-content');
let nb = $productPlot.data('index');

$(document).on('click', '#js-product-plot-add', function () {
    const prototype = $productPlot.data('prototype');
    const newProductPlot = prototype.replace(/__productPlot__/g, nb);
    const $prototype = $(newProductPlot)
        .addClass('row');
    $productPlotContent
        .append($prototype);
    nb++;
    $('#js-product-no-plot').addClass('d-none');
});


$(document).on('click', '.js-product-plot-delete', function () {
    if (!confirm("Etes-vous sûr de vouloir supprimer cet élément ?")) {
        return;
    }
    $(this).closest('.js-product-plot-block').remove();
    if ($('.js-product-plot-block').length == 0) {
        $('#js-product-no-plot').removeClass('d-none');
    }
});
