import '../routing.js';
import * as bootstrap from 'bootstrap';
const $ = require('jquery');
global.$ = global.jQuery = $;
require('jqueryui');

import 'remixicon/fonts/remixicon.css';
import 'select2';
import 'jquery-slimscroll';

const tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
const tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
    return new bootstrap.Tooltip(tooltipTriggerEl);
});

import './styles/admin.scss';
import './plugin/product.js';
import './plugin/select2.js';
import './plugin/main.js';
import './plugin/screen.js';
import './plugin/customer.js';
import './plugin/alert.js';
import './product/plot.js';
import './plugin/colorpicker';
import './plugin/counter';
import './plugin/form';
import './plugin/table';

// https://symfonycasts.com/screencast/reactjs
// https://www.youtube.com/watch?v=jPi9HcP7fU8
// import './plugin/react';
