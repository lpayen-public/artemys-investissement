const $ = require("jquery");
import {sidebar, openLoader, closeLoader} from "./main";

$(document).on('click', '.js-btn-customer-show', function () {

    openLoader();
    const $this = $(this);
    const token = $this.attr('data-token');
    const query = $this.attr('data-query');
    const url = Routing.generate('admin_customer_show', {token: token}) + '?' + query;

    $.ajax({
        method: 'post',
        url: url
    }).done(function (response) {
        sidebar(response, 350);
        closeLoader();
    });

});
