const $ = require("jquery");
let $body = $('body');
let $element = $('header, nav, #main-content, #breadcrumb');
let filterVal = 'blur(0.75px)'; // Floutage du logiciel (openLoader)
let rtime;
let timeout = false;
let delta = 100;
let xhr = new XMLHttpRequest();
let speedLoading = 250;
$('main').animate({opacity: 1}, speedLoading);

// Click sur les tabs
$(document).on('click', '.box-tab a', function () {
    let $this = $(this);
    // Lien des dump SF
    if ($this.hasClass('sf-dump-ref') || $this.hasClass('sf-dump-toggle')) {
        return false;
    }
    let href = $this.attr('href');
    // S'il trouve "#", c'est que ce n'est pas un lien vers une page
    if (href.indexOf('#') !== -1) {
        let tabId = href.replace('#', '');
        let $parent = $this.parent().parent();
        // si <ul> a un id, on le concatène pour être encore plus précis
        let ulId = $parent.attr('id');
        $parent.parent().find('li a').removeClass('active');
        $parent.parent().find('.nav-tab-pane').removeClass('active');
        $this.addClass('active');
        $('.nav-tab-pane#' + tabId).addClass('active');
        setAlignBlockHeight();
        setSession(tabId, ulId);
        return false;
    }
});

// Enregistre en session une information passée en param
// (ex : les tabs quand on clique dessus pour recharger le tab actif)
function setSession(value, ulId) {
    let name = $('#route').val() + (ulId ? '_' + ulId : '');
    let route = Routing.generate('main_tab_session_ajax');
    debug(name + ', ' + value);
    xhr.abort();
    xhr = $.ajax({
        url: route,
        method: "post",
        data: {name: name, value: value}
    }).done(function (response) {
        debug(response);
    });
    return false;
}

// console.log
function debug(text, comm) {
    if (isProd() && !isWebmaster()) {
        return;
    }
    let dt = new Date;
    let hours = dt.getHours() < 10 ? '0' + dt.getHours() : dt.getHours();
    let minutes = dt.getMinutes() < 10 ? '0' + dt.getMinutes() : dt.getMinutes();
    let seconds = dt.getSeconds() < 10 ? '0' + dt.getSeconds() : dt.getSeconds();
    let bugDate = hours + ':' + minutes + ':' + seconds;
    console.log('---------------------------------- ' + bugDate);
    if (typeof comm !== 'undefined') {
        console.log(comm);
    }
    console.log(text);
}

// Floutage du logiciel
function blur() {
    $element
        .css('filter', filterVal)
        .css('webkitFilter', filterVal)
        .css('mozFilter', filterVal)
        .css('oFilter', filterVal)
        .css('msFilter', filterVal);
}

// Défloutage du logiciel
function unblur() {
    $element
        .css('filter', '')
        .css('webkitFilter', '')
        .css('mozFilter', '')
        .css('oFilter', '')
        .css('msFilter', '');
}

// Ouverture du loader
export function openLoader() {
    closeLoader();
    let $loader = '<div class="loader">';
    $loader += '<div class="loader-content">';
    // $loader += '<i class="loader-icon fas fa-spin fa-circle-notch text-warning"></i>';
    $loader += '<div class="loader-icon">';
    $loader += '<i class="ri-loader-4-line text-warning"></i>';
    $loader += '</div>';
    $loader += '<div class="loader-message">';
    $loader += '<div class="bold text-black mb5">Enregistrement en cours</div>';
    $loader += '<div>Merci de patienter quelques instants</div>';
    $loader += '</div></div></div>';
    $body.append($loader);
    blur();
}

// openLoader();

// Fermeture du loader
export function closeLoader() {
    $('.loader').remove();
    unblur();
}

$(document).on('click', '.loader-content', function () {
    closeLoader();
});

// Logiciel en local
function isLocal() {
    return $('#isL').val() ? 1 : 0;
}

// Logiciel en test
function isTest() {
    return $('#isT').val() ? 1 : 0;
}

// Logiciel en dev
function isDev() {
    return $('#isD').val() ? 1 : 0;
}

// Logiciel en prod
function isProd() {
    return $('#isP').val() ? 1 : 0;
}

// Environnement
function getEnvironment() {
    return $('#env').val();
}

// User webmaster
function isWebmaster() {
    return $('#isW').val() ? 1 : 0;
}

// Navigation menu
function setNav() {
    return;
    $('#nav-menu li').each(function () {
        let $this = $(this);
        if ($this.find('ul').length) {
            $this.find(">a").append('<span class="arrow"></span>')
        }
        if ($this.hasClass('active')) {
            $this.addClass('open');
        }
    });
}

// Scrollbar
function setScrolls() {

    // https://manos.malihu.gr/jquery-custom-content-scroller/
    // https://manos.malihu.gr/repository/custom-scrollbar/demo/examples/scrollbar_themes_demo.html
    // http://manos.malihu.gr/jquery-custom-content-scroller/

    let wh = $(window).height();
    let headerH = $('header').outerHeight();
    let breadcrumbH = $('#breadcrumb').outerHeight();
    let navUserH = $('#nav-user').outerHeight();
    let navTitleH = $('#nav-content-title').outerHeight();
    let navMenuH = wh - headerH - navUserH - navTitleH;
    let sidebarPadding = parseInt($('#sidebar').css('paddingTop').replace('px', '')) * 2;
    let sidebarBtns = $('#sidebar-options').outerHeight();
    let sidebarH = wh - headerH - breadcrumbH - sidebarBtns - sidebarPadding;

    $('#nav-menu').slimScroll({
        height: navMenuH,
        color: '#999',
        position: 'left',
        size: '0px',
    });

    $('#sidebar-content').slimScroll({
        height: sidebarH,
        color: '#999',
        position: 'right',
        // size: '2px',
        size: '0px',
        distance: '0px'
    });

    return false;
}

// Resize élément
function resize() {

    if (new Date() - rtime < delta) {
        setTimeout(resize, delta);
    } else {
        timeout = false;
        // initSelect2();
        // setHauteurBloc();
        setScrolls();
        setBoxFullscreen();
        setSelect();
    }
}

// Alignement de blocs ayant le même data-align-height = " xxx "
function setAlignBlockHeight() {

    $('div[data-align-height]').height('auto');
    let aligns = {};

    $('div[data-align-height]').each(function () {

        let $this = $(this);
        let attrName = $this.attr('data-align-height');
        let height = parseInt($this.height());

        if (aligns[attrName] == null) {
            aligns[attrName] = height;
        } else if (height > aligns[attrName]) {
            aligns[attrName] = height;
        }
    });

    $.each(aligns, function (index, value) {

        let height = Math.ceil(value / 10) * 10;

        if (height !== 0) {
            $('div[data-align-height="' + index + '"]').height(value);
        }
    });
}

// Select2
function setSelect() {
    return;
    let width = $('body').width();
    if (width < 769) {
        $('select').removeClass('select2-hidden-accessible');
        $('.select2').remove();
        return false;
    }
    $('select').each(function () {
        if ($(this).hasClass('none') || $(this).hasClass('hidden') || $(this).hasClass('no-select')) {
            return;
        }
        if ($(this).hasClass('no-search') || width < 769 || $(this).find('option').length < 5) {
            $(this).select2({minimumResultsForSearch: 'Infinity', width: '100%'});
        } else {
            $(this).select2({width: '100%'});
        }
        let moreClass = $(this).attr('data-class');
        $($(this).data('select2').$container).find('.select2-selection').addClass(moreClass);
    });


    return false;
}

function initSelect2() {
    setSelect();
}

// Datatable
function setDatatable() {

    $('.datatable').each(function () {
        let globalParams = {
            language: {
                "sProcessing": "Traitement en cours...",
                "sSearch": "Rechercher",
                "sLengthMenu": "_MENU_ &eacute;l&eacute;ments",
                "sInfo": "Affichage de _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                "sInfoEmpty": "Affichage de 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                "sInfoFiltered": "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                "sInfoPostFix": "",
                "sLoadingRecords": "Chargement en cours...",
                "sZeroRecords": "Aucun &eacute;l&eacute;ment &agrave; afficher",
                "sEmptyTable": "Aucune donn&eacute;e disponible dans le tableau",
                "oPaginate": {
                    "sFirst": "Premier",
                    "sPrevious": "Pr&eacute;c&eacute;dent",
                    "sNext": "Suivant",
                    "sLast": "Dernier"
                },
                "oAria": {
                    "sSortAscending": ": activer pour trier la colonne par ordre croissant",
                    "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
                }
            },
            // responsive: true,
            dom: "<'row mb10'<'col-xs-12 col-md-4'l><'col-xs-12 col-md-4 text-center'B><'col-xs-12 col-md-4 text-right'f>>" + "t" + "<'row mt10'<'col-xs-12 col-md-6'i><'col-xs-12 col-md-6 text-right'p>>",
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': ['nosort', 'no-sort']
            }],
            order: [],
            initComplete: function () {
                this.api().columns().every(function () {
                    let column = this;
                    let select = $('<select class="no-select"><option value=""></option></select>')
                        .appendTo($(column.footer()).empty())
                        .on('change', function () {
                            let val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search(val ? '^' + val + '$' : '', true, false)
                                .draw();
                        });
                    column.data().unique().sort().each(function (d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>')
                    });
                });
                $('.dataTables_wrapper')
                    .find('.dt-button')
                    .addClass('btn btn-default');


                // console.log(this.searchBuilder());
            }
        };
        let $this = $(this);
        let dtParams = globalParams;
        if (typeof $this.attr('datatable-height') !== 'undefined') {
            dtParams.paging = false;
            dtParams.scrollY = parseInt($this.attr('datatable-height'));
        } else {
            dtParams.paging = true;
            dtParams.pageLength = 10;
            dtParams.lengthMenu = [[10, 25, 50, 100, -1], [10, 25, 50, 100, "Tous"]];
        }
        $this.DataTable(dtParams);
    });

    $('.dataTables_length select').addClass('form-control w75 mr10');
    $('.dataTables_filter input').addClass('form-control w200 ml10');
}

// Ouverture/fermeture du menu de gauche
function setHamburger() {
    let speed = 250;
    let $this = $('#hamburger');
    let navW = $('nav').width();

    // $('nav').toggleClass('open');
    // $('main').toggleClass('open');
    // $('#breadcrumb-content').toggleClass('open');
    // $('#logo').toggleClass('open');

    // if (!$this.hasClass('open')) {
    //     $this.addClass('open');
    //     $('nav').toggleClass('open');
    //     $('main').toggleClass('open');
    //     $('#breadcrumb-content').toggleClass('open');
    //     $('#logo').toggleClass('open');
    // } else {
    //     $this.addClass('open');
    //     $('nav').animate({left: -navW + 'px'}, speed);
    //     $('#logo').animate({width: '70px'}, speed);
    //     $('#header-content').animate({left: -navW + 'px'}, speed);
    //     $('#breadcrumb-content').animate({paddingLeft: '15px'}, speed);
    //     $('main').animate({paddingLeft: 0}, speed);
    // }


    if (!$this.hasClass('open')) {
        $this.addClass('open');
        $('nav').animate({left: 0}, speed);
        $('main').animate({paddingLeft: navW + 'px'}, speed);
        $('#breadcrumb-content').animate({paddingLeft: '235px'}, speed);
        $('#logo').animate({width: '220px'}, speed);
    } else {
        $this.removeClass('open');
        $('nav').animate({left: -navW + 'px'}, speed);
        $('#logo').animate({width: '70px'}, speed);
        $('#header-content').animate({left: -navW + 'px'}, speed);
        $('#breadcrumb-content').animate({paddingLeft: '15px'}, speed);
        $('main').animate({paddingLeft: 0}, speed);
    }

    // $this.toggleClass('open');
    let route = Routing.generate('admin_hamburger');
    xhr.abort();
    xhr = $.ajax({
        url: route,
        method: "get"
    });
    return false;
}

// Largeur du logiciel
function getWindowWidth() {
    return $(window).width();
}

// Hauteur du logiciel
function getWindowHeight() {
    return $(window).height();
}

// Retourne les dimensions d'un bloc à afficher en plein écran
function setBoxFullscreen() {
    let m = 10;
    let w = getWindowWidth() - m * 2;
    let h = getWindowHeight() - m * 2;
    $('.box-fullscreen').css({width: w, height: h, top: m, left: m});
}

// --------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------
// RECHERCHE CLIENT

// Fermeture du loader au clic dessus
$(document).on('click', '.loader-content', function () {
    if (isLocal()) {
        closeLoader();
    }
});

// Navigation menu à gauche
$(document).on('click', '#nav-menu a', function (e) {

    let $this = $(this);

    if ($this.attr('href') === '') {
        e.preventDefault();
    }

    let $parent = $this.parent();
    if ($parent.find('ul')) {
        if (!$parent.hasClass('open')) {
            // $parent.find('ul').addClass('block');
            $parent.addClass('open');
        } else {
            // $parent.find('ul').removeClass('block');
            $parent.removeClass('open');
        }
    }

});

// Ecouteur d'événement Resize
$(window).resize(function () {
    rtime = new Date();
    if (timeout === false) {
        timeout = true;
        setTimeout(resize, delta);
    }
});

// Popin loading au submit
$(document).on('submit', 'form', function () {
    openLoader();
});

// Popin loading au submit
$(document).on('click', '.js-btn-delete', function () {
    if (!confirm("Etes-vous sûr de vouloir supprimer cet élément ?")) {
        return false;
    }
    $(this).closest('form').submit();
});

// Hamburger
$(document).on('click', '#hamburger', function (e) {
    setHamburger();
});

// // Hamburger en tapant 2 fois la touche "Control"
// let nbArrowKeyPressed = 0;
// $(document).on('keyup', 'body', function (e) {
//     let key = e.keyCode || e.which;
//     if (key === 37 || key === 39) {
//         nbArrowKeyPressed++;
//     }
//     if (nbArrowKeyPressed === 2) {
//         setHamburger();
//         nbArrowKeyPressed = 0;
//     }
// });

// Ouverture d'un bloc en plein écran
let isFullscreen = false;
$(document).on('click', '.btn-fullscreen', function (e) {
    let $this = $(this);
    if (isFullscreen) {
        $('.box-fullscreen').remove();
        unblur();
    } else {
        blur();
        let $boxMax = $this.closest('.box').clone();
        $boxMax.addClass('box-fullscreen');
        $boxMax.find('.box-footer').remove();
        $('body').append($boxMax);
        setBoxFullscreen();
    }
    isFullscreen ^= 1;
});

// Changement d'un filtre global
$(document).on('change', '.filter', function () {
    openLoader();
    $('#form-filters').submit();
});

// --------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------
// RECHERCHE CLIENT

// Recherche d'un client via la saisie de la zone de recherche
function searchCustomer(e) {

    // Relachement de la touche CTRL
    // On ne prend pas puisque c'est lié certainement à un CTRL + V
    let key = e.keyCode || e.which;
    if (key === 17) {
        return;
    }

    let search = $('#software-search-text').val();
    if (search.length < 3) {
        closeSearch();
        return;
    }

    $('#software-search-icon-i').addClass('none');
    $('#software-search-icon-spinner').removeClass('none');

    xhr.abort();
    xhr = $.ajax({
        url: Routing.generate('admin_search'),
        method: "post",
        data: {search: search}
    }).done(function (response) {
        $('#search-results')
                .removeClass('none')
                .html(response);
        $('#software-search-icon-i').removeClass('none');
        $('#software-search-icon-spinner').addClass('none');
    });
    return false;
}

$(document).on('click', '#js-search-delete', function () {
    closeSearch();
    $('#software-search-text').val('');
});

// Fermeture de la recherche
function closeSearch() {
    $('#js-search').remove();
}

// Saisie de la recherche
$(document).on('keyup', '#software-search-text', function (e) {
    searchCustomer(e);
});

// Focus sur la zone de recherche
// Si une recherche a été faite, on la réaffiche
$(document).on('focus', '#software-search-text', function (e) {
    if ($('#software-results').html() !== '' && $('#software-results').hasClass('none')) {
        $('#software-results').removeClass('none');
    }
});

// Écouteur de l'événement click partout sur le logiciel
$(document).on('click', 'html', function (e) {
    return;
    // Liste des ID des éléments qui ne doivent pas générer de fermeture
    let idsExclude = ['search-results', 'software-results', 'software-search-text'];
    let position = idsExclude.indexOf($(e.target).attr('id'));

    // L'élément est dans le tableau à exclure
    if (position >= 0) {
        return;
    }

    // Un des parents de l'élément contient l'ID #software-search
    // Permet de bloquer la fermeture de la recherche en cliquant sur le bloc contenant
    // la zone de saisie de la recherche
    if ($(e.target).parents('#search-results').length) {
        return;
    }

    // Liste des fonctions qui ferment les différents blocs qu'on a besoin de fermer
    closeSearch();
    // ...();
    // ...();
    // ...();
});

// --------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------

// Après le chargement de la page
// $(document).ready(function () {
setAlignBlockHeight();
setDatatable();
setSelect();
setNav();
setScrolls();
// });

$(document).on('click', '#js-check-token', function () {

    xhr = $.ajax({
        url: Routing.generate('main_token'),
        method: "get"
    })
    return false;
});

export const sidebar = (response, width) => {

    width = width != undefined ? width:300
    const $sidebar = $('#sidebar');
    $('#sidebar-content').html(response);
    if ($sidebar.hasClass('open')) {
        // $sidebar.removeClass('open').animate({right: -300}, 250);
        // $('main').animate({paddingRight: 0}, 250);
    } else {
        $sidebar.addClass('open').animate({width:width, right: 0}, 250);
        $('main').animate({paddingRight: width}, 250);
    }

    // $('.slimScrollBar, #sidebar-content, .slimScrollDiv').css({top:0});
    $('#sidebar-content').slimScroll({scrollTo: 0});
}

$(document).on('click', '.js-btn-filter', function () {
    sidebar();
});

$(document).on('click', '.js-sidebar-btn-close', function () {
    const $sidebar = $('#sidebar');
    const width = $sidebar.outerWidth();
    $sidebar.removeClass('open').animate({right: -width}, 250);
    $('main').animate({paddingRight: 0}, 250);
});

