const $ = require("jquery");
let colorPicker = document.querySelector(".js-color");

function updateColorPicker(color)
{
    if ($('#js-active-user').val() == $('#js-update-user').val()) {
        $('.js-change-color').css({background: color});
        $('#nav-menu li.active a').css({background: color, borderRightColor: color});
        $('.js-reset-color.d-none').removeClass('d-none');
    }
    $('.js-color').val(color);
}

function updateColor(event)
{
    const color = event.target.value;
    updateColorPicker(color);
}

if (null != colorPicker) {

    $('.js-reset-color').on('click', function () {
        const color = $(this).attr('data-color');
        updateColorPicker(color);
    });

    colorPicker.addEventListener("input", updateColor, false);
    colorPicker.select();
}
