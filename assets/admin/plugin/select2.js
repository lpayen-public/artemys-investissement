const $ = require("jquery");

function initSelect2() {

    $('select').each(function () {
        if ($(this).hasClass('none') || $(this).hasClass('d-none')) {
            return false;
        }
        if ($(this).hasClass('no-search')) {
            $(this).select2({minimumResultsForSearch: Infinity, width: '100%'});
        } else {
            $(this).select2({
                width: '100%',
                "language": {
                    "noResults": function () {
                        return "Aucun résultat";
                    }
                },
            });
        }
    });
}

$("select").on("select2:unselect", function (evt) {
    if (!evt.params.originalEvent) {
        return;
    }

    evt.params.originalEvent.stopPropagation();
});

$(window).resize(function () {
    initSelect2();
});

$(document).ready(function () {
    initSelect2();
});
