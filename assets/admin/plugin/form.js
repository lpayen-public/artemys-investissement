const $ = require("jquery");

$(document).on('click', '.js-btn-submit', function () {
    const $form = $('body').find('form');
    $form.submit();
});

$(document).on('click', '.js-submit-stay', function () {
    const $form = $('body').find('form');
    $form.append('<input type="hidden" name="formStay" value="1" />');
    $form.submit();
});
