const alertHeight = 55;
let numAlert = 0;
$('.alert.alert-popin').each(function () {
    const $this = $(this);
    const width = $this.outerWidth() + 10;
    const from = -width + 'px';
    const top = (10 + numAlert * alertHeight) + 'px';
    const delay = 500 + numAlert * 250;
    $(this)
        .css({right: from, top: top})
        .delay(delay)
        .animate({right: '10px', opacity: 1}, 250, 'swing')
        .delay(2500)
        .animate({right: from}, 500, 'swing', function () {
            $(this).remove();
        })
    ;
    numAlert++;
});
