const $ = require("jquery");
const $pictures = $('#js-pictures');
const $cityResults = $('#city-results');
const $city = $('#product_city');
let xhrProduct = new XMLHttpRequest();

function checkProductPictures() {
    let nbPictures = $pictures.children().length;
    if (nbPictures > 1) {
        return true;
    }
    $('#product_files').prop('required', nbPictures > 0 ? false : true);
    $('.js-delete-product-media').remove();
    return false;
}

function deleteMedia(e) {
    const productToken = e.attr('data-product-token');
    const mediaToken = e.attr('data-token');
    xhrProduct.abort();
    xhrProduct = $.ajax({
        url: Routing.generate('admin_product_media_delete'),
        method: 'post',
        data: {productToken: productToken, mediaToken: mediaToken}
    }).done(function () {
        e.parent().parent().remove();
        checkProductPictures();
    });
}

$(document).on('click', '.js-delete-product-media', function (e) {
    e.preventDefault();
    e.stopPropagation();
    if ($(this).hasClass('js-is-picture') && !checkProductPictures()) {
        return;
    }
    if (!confirm("Etes-vous sûr ?")) {
        return;
    }
    deleteMedia($(this));
});

$(document).on('click', '.js-product-order', function (e) {
    e.preventDefault();
    e.stopPropagation();
    const $this = $(this);
    const productToken = $this.attr('data-product-token');
    const mediaToken = $this.attr('data-token');
    const direction = $this.attr('data-direction');
    xhrProduct.abort();
    xhrProduct = $.ajax({
        url: Routing.generate('admin_product_media_order'),
        method: 'post',
        data: {productToken: productToken, mediaToken: mediaToken, direction: direction}
    }).done(function (response) {
        $('#js-pictures-ajax').html(response);
    });
});
$(document).on('focus', '#product_city', function () {
    const $this = $(this);
    const value = $this.val();
    $this.val(value).select();
});
$(document).on('keyup', '#product_city', function () {
    const $this = $(this);
    const search = $this.val();
    if (search.length < 3) {
        $cityResults.addClass('d-none');
        return;
    }
    xhrProduct.abort();
    xhrProduct = $.ajax({
        url: Routing.generate('admin_product_city_search'),
        method: 'post',
        data: {search: search}
    }).done(function (response) {
        $cityResults.html(response).removeClass('d-none');
    });
});

$(document).on('click', '.js-city', function () {
    $('#product_cityToken').val($(this).attr('data-token'));
    $city.val($(this).attr('data-name'));
    $cityResults.addClass('d-none');
});

// $(document).ready(function () {
//     $('.js-pictures').sortable();
//     xhrProduct.abort();
//     xhrProduct = $.ajax({
//         url: Routing.generate('admin_product_media_order'),
//         method: 'post',
//         data: {datas: $('.js-pictures').serialize()}
//     }).done(function () {
//         // e.parent().remove();
//         // checkProductPictures();
//     });
// });

