import React from "react";
import { createRoot } from 'react-dom/client';
import SayHello from "../components/SayHello";

const jsReact = createRoot(document.getElementById('js-react'));
jsReact.render(<SayHello name="Luc" />);
