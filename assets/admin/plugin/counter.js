const $ = require("jquery");
const timeCounters = 250;
document.addEventListener('DOMContentLoaded', function () {
    $('.js-counter').each(function () {
        const $this = $(this);
        const to = parseInt($this.html());
        $this.html(0);
        let from = $this.data('from') != undefined ? $this.data('from') : 0;
        if (from === to) {
            return;
        }
        $this.attr('data-to', to);
        $this.attr('data-from', from);
        if (to < from) {
            from = 0;
            $this.attr('data-from', from);
        }
        const timeCounter = timeCounters / (to - from);
        const interval = setInterval(function () {
            if (from >= parseInt($this.data('to'))) {
                $this.html($this.data('to'));
                clearInterval(interval);
                return;
            }
            from++;
            $this.html(from);
        }, timeCounter);
    });
});
