const $windowSize = $('#js-windowSize');
const $btnSizeDown = $('#js-btn-window-size-down');
const $btnSizeUp = $('#js-btn-window-size-up');

function setWindowSize (size) {

    if (undefined === size) {
        size = $windowSize.val();
    }
    size = parseInt(size);
    document.body.style.zoom = size + '%';
    $windowSize.val(size);
    $btnSizeDown.attr('title', 'Diminuer à ' + (size - 10) + '%');
    $btnSizeUp.attr('title', 'Augmenter à ' + (size + 10) + '%');
}

$(document).on('click', '.js-btn-window-size', function () {
    let size = parseInt($windowSize.val());
    size += 'up' === $(this).attr('data-direction') ? 10 : -10;
    setWindowSize(size);
    $.ajax({
        method: 'post',
        url: Routing.generate('admin_window_size'),
        data: {windowSize: size}
    });
});

setWindowSize();
