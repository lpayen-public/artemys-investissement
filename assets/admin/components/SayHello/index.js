import React, {Component} from "react";
import './SayHello.css';

const SayHello = ({name}) => {
    return (
        <>
            <h1>Say Hello {name}</h1>
            <div>
                <a href="{{ path('front_index' }}">{name}</a>
            </div>
        </>
    );
}

export default SayHello;
