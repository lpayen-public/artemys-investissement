<?php

namespace App\Tests\Front;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
class FrontTest extends WebTestCase
{
    /**
     * @throws \Exception
     */
    public function testPageHome(): void
    {
        $client = static::createClient();

        $client->request('GET', '/');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('body', 'Artemys');
    }

    /**
     * @throws \Exception
     */
    public function testPageContact(): void
    {
        $client = static::createClient();

        $client->request('GET', '/contact');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('form', 'Nom');
    }
}
