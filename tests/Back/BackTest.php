<?php

namespace App\Tests\Back;

use App\Entity\User;
use App\Utils\Text;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @internal
 *
 * @coversNothing
 */
class BackTest extends WebTestCase
{
    private function createUser(string $role = 'ROLE_USER'): User
    {
        $em = static::getContainer()->get('doctrine')->getManager();
        $user = (new User())
            ->setToken(Text::token())
            ->setLastname('LASTNAME')
            ->setFirstname('Firstname')
            ->setEmail('email-' . rand(1000, 1000000) . '@email.com')
            ->setPhone('0610203040')
            ->setRoles([$role])
            ->setJobName('Job name')
            ->setPassword('123456')
            ->setCreatedAt(new \DateTime())
            ->setUpdatedAt(new \DateTime())
        ;

        $em->persist($user);
        $em->flush();

        return $user;
    }

    /**
     * @throws \Exception
     */
    public function testPageConnexion(): void
    {
        $client = static::createClient();
        $client->followRedirects();

        $client->request('GET', '/admin');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('body', 'identifier');
    }

    /**
     * @throws \Exception
     */
    public function testPageHome(): void
    {
        $client = static::createClient();
        $client->followRedirects();
        $user = $this->createUser();
        $client->loginUser($user, 'admin');

        $client->request('GET', '/admin');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('body', 'Bienvenue');
    }

    /**
     * @throws \Exception
     */
    public function testPageUsersAvailableIfWebmaster(): void
    {
        $client = static::createClient();
        $client->followRedirects();
        $user = $this->createUser('ROLE_SUPER_ADMIN');
        $client->loginUser($user, 'admin');

        $client->request('GET', '/admin/utilisateur');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('body', 'Utilisateurs');
        $this->assertSelectorTextContains('table', $user->getName(true));
        $this->assertSelectorTextContains('#nav-menu', 'Webmastering');
    }

    /**
     * @throws \Exception
     */
    public function testPageUsersRedirectToHomeIfNoWebmaster(): void
    {
        $client = static::createClient();
        $user = $this->createUser();
        $client->loginUser($user, 'admin');

        $client->request('GET', '/admin/utilisateur');
        $this->assertSame(Response::HTTP_MOVED_PERMANENTLY, $client->getResponse()->getStatusCode());

        $client->followRedirects();
        $client->request('GET', '/admin/utilisateur');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('body', 'Bienvenue');
    }
}
